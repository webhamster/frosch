from pages.models import PageMount, Page
from django.core.exceptions import ObjectDoesNotExist


def pagemounts(request):
    result = {
        'pagemounts': PageMount.objects.filter(footer=False, conference=None).order_by('sort_key'),
        'footerpagemounts': PageMount.objects.filter(footer=True, conference=None).order_by('sort_key'),
    }

    try:
        result['what_is_pam_card'] = Page.objects.get(name='what-is-pam')
    except ObjectDoesNotExist:
        pass
        
    return result
