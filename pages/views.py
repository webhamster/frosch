from .models import PageMount
from django.views.generic import DetailView
from core.views import ConferenceContextMixin

class Page(DetailView):
    slug_url_kwarg = 'pagemount_slug'
    template_name = 'core/page.html'
    context_object_name = 'pagemount'
    queryset = PageMount.objects.filter(conference=None)


class ConferencePage(ConferenceContextMixin, DetailView):
    slug_url_kwarg = 'pagemount_slug'
    template_name = 'core/conference_page.html'
    context_object_name = 'pagemount'

    def get_queryset(self):
        return self.get_conference().pagemount_set

