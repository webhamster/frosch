from pages import views
from django.conf.urls import url
from django.conf import settings
from .sitemaps import PageViewSitemap, ConferencePageViewSitemap

app_name = 'pages'

sitemaps = {
    'pages_page': PageViewSitemap(),
    'pages_conference_page': ConferencePageViewSitemap(),
}


urlpatterns = [
    url(
        r'^%s(?P<conference_id>[^/]+)/(?P<pagemount_slug>.+)/$' % settings.FROSCH_CONFERENCE_PREFIX, views.ConferencePage.as_view(), name='conference_page'),
    url(
        r'^(?P<pagemount_slug>.+)/$', views.Page.as_view(), name='page'),
]
