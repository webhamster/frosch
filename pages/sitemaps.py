from django.contrib.sitemaps import Sitemap
from .views import Page as PageView
from .models import PageMount

class PageViewSitemap(Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return PageView().get_queryset()


class ConferencePageViewSitemap(Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return PageMount.objects.filter(conference__isnull=False)
