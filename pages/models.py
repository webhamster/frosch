from django.db import models
from core.models import Conference
from django.urls import reverse

class Page(models.Model):
    name = models.SlugField(primary_key=True)
    title = models.CharField(max_length=100)
    content = models.TextField()


class PageMount(models.Model):
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    slug = models.SlugField()
    title = models.CharField(max_length=100)
    footer = models.BooleanField(default=False)
    conference = models.ForeignKey(Conference, on_delete=models.CASCADE, blank=True, null=True)
    sort_key = models.FloatField(default=0)
    
    def get_absolute_url(self):
        if self.conference:
            return reverse('page:conference_page', kwargs={'conference_id': self.conference.url_snippet, 'pagemount_slug': self.slug})
        return reverse('page:page', kwargs={'pagemount_slug': self.slug})

    class Meta:
        ordering = ['sort_key']
        unique_together = (('slug', 'conference'),)
        index_together = [['slug', 'conference'],]
