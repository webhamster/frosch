from modeltranslation.translator import register, TranslationOptions
from .models import Page, PageMount

@register(Page)
class PageTranslationOptions(TranslationOptions):
    fields = ('title', 'content', )

@register(PageMount)
class PageMountTranslationOptions(TranslationOptions):
    fields = ('slug', 'title', )
