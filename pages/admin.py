from django.contrib import admin
from .models import Page, PageMount
from modeltranslation.admin import TranslationAdmin, TranslationTabularInline
from django import forms
from pagedown.widgets import AdminPagedownWidget
from django.conf import settings


class PageAdminForm(forms.ModelForm):
    class Meta:
        widgets = {
         
        }
        widgets.update(dict(
            ("%s_%s" % (name, lang), AdminPagedownWidget())
            for name in ['content', ]
            for lang, _ in settings.LANGUAGES
        ))


class PageMountInline(TranslationTabularInline):
    model = PageMount
    extra = 1


@admin.register(Page)
class PageAdmin(TranslationAdmin):
    list_display = ('name', 'title')
    form = PageAdminForm
    inlines = [
        PageMountInline,
    ]
