# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager, Group
from django.utils.translation import ugettext_lazy as _, override
from django.template import RequestContext
from post_office import mail
from django.conf import settings
from django.utils import timezone
from django.db import transaction
from django.apps import apps
import random
import string
from datetime import datetime, timedelta
from django.db.models import Q, Max
from core.conflicts import ConflictCollection, UserAvailabilityConflict, UserDutiesConflict, RoomConflict
from core.changes import ChangeList, ScheduleableChanges
from natural.text import nato
from core.utils import find_template, get_initial_lang
from django.db.models.signals import post_save
from django.dispatch import receiver
from filer.fields.image import FilerImageField
from filer.fields.folder import FilerFolderField
#from django.contrib.gis.db import models as gismodels
from djgeojson.fields import PointField
from django.urls import reverse
import django.apps


def send_templated_mail_to_frosch_admins(request, template, context):
    context.update({
        'user': request.user,
        'FROSCH_NAME': settings.FROSCH_NAME,
    })
    template, lang_context = find_template(template)

    with lang_context:
        mail.send(
            recipients=list(x[1] for x in settings.ADMINS),
            template=template,
            context=RequestContext(request, context),
            headers={'X-FROSCH-Admin-Notification': 'Yes'},
        )

        
class CoordinateType():
    MAIN, OTHER, PARKING, ENTRANCE, PUBLIC_TRANSPORT = range(5)
    descriptions = (
        (MAIN, _("Conference location")),
        (OTHER, _("Other")),
        (PARKING, _("Parking")),
        (ENTRANCE, _("Entrance")),
        (PUBLIC_TRANSPORT, _("Public Transportation")),
    )

    # See https://github.com/lvoogdt/Leaflet.awesome-markers for icon options
    icon_options = {
        MAIN: {
            'icon': 'bullseye',
            'prefix': 'fa',
            'markerColor': 'darkred',
        },
        OTHER: {
            'icon': 'bullseye',
            'prefix': 'fa',
            'markerColor': 'lightgray',
        },
        PARKING: {
            'icon': 'car',
            'prefix': 'fa',
            'markerColor': 'cadetblue',
        },
        ENTRANCE: {
            'icon': 'home',
            'prefix': 'fa',
            'markerColor': 'darkred',
        },
        PUBLIC_TRANSPORT: {
            'icon': 'bus',
            'prefix': 'fa',
            'markerColor': 'orange',
        },
    }

    
class Conference(models.Model):
    long_name = models.CharField(max_length=100)
    short_name = models.CharField(max_length=20)
    url_snippet = models.SlugField(max_length=20, unique=True)
    is_active = models.BooleanField('Make conference publicly visible', default=False)
    contacts = models.ForeignKey(Group, blank=True, null=True, on_delete=models.SET_NULL)
    rooms = models.ManyToManyField('core.Room', blank=True)
    venue = models.ForeignKey('core.Venue', null=True, on_delete=models.SET_NULL)
    intro_text = models.TextField(null=True, blank=True)
    intro_image = FilerImageField(blank=True, null=True, related_name='intro_image', on_delete=models.SET_NULL)
    venue_text = models.TextField(null=True, blank=True)
    venue_image = FilerImageField(blank=True, null=True, related_name='venue_image', on_delete=models.SET_NULL)
    schedule_text = models.TextField(null=True, blank=True)
    gallery = FilerFolderField(null=True, blank=True, on_delete=models.SET_NULL)

    class Meta:
        indexes = [
            models.Index(fields=['url_snippet']),
        ]

    def get_notification_addresses(self):
        return list(self.contacts.user_set.filter(is_staff=True).values_list('email', flat=True))

    def send_templated_mail_to_admins(self, request, template, context):
        context.update({
            'user': request.user,
            'FROSCH_NAME': settings.FROSCH_NAME,
        })
        template, lang_context = find_template(template)

        with lang_context:
            mail.send(
                recipients=self.get_notification_addresses(),
                template=template,
                context=RequestContext(request, context),
                headers={'X-FROSCH-Admin-Notification': 'Yes'},
            )

    def is_multiday(self):
        return self.public_days.count() > 1

    def get_conference_day_limits(self):
        cdays = self.public_days.order_by('date').all()
        if len(cdays) == 0:
            return None
        return (cdays.first(), cdays.last())

    @property
    def public_days(self):
        return self.conferenceday_set.exclude(Q(public_start=None) | Q(public_end=None)).order_by('date')

    @property
    def public_start(self):
        first_public_day = self.public_days.order_by('date').first()
        return datetime.combine(first_public_day.date, first_public_day.public_start)

    @property
    def public_end(self):
        first_public_day = self.public_days.order_by('-date').first()
        return datetime.combine(first_public_day.date, first_public_day.public_end)

    def __str__(self):
        return self.short_name

    @property
    def main_coordinates(self):
        return self.coordinates.filter(type=CoordinateType.MAIN).all()

    def get_absolute_url(self):
        return reverse('core_conference:conference', args=[str(self.url_snippet)])


class ConferenceHighlight(models.Model):
    conference = models.ForeignKey(Conference, related_name='highlights', on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    description = models.TextField(max_length=250)
    link = models.URLField(blank=True, null=True)
    event = models.ForeignKey('callforcontributions.Event', blank=True, null=True, on_delete=models.CASCADE)
    image = FilerImageField(on_delete=models.CASCADE)

    @property
    def link_url(self):
        if self.link:
            return self.link
        elif self.event:
            return 'DUMMY' #reverse('core_conference:conference', kwargs={'event_id': self.event.id}) # CHANGE TO core:event
        

class Room(models.Model):
    name = models.CharField(max_length=50)
    sort_key = models.FloatField(default=0)
    is_public = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['sort_key']


class ConferenceDay(models.Model):
    conference = models.ForeignKey(Conference, db_index=True, on_delete=models.CASCADE)
    date = models.DateField()
    start = models.TimeField(_("Start of activities."))
    end = models.TimeField(_("End of activities."))
    public_start = models.TimeField(
        _("Start of public activities (leave empty for non-public day)."),
        null=True,
        blank=True)
    public_end = models.TimeField(
        _("End of public activities (leave empty for non-public day)."),
        null=True,
        blank=True)

    def __str__(self):
        return '%s: %s' % (self.conference.short_name, self.date.strftime('%d.%m.%Y'))

    def get_start_datetime(self):
        return timezone.make_aware(datetime.combine(self.date, self.start), timezone.get_default_timezone())

    def get_end_datetime(self):
        return timezone.make_aware(datetime.combine(self.date, self.end), timezone.get_default_timezone())
    
    def get_public_start_datetime(self):
        if self.public_start is not None and self.public_end is not None:
            return timezone.make_aware(datetime.combine(self.date, self.public_start), timezone.get_default_timezone())

    def get_public_end_datetime(self):
        if self.public_start is not None and self.public_end is not None:
            return timezone.make_aware(datetime.combine(self.date, self.public_end), timezone.get_default_timezone())

    def get_absolute_url(self):
        # TODO: currently we do not have a view for a specific day.
        return reverse('core_conference:conference_public_schedule_day', kwargs={'conference_id': self.conference.url_snippet, 'day': self.id})


class UserAvailability(models.Model):
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    day = models.ForeignKey('ConferenceDay', db_index=True, on_delete=models.CASCADE)
    start = models.TimeField(_('Available not before'), null=True, blank=True)
    end = models.TimeField(_('Available not after'), null=True, blank=True)
    start_datetime = models.DateTimeField(null=True, blank=True, editable=False, db_index=True)
    end_datetime = models.DateTimeField(null=True, blank=True, editable=False, db_index=True)
    not_available = models.BooleanField(_('Not available on the whole day'), default=False)

    def save(self, **kwargs):
        if self.start is None:
            self.start_datetime = None
        else:
            self.start_datetime = datetime.combine(self.day.date, self.start)
            if not timezone.is_aware(self.start_datetime):
                tz = timezone.get_default_timezone()
            self.start_datetime = timezone.make_aware(self.start_datetime, tz)

        if self.end is None:
            self.end_datetime = None
        else:
            self.end_datetime = datetime.combine(self.day.date, self.end)
            if not timezone.is_aware(self.end_datetime):
                tz = timezone.get_default_timezone()
            self.end_datetime = timezone.make_aware(self.end_datetime, tz)

        super().save(**kwargs)

    def is_valid(self):
        return self.not_available or self.start is not None or self.end is not None

    def __str__(self):
        out = u"%s | %s: " % (str(self.user), self.day.date.strftime("%Y-%m-%d"))
        if self.not_available:
            return u"%sNOT AVAILABLE" % out
        return u"%snot before: %s -- not after: %s" % (
            out,
            self.start.strftime("%H:%M") if self.start else "-",
            self.end.strftime("%H:%M") if self.end else "-")


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(
        _('public name'), max_length=50, unique=True,
        help_text=_('Required. Default: Firstname Lastname. Alternative: Nickname. Public.'),
        error_messages={
            'unique': _("A user with that username already exists."),
        })
    email = models.EmailField(
        _('email address'),
        unique=True,
        help_text=_("Required. We will send you notifications to this address, but no spam."))

    phone = models.CharField(
        _('phone number'),
        max_length=30,
        help_text=_("Not required, but strongly recommended. We might want to call you if there are last-minute changes or for other important information."),
        null=True,
        blank=True)
    affiliation = models.CharField(
        _('affiliation'),
        null=True,
        blank=True,
        max_length=60,
        help_text=_("Optional and public. If provided, will also be printed on your speaker badge."))
    about = models.TextField(
        _('about me'),
        null=True,
        blank=True,
        help_text=_("Optional. Public. Use Markdown for formatting. "
                    "If you plan to submit a contribution, describe yourself here."))
    notes = models.TextField(
        _('notes'),
        null=True,
        blank=True,
        help_text=_("Optional. Only visible to admins. Any notes on your person that do not fit anywhere else."))
    twitter = models.CharField(
        _('twitter account'),
        null=True,
        blank=True,
        max_length=140,
        help_text=_("Optional. Public."))
    secret_token = models.CharField(max_length=15, null=True, blank=True)
    password_reset_token = models.CharField(max_length=15, null=True, blank=True)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin '
                    'site.'))
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    preferred_language = models.CharField(
        _('Preferred language'),
        max_length=10,
        choices=settings.LANGUAGES,
        default=get_initial_lang,
        help_text=_("Used for emails."))
    image = models.ImageField(
        upload_to="event_media",
        verbose_name=_("Profile Picture"),
        help_text=_("Your profile picture (PNG or JPEG)."),
        blank=True,
        null=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = ['username']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_short_name(self):
        return self.username

    def get_long_name(self):
        return self.username

    def name(self):
        return self.username
            
    @staticmethod
    def get_token(length):
        return ''.join(
            random.choice(string.ascii_letters + string.digits) for _ in range(length))

    def generate_secret_token(self):
        self.secret_token = self.get_token(15)

    def generate_password_reset_token(self):
        self.password_reset_token = self.get_token(15)

    def send_templated_mail(self, request, template, context, send_now=True, dry_run=False):
        context.update({
            'request': request,
            'recipient': self,
            'user': request.user,
            'FROSCH_NAME': settings.FROSCH_NAME,
        })
        template_obj, lang_context = find_template(template, self.preferred_language)
        with lang_context:
            if dry_run:
                return mail.create(
                    recipients=[self.email],
                    context=context,
                    template=template_obj,
                    render_on_delivery=False,
                    commit=False,
                    sender='dummy@example.com'
                )
            else:
                return mail.send(
                    recipients=[self.email],
                    context=context,
                    template=template_obj,
                    priority=mail.PRIORITY.now if send_now else None
                )

    def has_full_availability(self, conference):
        for d in conference.conferenceday_set.all():
            if not UserAvailability.objects.filter(user_id=self.id, day_id=d.id).exists():
                return False
        return True

    @property
    def events(self):
        return self.event_set.annotate(conference_end=Max('conference__conferenceday__date')).order_by('-conference_end', '-start_datetime')

    @property
    def jobs(self):
        return self.jobs_assigned.filter(version=Scheduleable.RELEASED)

    def get_absolute_url(self):
        return reverse('core:user', kwargs={'user_id': self.id})

    def __str__(self):
        return self.username


class UserAssignment(models.Model):
    """A user assignment is a mapping from users to conferences annotated
    with the user's role at that conference. Most of the entries will
    be created automatically, but the need may arise to change/add some entries manually.
    """
    conference = models.ForeignKey(Conference, db_index=True, on_delete=models.CASCADE)
    user = models.ForeignKey(User, db_index=True, on_delete=models.CASCADE, blank=True, null=True)
    custom_badge_name = models.CharField(max_length=100, blank=True, null=True)
    custom_badge_affiliation = models.CharField(max_length=100, blank=True, null=True)
    roles = models.CharField(max_length=100)
    notes = models.CharField(null=True, blank=True, max_length=250)
    locked = models.BooleanField(default=True, blank=True)

    @property
    def roles_list(self):
        return [x.strip() for x in self.roles.split(',') if x.strip()]

    @roles_list.setter
    def roles_list(self, lst):
        self.roles = ', '.join(l for l in lst if l)

    @classmethod
    def generate_for_conference(cls, conference):
        from callforcontributions.models import Event, EventStatus
        from staffregistration.models import Job
        
        cls.objects.filter(
            conference=conference,
            locked=False
        ).delete()

        def add_role(role, users):
            for user in users:
                entry, created = cls.objects.get_or_create(
                    user=user,
                    conference=conference
                )
                if created:
                    entry.locked = False
                if entry.locked:
                    continue
                rls = entry.roles_list
                if role not in rls:
                    rls.append(role)
                    entry.roles_list = rls
                    entry.save()
                    
        with override(settings.BADGE_LANGUAGE):
            organizer = str(_("Organizer"))
            speaker = str(_("Speaker"))
            team = str(_("Team"))

        add_role(
            organizer,
            conference.contacts.user_set.all())
            
        for event in Event.objects.filter(
                Q(status=EventStatus.CONFIRMED) | Q(status=EventStatus.UNCONFIRMED),
                conference=conference,
                version=Event.RELEASED,
                hide_speakers=False).all():
            add_role(
                speaker,
                event.speaker.all())

        for job in Job.objects.filter(
                conference=conference,
                version=Job.RELEASED).all():
            add_role(
                team,
                job.staff_assigned.all())


class HasConflicts(Exception):
    pass


class Scheduleable(models.Model):

    """Scheduleables (= events, jobs) can be of the following scheduling states (short SS):
    | State  | day      | time/length | room     | Examples/Notes                                           |
    |--------+----------+-------------+----------+----------------------------------------------------------|
    | SS_DTR | not None | not None    | not None | Normal scheduled events (talk in some room at some time) |
    | SS_DT  | not None | not None    | None     | Happening everywhere at the same time, e.g. lunch break  |
    | SS_DR  | not None | None        | not None | Event happening all day (e.g., exhibition)               |
    |        | not None | None        | None     | - not implemented -                                      |
    |        | None     | not None    | not None | - not implemented -                                      |
    |        | None     | not None    | None     | - not implemented -                                      |
    |        | None     | None        | not None | - not implemented -                                      |
    |        | None     | None        | None     | - not implemented -                                      |
    
    """
    VERSIONS = (DRAFT, RELEASED) = range(2)
    SCHEDULING_STATES = (SS_NONE, SS_DTR, SS_DT, SS_DR) = range(4)

    COPY_MANYTOONE = []

    conference = models.ForeignKey(Conference, db_index=True, on_delete=models.CASCADE)
    room = models.ForeignKey(Room, null=True, blank=True, db_index=True, on_delete=models.CASCADE)
    time = models.TimeField(null=True, blank=True)
    day = models.ForeignKey('core.ConferenceDay', null=True, blank=True, db_index=True, on_delete=models.CASCADE)
    start_datetime = models.DateTimeField(null=True, blank=True, editable=False, db_index=True)
    end_datetime = models.DateTimeField(null=True, blank=True, editable=False, db_index=True)
    version = models.SmallIntegerField(default=DRAFT, editable=False, db_index=True)
    draft = models.OneToOneField(
        'self',
        verbose_name=_("If this is a release version, which event is this event based on?"),
        default=None,
        blank=True,
        null=True,
        editable=False,
        on_delete=models.SET_NULL,
        related_name='released')
    saved_draft_id = models.IntegerField(null=True, blank=True, editable=False)  # we might need the draft.id even if the relation "draft" was already removed
    scheduled = models.SmallIntegerField(default=SS_NONE, editable=False, db_index=True)

    # def has_datetime_schedule(self):
    #     return self.time is not None and self.day_id is not None and self.length is not None
    #
    # def has_date_schedule(self):
    #      return self.day is not None

    @property
    def stable_id(self):
        return self.saved_draft_id or self.id

    @property
    def is_scheduled(self):
        return self.scheduled != self.SS_NONE

    @property
    def to_be_published(self):
        return self.is_scheduled

    @property
    def has_datetime(self):
        return self.scheduled not in (self.SS_NONE, self.SS_DR)

    @property
    def has_date(self):
        return self.is_scheduled  # might change if there are scheduleables without dates that can be scheduled

    @property
    def has_room(self):
        return self.room_id is not None

    @property
    def in_past(self):
        if self.has_datetime:
            return self.end_datetime < timezone.now()
        if self.has_date:
            return self.day.date < timezone.now().date()
        else:
            return None

    def get_latest_released_or_draft(self):
        if self.version == self.RELEASED:
            return self
        if self.released is not None:
            return self.released
        return self

    def set_start_datetime(self, start_datetime):
        """Set start_datetime, adjust time and day, and end_datetime accordingly."""
        start_datetime = start_datetime.astimezone(timezone.get_default_timezone())
        self.time = start_datetime.time()
        self.day = ConferenceDay.objects.get(
            conference=self.conference,
            date=start_datetime.date()
        )
        self.start_datetime = start_datetime

    def save(self, *args, **kwargs):
        if self.time is not None and self.day_id is not None and self.length is not None:
            self.start_datetime = datetime.combine(self.day.date, self.time)
            if not timezone.is_aware(self.start_datetime):
                tz = timezone.get_default_timezone()
                self.start_datetime = timezone.make_aware(self.start_datetime, tz)
            self.end_datetime = self.start_datetime + timedelta(minutes=int(self.length))
        else:
            self.start_datetime = None
            self.end_datetime = None

        self.scheduled = self.SS_NONE
        if self.room_id is not None:
            if self.start_datetime is not None:
                self.scheduled = self.SS_DTR
            elif self.day_id is not None:
                self.scheduled = self.SS_DR
        else:
            if self.start_datetime is not None:
                self.scheduled = self.SS_DT

        # Check if there is a VersionableContainer, and if there is, update the container's dirty flag
        for m in django.apps.apps.get_models():
            if getattr(m, 'versionable_model', None) is type(self):
                try:
                    obj = m.objects.get(conference_id=self.conference_id)
                except m.DoesNotExist:
                    continue
                obj.needs_release = True
                obj.save()
                    
        super().save(*args, **kwargs)
        
    def get_conflicts(self, raise_exception=False, userset=None, skip_room=False):
        conflicts = ConflictCollection(self.id)
        if self.scheduled not in (self.SS_DTR, self.SS_DT):  # events without date and time cannot have conflicts
            return conflicts

        persons_required = userset or self.get_persons_required().all()
        room_required = self.get_room_required() if not skip_room else None
        # First we check that all of this events users are available.
        for user in persons_required:
            conflict = UserAvailabilityConflict.check_user_and_scheduleable(user, self)
            if conflict is not None:
                if raise_exception:
                    raise HasConflicts()
                conflicts.append(conflict)

        # Now we check events/jobs happening at the same time for speaker/room conflicts

        # This is a generic filter that filters jobs/events happening at the same conference and at the same time
        # Note that this automatically excludes unscheduled events.
        filter = (
            Q(conference_id=self.conference_id) &
            (Q(scheduled=self.SS_DTR) | Q(scheduled=self.SS_DT)) &
            Q(day_id=self.day_id) &
            (
                Q(end_datetime__gt=self.start_datetime, end_datetime__lte=self.end_datetime) |
                Q(start_datetime__gte=self.start_datetime, start_datetime__lt=self.end_datetime) |
                Q(start_datetime__lte=self.start_datetime, end_datetime__gte=self.end_datetime)))

        for cls in Scheduleable.__subclasses__():
            Model = apps.get_model(app_label=cls._meta.app_label, model_name=cls.__name__)
            if cls is type(self):  # do not match ourselves
                my_filter = filter & ~Q(id=self.id)
            else:
                my_filter = filter
            scheduleables_same_time = (
                Model.objects
                .filter(version=Scheduleable.DRAFT)
                .filter(my_filter))
            
            for e in scheduleables_same_time.all():
                conflict_persons = list(e.get_persons_required().all() & persons_required)
                if len(conflict_persons) > 0:
                    if raise_exception:
                        raise HasConflicts()
                    conflicts.append(UserDutiesConflict(conflict_persons, e))

                if room_required is not None:
                    e_room_required = e.get_room_required()
                    if e_room_required == room_required:
                        if raise_exception:
                            raise HasConflicts()
                        conflicts.append(RoomConflict(room_required, e))
                        
        return conflicts

    def get_meta(self):
        """Just returns meta. We need this in schedule_event.html template,
        since django templates do not allow accessing _meta (it starts with an
        underscore)."""
        return self._meta
    
    class Meta:
        abstract = True

    def get_copy(self):
        orig_id = int(self.id)
        old_relations = {}
        old_manytoone = {}
        # copy manytomanyfields
        for f in self.__class__._meta.get_fields():
            if isinstance(f, models.ManyToManyField):
                old_relations[f.name] = list(getattr(self, f.name).values_list('id', flat=True))
            if isinstance(f, models.ManyToOneRel) and f.name in self.COPY_MANYTOONE:
                old_manytoone[f.name] = list(item.get_copy() for item in getattr(self, f.name).all())
        copy = self
        copy.pk = None
        copy.id = None
        copy.draft_id = orig_id
        copy.saved_draft_id = orig_id
        copy.version = Scheduleable.RELEASED
        copy.save()
        # We need to copy the contents of manytomanyfields manually
        for f_name, value in old_relations.items():
            getattr(copy, f_name).add(*value)
        # And the contents of manytoonefields as well.
        for f in self.__class__._meta.get_fields():
            if isinstance(f, models.ManyToOneRel) and f.name in self.COPY_MANYTOONE:
                for item in old_manytoone[f.name]:
                    if item: # bugfix. ignore None (occurs if copy could not be created above)
                        getattr(copy, f.name).add(item)
                
        copy.save()
        return copy
    

@receiver(post_save, sender=ConferenceDay)
def update_scheduleables(instance, **kwargs):
    for CLS in Scheduleable.__subclasses__():
        for o in CLS.objects.filter(day_id=instance.id).all():
            o.save()
            

class VersionableContainer(models.Model):
    last_draft_released = models.DateTimeField(null=True, blank=True, editable=False)
    version_counter = models.IntegerField(default=-1, editable=False)
    versionable_model = None
    needs_release = models.BooleanField(default=False, editable=False)

    def _increment_version_counter(self):
        self.version_counter += 1
        self.last_draft_released = timezone.now()
        self.save()

    def get_version(self):
        """Returns a string such as "Alpha Charlie" for the version number of
        the schedule. This helps users to quickly identify changes in the
        schedule just by the name."""
        out = ""
        n = self.version_counter + 1
        if n < 1:
            return "-"
        while True:
            n, r = divmod(n - 1, 26)
            out += string.ascii_lowercase[r]
            if n <= 0:
                break
        return nato(out[::-1]).upper()

    @transaction.atomic
    def release_draft(self, dry_run=True):
        all = (
            self.versionable_model.objects
            .filter(conference_id=self.conference)
            .prefetch_related(*self.versionable_model.schedule_prefetch_related)
            .select_related(*self.versionable_model.schedule_select_related))
            
        drafts = (
            all
            .filter(version=Scheduleable.DRAFT)
            .order_by('start_datetime'))
        
        sid = transaction.savepoint()

        self._increment_version_counter()
        changes = ChangeList()
        new_released = []
        for draft in drafts.all():
            try:
                released = draft.released
            except self.versionable_model.DoesNotExist:
                released = None

            c = ScheduleableChanges(draft, released)
            if len(c.changes):
                changes.append(c)

            if released is not None:
                released.delete()

            # We now copy the draft to a new released entry, but only if the draft has a datetime schedule
            if draft.to_be_published:
                copy = draft.get_copy()
                new_released.append(copy.id)

        # Now remove any superfluous released events.
        removed_events = all.filter(version=Scheduleable.RELEASED).exclude(id__in=new_released).all()
        for released in removed_events:
            c = ScheduleableChanges(None, released)
            if len(c.changes):
                changes.append(c)
            released.delete()

        self.needs_release = False
        self.save()
        if dry_run:
            transaction.savepoint_rollback(sid)
        else:
            transaction.savepoint_commit(sid)
        return changes

    class Meta:
        abstract = True


class Venue(models.Model):
    name = models.CharField(max_length=100)
    link = models.URLField()
    logo = FilerImageField(related_name='venue_logo', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Coordinate(models.Model):
    conference = models.ForeignKey(Conference, related_name='coordinates', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    location = PointField()
    type = models.SmallIntegerField(choices=CoordinateType.descriptions, default=CoordinateType.MAIN)

    @property
    def icon_options_json(self):
        return repr(CoordinateType.icon_options[self.type])


