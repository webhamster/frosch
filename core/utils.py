from django.conf import settings

from post_office.models import EmailTemplate
from django.utils.translation import get_language, override
from django.utils.translation.trans_real import get_supported_language_variant
from icalendar import Calendar, Event, vText

    
def find_template(template, lang=None):
    if lang is None:
        lang = get_language()
    supported_lang = get_supported_language_variant(lang)

    base_template = EmailTemplate.objects.get(
        name=template,
        language=''
    )
    try:
        template_obj = base_template.translated_templates.get(
            language=supported_lang
        )
        lang_selected = supported_lang
    except EmailTemplate.DoesNotExist:
        template_obj = base_template
        lang_selected = get_language()
    return template_obj, override(lang_selected)


def get_initial_lang():
    try:
        return get_supported_language_variant(get_language())
    except LookupError:
        return settings.LANGUAGE_CODE


def make_ical(scheduleable_set):
    cal = Calendar()
    for s in scheduleable_set.all():
        event = Event()
        event.add('dtstart', s.start_datetime)
        event.add('dtend', s.end_datetime)
        event.add('summary', str(s))
        event.add('description', str(s))
        event.add('location', str(s.room))
        i = s.saved_draft_id or s.draft_id or s.id
        event['uid'] = f"{s.__class__.__name__.upper()}_{i}_{s.conference.url_snippet.upper()}"
        cal.add_component(event)
    return cal.to_ical()
