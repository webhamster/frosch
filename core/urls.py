from django.conf.urls import url
from core import views
from django.views.decorators.cache import cache_page
from django.conf import settings
from .sitemaps import StaticCoreViewSitemap, PeopleDetailViewSitemap

app_name = 'core'

sitemaps = {
    'core_static': StaticCoreViewSitemap(),
    'core_people_detail': PeopleDetailViewSitemap(),
}

urlpatterns = [
    url(
        r'^people/$',
        views.PeoplePage.as_view(),
        name='people'),
    url(
        r'^people/(?P<user_id>[^/]+)$',
        views.PeopleDetailPage.as_view(),
        name='user'),
    url(
        r'^contributions/$',
        views.ContributionsPage.as_view(),
        name='contributions'),
    url(
        r'^archive/',
        cache_page(settings.FROSCH_CACHE_TIMEOUT)(views.RedirectToMostRecentArchivedConference.as_view()),
        name='archive'),
    url(
        r'^contact/$', views.Contact.as_view(), name='contact'),
]
