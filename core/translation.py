from modeltranslation.translator import register, TranslationOptions
from core.models import Conference, Room, ConferenceHighlight, Coordinate


@register(Conference)
class ConferenceTranslationOptions(TranslationOptions):
    fields = ('long_name', 'short_name', 'intro_text', 'venue_text', 'schedule_text', )


@register(Room)
class RoomTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(ConferenceHighlight)
class ConferenceHighlightTranslationOptions(TranslationOptions):
    fields = ('title', 'description', )

@register(Coordinate)
class CoordinateTranslationOptions(TranslationOptions):
    fields = ('name', 'description', )

