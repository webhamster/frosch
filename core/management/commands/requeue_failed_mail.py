from django.core.management.base import BaseCommand
from django.db.models import Count
from post_office.models import Email, STATUS
import logging
from logging.config import dictConfig

# Taken from https://github.com/nvie/rq/blob/master/rq/logutils.py
def setup_loghandlers(level=None):
    # Setup logging for requeue_failed_mail if not already configured
    logger = logging.getLogger('requeue_failed_mail')
    if not logger.handlers:
        dictConfig({
            "version": 1,
            "disable_existing_loggers": False,

            "formatters": {
                "requeue_failed_mail": {
                    "format": "[%(levelname)s]%(asctime)s PID %(process)d: %(message)s",
                    "datefmt": "%Y-%m-%d %H:%M:%S",
                },
            },

            "handlers": {
                "requeue_failed_mail": {
                    "level": "DEBUG",
                    "class": "logging.StreamHandler",
                    "formatter": "requeue_failed_mail"
                },
            },

            "loggers": {
                "requeue_failed_mail": {
                    "handlers": ["requeue_failed_mail"],
                    "level": level or "DEBUG"
                }
            }
        })
    return logger

logger = setup_loghandlers()

class Command(BaseCommand):
    help = 'Requeue failed mail sent with post_office.'

    def add_arguments(self, parser):
        parser.add_argument(
            '--max-attempts',
            type=int,
            dest='max_attempts',
            default=5,
            help='Number of attempts to try to send mail.',
        )
    
    def handle(self, *args, **options):
        failed_mail = Email.objects.filter(status=STATUS.failed).annotate(num_attempts=Count('logs'))
        requeue_mail = failed_mail.filter(num_attempts__lt=options['max_attempts'])
        requeue_mail_count = requeue_mail.count()
        
        if requeue_mail_count:
            logger.info('Requeuing %i previously failed mails' % requeue_mail_count)
            requeue_mail.update(status=STATUS.queued)
