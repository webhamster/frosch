from django.core.management.base import BaseCommand
from post_office.models import EmailTemplate
import glob
import re
import os
from django.conf import settings


class Command(BaseCommand):
    help = 'Load templates from apps/templates/email/ and put them into the database for post_office to use.'

    FOOTER_TEMPLATE = '{settings.BASE_DIR}/core/templates/email/footer_{lang}.email'
    LANG_TEMPLATE_SEARCH = '{settings.BASE_DIR}/*/templates/email/*_{lang}.email'
    OTHER_TEMPLATE_SEARCH = '{settings.BASE_DIR}/*/templates/email/*.email'
    MATCH_NAME = '{settings.BASE_DIR}/([^/]+)/templates/email/([^/]+?)(?:_{lang})?.email'

    done = []

    def handle(self, *args, **options):
        base_language = settings.LANGUAGE_CODE
        print("Loading email templates.")
        print(f"Step 1: loading base language templates (language code {base_language}=settings.LANGUAGE_CODE")
        self.load_templates(base_language, search=self.LANG_TEMPLATE_SEARCH, is_base=True)
        print(f"Step 2: loading templates for other languages.")
        for lang, _ in settings.LANGUAGES:
            if lang == base_language:
                continue
            self.load_templates(lang, search=self.LANG_TEMPLATE_SEARCH)
        print(f"Step 3: loading language-independent templates")
        self.load_templates('other', search=self.OTHER_TEMPLATE_SEARCH, is_base=True)
        
    def load_templates(self, language, search, is_base=False):
        print(f"> Language code {language}")
        footer_template_path = self.FOOTER_TEMPLATE.format(settings=settings, lang=language)
        if os.path.exists(footer_template_path):
            print(f" - reading footer from {footer_template_path}")
            with open(footer_template_path) as f:
                footer = f.read()
                self.done.append(footer_template_path)
        else:
            print(f" - not using footer (file does not exist: {footer_template_path})")
            footer = ''
        
        template_path = search.format(settings=settings, lang=language)
        templates = glob.iglob(template_path)
        print(f" - template search path: {template_path}")
        
        for t in templates:
            if t == footer_template_path:
                continue
            if t in self.done:
                continue
            self.done.append(t)
            print(f" - found {t}")
            with open(t, 'r') as f:
                content = f.read()
            if '{% block ' in content:  # migrating old-style templates
                print(f" - migrating from old-style FROSCH template to new style")
                content = re.sub(
                    r"""^.*\{% block _?subject %\}(.*)\{% endblock %\}.*"""
                    r"""\{% block _?plain(?:_..)? %\}(.*)\{% endblock(?:_..)? %\}.*$""",
                    self.dump,
                    content,
                    flags=re.DOTALL
                )
                with open(t, 'w') as f:
                    f.write(content)
                
            subject, body = content.split('\n', maxsplit=1)
            app, name = re.findall(
                self.MATCH_NAME.format(settings=settings, lang=language),
                t
            )[0]
            output_name = f"{app}.{name}"

            if is_base:
                template, _ = EmailTemplate.objects.get_or_create(
                    name=output_name,
                    language='',
                )
            else:
                base = EmailTemplate.objects.get(
                    name=output_name,
                    language='',
                )
                template, _ = base.translated_templates.get_or_create(
                    language=language
                )
            template.subject = subject
            template.content = body + footer
            template.save()
            print(f" - created {output_name}")
            

    @staticmethod
    def dump(match):
        return match.group(1).strip() + "\n" + match.group(2).strip() + "\n"
