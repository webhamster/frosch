import json

from django.contrib import admin
from core.models import Conference, ConferenceDay
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponse
from django.conf.urls import url
from django.urls import reverse
from django.shortcuts import render, redirect
from core.view_utils import ScheduleLayer, ConferenceScheduleView
from django.urls import reverse


class ConferenceListFilter(admin.SimpleListFilter):
    title = "Conference"
    parameter_name = 'conference'
    
    def lookups(self, request, model_admin):
        return [('*', _('All')), (None, _('Latest'))] + [(str(x.id), x.short_name) for x in Conference.objects.all()]

    def choices(self, changelist):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == lookup,
                'query_string': changelist.get_query_string({
                    self.parameter_name: lookup,
                }, []),
                'display': title,
            }
    
    def queryset(self, request, queryset):
        if self.value() == '*':
            return queryset
        if self.value():
            return queryset.filter(conference=self.value())
        conf = ConferenceDay.objects.latest('date').conference
        return queryset.filter(conference=conf)


class AdminScheduleView():
    def __init__(self, parent, model, template, url, passive_model=None):
        self.parent = parent
        self.model = model
        self.passive_model = passive_model
        self.template = template
        self.url = url
        self.name = 'schedule'

    def actions(self):
        return ()
    
    def links(self, conference):
        return (
            (
                self.model._meta.verbose_name_plural,
                'list',
                (
                    reverse(
                        f'admin:{self.model._meta.app_label}_{self.model._meta.model_name}_changelist')
                    + f"?conference={conference.id}"
                )
            ),
            (
                self.model._meta.verbose_name_plural,
                self.name,
                reverse('admin:' + self.url, args=[conference.id])
            ),
        )
    
    def urls(self):
        my_urls = [
            url(r'(.*)/' + self.url + '$',
                self.parent.admin_site.admin_view(self.view),
                name=self.url)]
        return my_urls

    def view(self, request, conference_id):
        conference = (
            Conference.objects
            .prefetch_related('rooms', 'conferenceday_set')
            .get(id=conference_id))
        scheduleables = (self.model.objects
                         .filter(
                             conference_id=conference.id,
                             version=self.model.DRAFT)
                         .prefetch_related(*self.model.schedule_prefetch_related)
                         .select_related(*self.model.schedule_select_related))

        job_type_id = request.GET.get('job_type_id', None)
        if self.model.__name__ == 'Job':
            from staffregistration.models import JobType
            job_types = JobType.objects.filter(
                conference=conference
            ).order_by('name').all()
            if job_type_id is None:
                return redirect(
                    reverse(
                        'admin:' + self.url,
                        args=[conference_id],
                    ) + f"?job_type_id={job_types[0].id}"
                )
            # this filter works only for jobs
            scheduleables = scheduleables.filter(job_type_id=job_type_id)
            
        active_layer = ScheduleLayer(scheduleables.exclude(length=None))
        not_scheduleable = scheduleables.filter(length=None)
        if self.passive_model is not None:
            pl = (self.passive_model.objects
                  .filter(
                      conference_id=conference.id,
                      version=self.passive_model.DRAFT)
                  .prefetch_related(*self.passive_model.schedule_prefetch_related)
                  .select_related(*self.passive_model.schedule_select_related))

            passive_layers = [ScheduleLayer(pl), ]
        else:
            passive_layers = []
            
        scheduleview = ConferenceScheduleView(
            conference,
            conference.conferenceday_set,
            conference.rooms.order_by('sort_key', 'name'),
            active_layer=active_layer,
            passive_layers=passive_layers,
            editable=True)

        if request.POST:
            return HttpResponse(
                json.dumps(scheduleview.handle_post(request)),
                content_type='application/json')
        elif request.META.get('HTTP_ACCEPT').startswith('application/json'):
            return HttpResponse(
                json.dumps(scheduleview.get_initial_conflicts_json()),
                content_type='application/json')
        else:
            context = {
                'scheduleset': scheduleview,
                'not_scheduleable': not_scheduleable,
                'url': 'admin:' + self.url,
                'conference': conference,
            }
            if self.model.__name__ == 'Job':
                context.update({
                    'job_types': job_types,
                    'current_job_type_id': int(job_type_id),
                })
            return render(request, self.template, context)

