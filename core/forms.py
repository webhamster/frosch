import re

from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import ugettext_lazy as _

from captcha.fields import CaptchaField
from pagedown.widgets import PagedownWidget

from core.models import User, UserAvailability

from easy_thumbnails.widgets import ImageClearableFileInput


class AbstractUserChangeForm(forms.ModelForm):
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
        'twitter_illegal': _("The twitter account name contains illegal characters."),
    }
    password1 = forms.CharField(label=_("Password"),
                                widget=forms.PasswordInput,
                                required=True)
    password2 = forms.CharField(label=_("Password confirmation"),
                                widget=forms.PasswordInput,
                                help_text=_("Enter the same password as above, for verification."),
                                required=True)

    class Meta:
        meta = True
        
    def clean_twitter(self):
        twitter = self.cleaned_data.get('twitter')
        if twitter is None:
            return ""
        twitter = twitter.strip()
        if twitter.lower().startswith('http://') or twitter.lower().startswith('https://'):
            twitter = twitter.split('/')[-1]
        if twitter.startswith('@'):
            twitter = twitter[1:]
        if not re.match(r'^[a-zA-Z0-9_]*$', twitter):
            raise forms.ValidationError(
                self.error_messages['twitter_illegal'],
                code='twitter_illegal'
            )
        return twitter
        
    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        user = super(AbstractUserChangeForm, self).save(commit=False)
        if self.cleaned_data['password1']:
            user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

    
class UserChangeForm(AbstractUserChangeForm):
    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)
        self.fields['password1'].required = False
        self.fields['password2'].required = False
        self.fields['password1'].label = _("Change password (empty = unchanged)")
        self.fields['password2'].label = _("Password confirmation (empty = unchanged)")
        print (self.fields['image'].widget.template_name)
        
    class Meta:
        model = User
        fields = (
            "username",
            "email",
            "affiliation",
            "image",
            "phone",
            "twitter",
            "preferred_language",
            "about",
            "notes",
            "password1",
            "password2")
        widgets = {
            'about': PagedownWidget(),
#            'image': ImageClearableFileInput(),
        }

        
class UserPasswordResetForm(AbstractUserChangeForm):
    class Meta:
        model = User
        fields = ("password1", "password2")

    def save(self, commit=True):
        user = super(UserPasswordResetForm, self).save(commit=False)
        user.password_reset_token = ''
        if commit:
            user.save()
        return user


class UserCreationForm(AbstractUserChangeForm):
    captcha = CaptchaField(help_text=_("Please solve this equation to prove you're not a bot."))

    class Meta:
        model = User
        fields = ("username", "email", "phone", "twitter", "password1", "password2")

    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_active = False
        user.generate_secret_token()

        if commit:
            user.save()
        return user

    
class CustomTimeInput(forms.TimeInput):
    input_type = 'time'
    
    
class UserAvailabilityForm(forms.ModelForm):
    class Meta:
        model = UserAvailability
        fields = ('not_available', 'start', 'end')
        widgets = {
            'start': CustomTimeInput(
                attrs={'class': 'form-control'},
                format='%H:%M'),
            'end': CustomTimeInput(
                attrs={'class': 'form-control'},
                format='%H:%M'),
            'not_available': forms.Select(
                attrs={'class': 'custom-select'},
                choices=(
                    ('False', _("available")),
                    ('True', _("not available")),
                )
            )
        }
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance is not None:
            print(self.instance)
            self.fields['not_available'].initial = 'True' if self.instance.not_available else 'False'
    """


class ContactForm(forms.Form):
    name = forms.CharField(label=_('Your name'), max_length=100)
    email = forms.EmailField(label=_('Your e-mail address'))
    message = forms.CharField(label=_('Your message'), widget=forms.Textarea)
    url = forms.CharField(
        max_length=4096, required=False, widget=forms.HiddenInput)
    conference_id = forms.CharField(
        max_length=256, required=False, widget=forms.HiddenInput)
