from django.conf.urls import url
from core import views

app_name = 'core'

urlpatterns = [
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^register/$', views.register, name='register'),
    url(r'^thanks/$', views.thanks, name='thanks'),
    url(r'^unlock/(?P<user_id>\d+)/(?P<user_secret_token>[0-9a-zA-Z]+)/$', views.unlock, name='unlock'),
    url(r'^reset-password/$', views.reset_password, name='reset-password'),
    url(r'^new-password/(?P<user_id>\d+)/(?P<user_password_reset_token>[0-9a-zA-Z]+)/$',
        views.new_password, name='new-password'),
]
