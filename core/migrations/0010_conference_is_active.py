# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_auto_20151016_1914'),
    ]

    operations = [
        migrations.AddField(
            model_name='conference',
            name='is_active',
            field=models.BooleanField(default=False, verbose_name=b'Make conference publicly visible'),
        ),
    ]
