# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-02-01 19:55
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.FILER_IMAGE_MODEL),
        ('core', '0031_auto_20180201_1453'),
    ]

    operations = [
        migrations.AddField(
            model_name='conference',
            name='venue_image',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='venue_image', to=settings.FILER_IMAGE_MODEL),
        ),
        migrations.AddField(
            model_name='conference',
            name='venue_text',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='conference',
            name='venue_text_de',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='conference',
            name='venue_text_en',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='conference',
            name='intro_image',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='intro_image', to=settings.FILER_IMAGE_MODEL),
        ),
        migrations.AlterField(
            model_name='coordinate',
            name='conference',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='coordinates', to='core.Conference'),
        ),
        migrations.AlterField(
            model_name='coordinate',
            name='type',
            field=models.SmallIntegerField(choices=[(0, 'Conference location'), (1, 'Other'), (2, 'Parking'), (3, 'Entrance'), (4, 'Public Transportation')], default=0),
        ),
    ]
