# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-02-09 09:55
from __future__ import unicode_literals

from django.db import migrations
import django.db.models.deletion
import filer.fields.folder


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0007_auto_20161016_1055'),
        ('core', '0040_pagemount_footer'),
    ]

    operations = [
        migrations.AddField(
            model_name='conference',
            name='gallery',
            field=filer.fields.folder.FilerFolderField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='filer.Folder'),
        ),
    ]
