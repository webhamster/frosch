# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_user_twitter'),
    ]

    operations = [
        migrations.AlterField(
            model_name='useravailability',
            name='end',
            field=models.TimeField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='useravailability',
            name='start',
            field=models.TimeField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
