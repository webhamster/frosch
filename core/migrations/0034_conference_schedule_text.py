# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-02-04 18:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0033_auto_20180203_2125'),
    ]

    operations = [
        migrations.AddField(
            model_name='conference',
            name='schedule_text',
            field=models.TextField(blank=True, null=True),
        ),
    ]
