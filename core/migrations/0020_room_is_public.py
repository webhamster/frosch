# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-28 15:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0019_auto_20171227_1526'),
    ]

    operations = [
        migrations.AddField(
            model_name='room',
            name='is_public',
            field=models.BooleanField(default=True),
        ),
    ]
