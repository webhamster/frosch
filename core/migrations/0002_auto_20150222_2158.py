# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='preferred_language',
            field=models.CharField(default=core.models.get_initial_lang, help_text='Used for emails.', max_length=10, verbose_name='Preferred language', choices=[(b'de', 'German'), (b'en', 'English')]),
            preserve_default=True,
        ),
    ]
