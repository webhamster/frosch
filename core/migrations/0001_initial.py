# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import django.utils.translation
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(default=django.utils.timezone.now, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(help_text='Required. Default: Firstname Lastname. Alternative: Nickname. Public.', unique=True, max_length=50, verbose_name='public name', error_messages={b'unique': 'A user with that username already exists.'})),
                ('email', models.EmailField(help_text='Required. We will send you notifications to this address, but no spam.', unique=True, max_length=75, verbose_name='email address')),
                ('phone', models.CharField(help_text='Required. We need your phone number in case there are any last minute changes to the schedule.', max_length=30, verbose_name='phone number')),
                ('about', models.TextField(help_text='Optional. Public. Use Markdown for formatting. If you plan to submit a contribution, describe yourself here.', null=True, verbose_name='about me', blank=True)),
                ('notes', models.TextField(help_text='Optional. Only visible to admins. Any notes on your person that do not fit anywhere else.', null=True, verbose_name='notes', blank=True)),
                ('secret_token', models.CharField(max_length=15, null=True, blank=True)),
                ('password_reset_token', models.CharField(max_length=15, null=True, blank=True)),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('preferred_language', models.CharField(default=django.utils.translation.get_language, help_text='Used for emails.', max_length=10, verbose_name='Preferred language', choices=[(b'de', 'German'), (b'en', 'English')])),
                ('groups', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of his/her group.', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Conference',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('long_name', models.CharField(max_length=100)),
                ('long_name_de', models.CharField(max_length=100, null=True)),
                ('long_name_en', models.CharField(max_length=100, null=True)),
                ('short_name', models.CharField(max_length=20)),
                ('short_name_de', models.CharField(max_length=20, null=True)),
                ('short_name_en', models.CharField(max_length=20, null=True)),
                ('url_snippet', models.SlugField(unique=True, max_length=20)),
                ('description', models.TextField()),
                ('description_de', models.TextField(null=True)),
                ('description_en', models.TextField(null=True)),
                ('notification_email_addresses', models.TextField(help_text='Separate multiple addresses with commas.')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ConferenceDay',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('start', models.TimeField()),
                ('end', models.TimeField()),
                ('conference', models.ForeignKey(to='core.Conference')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserAvailability',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start', models.TimeField()),
                ('end', models.TimeField()),
                ('start_datetime', models.DateTimeField(null=True, editable=False, blank=True)),
                ('end_datetime', models.DateTimeField(null=True, editable=False, blank=True)),
                ('date', models.ForeignKey(to='core.ConferenceDay')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
