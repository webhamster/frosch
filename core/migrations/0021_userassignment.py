# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-01-03 21:29
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0020_room_is_public'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserAssignment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('roles', models.TextField()),
                ('notes', models.TextField(blank=True, null=True)),
                ('locked', models.BooleanField(default=True)),
                ('conference', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Conference')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
