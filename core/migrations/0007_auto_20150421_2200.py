# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20150420_2226'),
    ]

    operations = [
        migrations.AddField(
            model_name='useravailability',
            name='not_available',
            field=models.BooleanField(default=False, verbose_name='Not available on this day'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='useravailability',
            name='end',
            field=models.TimeField(null=True, verbose_name='Available until', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='useravailability',
            name='start',
            field=models.TimeField(null=True, verbose_name='Available from', blank=True),
            preserve_default=True,
        ),
    ]
