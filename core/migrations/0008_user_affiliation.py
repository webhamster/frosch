# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20150421_2200'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='affiliation',
            field=models.CharField(max_length=60, null=True, verbose_name='affiliation', blank=True),
            preserve_default=True,
        ),
    ]
