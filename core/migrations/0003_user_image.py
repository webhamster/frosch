# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20150222_2158'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='image',
            field=models.ImageField(help_text='Optional. Will be used in official schedule.', null=True, upload_to=b'user_images', blank=True),
            preserve_default=True,
        ),
    ]
