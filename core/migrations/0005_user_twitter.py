# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_remove_user_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='twitter',
            field=models.CharField(help_text='Optional. Public.', max_length=140, null=True, verbose_name='twitter account', blank=True),
            preserve_default=True,
        ),
    ]
