# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-12 21:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0016_auto_20171208_0944'),
    ]

    operations = [
        migrations.AlterField(
            model_name='useravailability',
            name='end_datetime',
            field=models.DateTimeField(blank=True, db_index=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='useravailability',
            name='start_datetime',
            field=models.DateTimeField(blank=True, db_index=True, editable=False, null=True),
        ),
    ]
