$(document).ready(function() {
    var modal = $('#eventModal');
    
    $('.public-schedule a.eventlink').on('click', function() {
        var target = $(this).attr('href') + "/popup";
        showModalFromURL(target);
        return false;
    });

    function showModalFromURL(url) {
        
        modal.find('.modal-dialog').load(url + " .event", null, function() {
            modal.modal('show');
        });
        
    }

    $(modal).on('hide.bs.modal', function() {
        console.debug('remove');
        modal.find('.modal-dialog').html('');
    });
    
});
