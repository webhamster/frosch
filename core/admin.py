from callforcontributions.admin import (
    MatrixAdminExtension,
    ConferenceEventSettingsInline,
    PrintScheduleAdminExtension,
    PrintBookletAdminExtension,
)
from visitorregistration.admin import (
    ConferenceRegistrationInline,
)
from callforcontributions.models import Event, EventStatus, ConferenceEventSettings
from callforcontributions.utils import ScheduleDumper
from core.models import (Conference, ConferenceDay, Room, User,
                         UserAvailability, UserAssignment, Venue, ConferenceHighlight, Coordinate)
from core.admin_utils import AdminScheduleView, ConferenceListFilter
from django import forms
from django.conf import settings
from django.contrib import admin
from django.db.models import Q, Min
from django.http import HttpResponse
from django.utils.html import mark_safe
from pagedown.widgets import AdminPagedownWidget
from staffregistration.admin import (
    ConferenceStaffSettingsInline,
    PrintStaffOverviewAdminExtension
)
from staffregistration.admin_utils import JobGeneratorAdminExtension
from staffregistration.models import Job, ConferenceStaffSettings
from modeltranslation.admin import TranslationAdmin, TranslationStackedInline, TranslationTabularInline
from django.utils.text import Truncator
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.shortcuts import get_object_or_404, render
from django.conf.urls import url
from hijack_admin.admin import HijackUserAdminMixin
from leaflet.admin import LeafletGeoAdminMixin


class ConferenceDayInline(admin.TabularInline):
    model = ConferenceDay
    extra = 1

    
class ConferenceHighlightInline(TranslationStackedInline):
    model = ConferenceHighlight
    extra = 1

    
class ConferenceAdminForm(forms.ModelForm):
    class Meta:
        widgets = {
         
        }
        widgets.update(dict(
            ("%s_%s" % (name, lang), AdminPagedownWidget())
            for name in ['description', 'intro_text', 'venue_text', 'schedule_text', ]
            for lang, _ in settings.LANGUAGES
        ))

        
class UserAdminForm(forms.ModelForm):
    class Meta:
        widgets = {
            'about': AdminPagedownWidget()
        }


class UserAssignmentGeneratorAdminExtension:
    def actions(self):
        def generate(self, request, queryset):
            for conference in queryset:
                UserAssignment.generate_for_conference(conference)
            messages.success(request, "User list(s) generated.")
        generate.short_description = 'Generate user assignment list'
        return (generate, )


class ScheduleDumperAdminExtension:
    def actions(self):
        def dump(self, request, queryset):
            for conference in queryset:
                sd = ScheduleDumper(conference)
                sd.dump()
                messages.success(request, f"Dumped schedule for {conference.short_name}.")
        dump.short_description = 'Dump schedule'
        return (dump, )
    
        
class ReleaseVersionAdminExtension:
    def __init__(self,
                 parent,
                 versionablecontainer_model,
                 template_email,
                 template_preview):
        self.parent = parent
        self.model = versionablecontainer_model
        self.idstring = self.model._meta.label_lower
        self.template_email = template_email
        self.template_preview = template_preview

    def links(self, conference):
        try:
            container = self.model.objects.get(conference_id=conference.id)
        except self.model.DoesNotExist:
            return ()
        dirty = " <b style='color: red;' title='changes detected - needs release'>⚠</b>" if container.needs_release else ""
        links = (
            ( 
                self.model.versionable_model._meta.verbose_name_plural,
                mark_safe(_("release") + dirty),
                reverse(f'admin:versionable_{self.idstring}_changes', args=[conference.id])),
        )
        return links
        
    def urls(self):
        return (
            url(
                f'(.*)/changes/{self.idstring}$',
                self.parent.admin_site.admin_view(self.changes_view),
                name=f"versionable_{self.idstring}_changes"),
        )

    def changes_view(self, request, conference_id):
        container = get_object_or_404(self.model, conference_id=conference_id)
        conference = container.conference
        
        if request.method == 'GET':
            dry_run = True
        elif request.method == 'POST':
            dry_run = False
            
        changelist = container.release_draft(dry_run)

        emails = []
        for user, changes in changelist.views_by_users().items():
            emails.append(user.send_templated_mail(request, self.template_email, {
                'container': container,
                'conference': conference,
                'changelist': changes,
            }, dry_run=dry_run, send_now=False))

        if dry_run:
            return render(request, self.template_preview, {
                'container': container,
                'conference': conference,
                'changelist': changelist,
                'emails': emails,
            })
        else:
            messages.success(request, "New schedule version released, {0} emails sent.".format(len(emails)))
            return HttpResponseRedirect(reverse('admin:core_conference_changelist'))


@admin.register(Conference)
class ConferenceAdmin(TranslationAdmin):
    form = ConferenceAdminForm
    list_display = ('url_snippet', 'is_active', 'long_name', 'contacts', 'custom_links', 'show_versions')
    prepopulated_fields = {"url_snippet": ("short_name",)}
    inlines = [
        ConferenceDayInline,
        ConferenceEventSettingsInline,
        ConferenceStaffSettingsInline,
        ConferenceRegistrationInline,
        ConferenceHighlightInline,
    ]
    actions = []
    _extensions = []
    save_as = True
    save_on_top = True
 
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._extensions.append(MatrixAdminExtension(self))
        
        self._extensions.append(
            AdminScheduleView(
                self,
                model=Event,
                template='callforcontributions/admin/schedule.html',
                url='schedule/events'))

        self._extensions.append(ReleaseVersionAdminExtension(
            self,
            versionablecontainer_model=ConferenceEventSettings,
            template_email='callforcontributions.events-changed',
            template_preview='callforcontributions/admin/changes_events_preview.html'
        ))

        self._extensions.append(ScheduleDumperAdminExtension())

        self._extensions.append(
            AdminScheduleView(
                self,
                model=Job,
                passive_model=Event,
                template='staffregistration/admin/schedule.html',
                url='schedule/jobs'))
        
        self._extensions.append(ReleaseVersionAdminExtension(
            self,
            versionablecontainer_model=ConferenceStaffSettings,
            template_email='staffregistration.jobs-changed',
            template_preview='callforcontributions/admin/changes_events_preview.html'
        ))

        self._extensions.append(UserAssignmentGeneratorAdminExtension())

        self._extensions.append(JobGeneratorAdminExtension())
        
        self._extensions.append(PrintScheduleAdminExtension(self))
        
        self._extensions.append(PrintBookletAdminExtension(self))

        self._extensions.append(PrintStaffOverviewAdminExtension(self))
        
        for e in self._extensions:
            if hasattr(e, 'actions'):
                self.actions.extend(e.actions())

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.annotate(start=Min('conferenceday__date')).order_by('-start')


    def custom_links(self, conference):
        out = {}
        for e in self._extensions:
            if not hasattr(e, 'links'):
                continue
            for category, title, link in e.links(conference):
                if category not in out:
                    out[category] = []
                out[category].append("""<a href="{}">{}</a>""".format(link, title))
            
        return mark_safe(' &middot; '.join(category + ": " + ", ".join(out[category]) for category in out))
    
    def get_urls(self):
        urls = super().get_urls()
        for e in self._extensions:
            if hasattr(e, 'urls'):
                urls.extend(e.urls())
        return urls

    def show_versions(self, conference):
        try:
            c = conference.eventsettings.get_version()
        except ConferenceEventSettings.DoesNotExist:
            c = '-'
        try:
            j = conference.conferencestaffsettings.get_version()
        except ConferenceStaffSettings.DoesNotExist:
            j = "-"
        return f"contributions: {c}, jobs: {j}"
        

    
class UserConferenceListFilter(admin.SimpleListFilter):
    title = "Conference (Confirmed Contributor or Staff Member)"
    parameter_name = 'user_conference'
    
    def lookups(self, request, model_admin):
        return [(x.url_snippet, x) for x in Conference.objects.all()]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(
                Q(event__conference__url_snippet=self.value(), event__status=EventStatus.CONFIRMED) |
                Q(jobs_assigned__conference__url_snippet=self.value())).distinct()
        return queryset


class UserAvailabilityInlineAdmin(admin.TabularInline):
    model = UserAvailability


@admin.register(User)
class UserAdmin(admin.ModelAdmin, HijackUserAdminMixin):
    list_display = (
        'show_name_and_affiliation',
        'email',
        'phone',
        'twitter',
        'date_joined',
        'preferred_language',
        'notes_',
        'hijack_field',
    )
    form = UserAdminForm
    list_filter = (UserConferenceListFilter,)
    search_fields = ('username', 'email', 'twitter', 'affiliation', 'about', 'notes',)
    actions = [
        'show_email_addresses',
    ]
    inlines = [
        UserAvailabilityInlineAdmin,
    ]
    fieldsets = (
        (None, {
            'fields': (('username', 'affiliation'), 'about', 'notes', )
        }),
        ('Contact', {
            'fields': (('email', 'phone', 'twitter',), )
        }),
        ('Internal', {
            'fields': ('is_active', 'is_staff', 'password', 'preferred_language', 'date_joined', 'groups',)
        }),
    )
    ordering = ('username', )
    
    def show_name_and_affiliation(self, user):
        if not user.affiliation:
            return user.username
        else:
            return "%s (%s)" % (user.username, user.affiliation)

    show_name_and_affiliation.short_description = "Name (Affiliation)"
    show_name_and_affiliation.admin_order_field = 'username'
        
    def show_email_addresses(self, request, users):
        emails = set()
        for user in users:
            emails.add(user.email)

        return HttpResponse("\n".join(emails), content_type="text/plain")

    show_email_addresses.short_description = "List email addresses"

    def notes_(self, user):
        return Truncator(user.notes).chars(100)


@admin.register(UserAssignment)
class UserAssignmentAdmin(admin.ModelAdmin):
    actions = ('print_badges', 'print_list',)
    list_filter = (ConferenceListFilter,)
    list_display_links = ('user', )
    list_display = ('conference', 'user', 'custom_badge_name', 'custom_badge_affiliation', 'roles', 'locked', 'notes', )
    list_editable = ('custom_badge_name', 'custom_badge_affiliation', 'roles', 'locked', 'notes', )
    ordering = ('conference', 'user', 'roles', )

    @staticmethod
    def print_badges(cls, request, user_assignments):
        ua = list(user_assignments)
        # fill up badges up to full page
        for x in range(0, (10 - len(user_assignments)) % 10):
            ua.append({})
        return render(request, 'core/admin/badges.html', {
            'assignments': ua,
        })

    print_badges.short_description = "Print Badges"

    @staticmethod
    def print_list(cls, request, user_assignments):
        uas = []
        for ua in user_assignments.order_by('user__username').all():
            ua.ua_availability = ua.user.useravailability_set.filter(
                day__in=ua.conference.conferenceday_set.all()).all()
            uas.append(ua)
        return render(request, 'core/admin/user_assignments_list.html', {
            'assignments': uas,
        })

    print_list.short_description = "Print User List"


@admin.register(Room)
class RoomAdmin(TranslationAdmin):
    list_display = ('id', 'name_de', 'name_en', 'sort_key', 'used_in')
    list_editable = ('name_de', 'name_en', 'sort_key',)
    ordering = ('id',)

    def used_in(self, room):
        return ', '.join(room.conference_set.values_list('short_name', flat=True))


@admin.register(Venue)
class VenueAdmin(admin.ModelAdmin):
    list_display = ('name', 'logo')


class CoordinateAdminForm(forms.ModelForm):
    class Meta:
        widgets = {}
        widgets.update(dict(
            ("%s_%s" % (name, lang), AdminPagedownWidget())
            for name in ['description']
            for lang, _ in settings.LANGUAGES
        ))


@admin.register(Coordinate)
class CoordinateAdmin(LeafletGeoAdminMixin, TranslationAdmin):
    list_display = ('name', 'type', 'conference')
    list_filter = (ConferenceListFilter, )
    form = CoordinateAdminForm
    save_as = True
    save_on_top = True
    fields = (
        'conference',
        'type',
        'location',
        'name',
        'description',
    )


