from django.utils import formats
from django.utils.translation import ugettext_lazy as _
from django.utils.translation.trans_real import get_supported_language_variant
from django.utils import translation, timezone


class Change():
    def str_for_user(self, user):
        return str(self)


class NewScheduleableChange(Change):
    def __str__(self):
        return str(_("was put on the schedule (you are assigned)."))


class RemovedScheduleableChange(Change):
    def __str__(self):
        return str(_("was removed from the schedule."))


class TimeChange(Change):
    @timezone.override(timezone.get_default_timezone())
    def __init__(self, to_start):
        self.to_start = formats.date_format(timezone.localtime(to_start), "SHORT_DATETIME_FORMAT")

    def __str__(self):
        return str(_("was moved to {to_start}.").format(
            to_start=self.to_start
        ))


class DurationChange(Change):
    def __init__(self, duration):
        self.duration = duration

    def __str__(self):
        return str(_("duration was changed to {duration} minutes.").format(
            duration=self.duration
        ))

    
class DayChange(Change):
    def __init__(self, from_day, to_day):
        self.from_day = formats.date_format(from_day.date, "SHORT_DATE_FORMAT")
        self.to_day = formats.date_format(to_day.date, "SHORT_DATE_FORMAT")

    def __str__(self):
        return str(_("was moved from {from_day} to {to_day}.").format(
            from_day=self.from_day,
            to_day=self.to_day
        ))
    
    
class UserChange(Change):
    def __init__(self, user_id, user_username):
        self.user_id = user_id
        self.user_username = user_username

    def __str__(self):
        return self.stringify(self.STRING_3RD_PERSON)

    def str_for_user(self, user):
        if user.id == self.user_id:
            return self.stringify(self.STRING_1ST_PERSON)
        else:
            None

    def stringify(self, string):
        return string.format(
            username=self.user_username
        )

    
class UserRemovedChange(UserChange):
    STRING_1ST_PERSON = _("you are no longer assigned.")
    STRING_3RD_PERSON = _("the user {username} is no longer assigned.")


class UserAddedChange(UserChange):
    STRING_1ST_PERSON = _("you are now assigned.")
    STRING_3RD_PERSON = _("the user {username} is now assigned.")
    
    
class RoomChange(Change):
    def __init__(self, from_room, to_room):
        self.from_room = from_room
        self.to_room = to_room
        
    def __str__(self):
        return _("the room was changed from {from_room} to {to_room}.").format(
            from_room=self.from_room,
            to_room=self.to_room,
        )
    
        
class ScheduleableChanges():
    def __init__(self, draft, released, skip_past_events=True):
        """Draft or released may be None."""
        from core.models import User  # avoid circular import
        self.draft = draft
        self.released = released
        self.changes = []
        self._affected = User.objects.none()
        self._reference = draft
        
        if skip_past_events:
            skip = True
            if released is not None and not released.in_past:
                skip = False
            if draft is not None and not draft.in_past:
                skip = False
            if skip:
                return
                                 
        self.check_changes()
        # Now: freeze affected users and _reference event, since released/draft may be changed/deleted.
        self.affected_persons = list(self._affected.distinct().all())

        if self._reference.has_datetime:
            start_datetime = formats.date_format(
                timezone.localtime(self._reference.start_datetime), "SHORT_DATE_FORMAT")
            start_datetime += ", "
            start_datetime += formats.time_format(
                timezone.localtime(self._reference.start_datetime), "TIME_FORMAT")
            start_datetime += "-"
            start_datetime += formats.time_format(
                timezone.localtime(self._reference.end_datetime), "TIME_FORMAT")
        elif self._reference.has_date:
            start_datetime = formats.date_format(self._reference.day.date, "SHORT_DATE_FORMAT")
        else:
            start_datetime = "no date/time set"
        self.reference_event_str = f"{self._reference} ({start_datetime}, {self._reference.room})"

    def check_changes(self):
        # If this was never released before:
        if self.released is None:
            if self.draft.to_be_published:
                self.changes.append(NewScheduleableChange())
                self._affected |= self.draft.get_persons_required().all()
                return
            else:
                return
        # If there is a released event already:
        elif self.released.to_be_published:
            # ... and this is still supposed to be released:
            if self.draft is not None and self.draft.to_be_published:
                """We know that there is a draft event that is supposed to be released
                and there exists a released event already. We now have to check the
                details of the events."""
                self.check_changes_timing()
                self.check_changes_persons()
                self.check_changes_room()
            # ... and this is not supposed to be released:
            else:
                self.changes.append(RemovedScheduleableChange())
                self._reference = self.released
                self._affected |= self.released.get_persons_required().all()
                return

    def check_changes_timing(self):
        changed = False
        if self.draft.has_date:
            if self.draft.day_id != self.released.day_id:
                self.changes.append(DayChange(self.released.day, self.draft.day))
                changed = True

        if self.draft.has_datetime:
            if not self.draft.start_datetime == self.released.start_datetime:
                self.changes.append(TimeChange(self.draft.start_datetime))
                changed = True
            if not int(self.draft.length) == int(self.released.length):
                self.changes.append(DurationChange(int(self.draft.length)))
                changed = True

        if changed:
            self._affected |= self.draft.get_persons_required().all()
            self._affected |= self.released.get_persons_required().all()

    def check_changes_persons(self):
        draft_persons = self.draft.get_persons_required()
        released_persons = self.released.get_persons_required()
        
        added_persons = draft_persons.exclude(
            id__in=released_persons.values_list('id', flat=True))
        for p in added_persons.all():
            self.changes.append(UserAddedChange(p.id, p.username))

        removed_persons = released_persons.exclude(
            id__in=draft_persons.values_list('id', flat=True))
        for p in removed_persons.all():
            self.changes.append(UserRemovedChange(p.id, p.username))

        self._affected |= added_persons.all()
        self._affected |= removed_persons.all()

    def check_changes_room(self):
        if self.draft.room != self.released.room:
            self.changes.append(RoomChange(str(self.released.room), str(self.draft.room)))
            self._affected |= self.released.get_persons_required().all()


class ChangeList(list):
    def views_by_users(self):
        """Return a customized views for all users."""
        persons = {}
        orig_lang = translation.get_language()
        for scheduleablechanges in self:
            for u in scheduleablechanges.affected_persons:
                lang = get_supported_language_variant(u.preferred_language)
                translation.activate(lang)
                entry = (
                    scheduleablechanges,
                    [c.str_for_user(u) for c in scheduleablechanges.changes if c.str_for_user(u) is not None]
                )
                if u not in persons:
                    persons[u] = [entry, ]
                else:
                    persons[u].append(entry)
        translation.activate(orig_lang)
        return persons

    def view(self):
        for scheduleablechanges in self:
            yield (
                scheduleablechanges,
                [str(c) for c in scheduleablechanges.changes]
            )
