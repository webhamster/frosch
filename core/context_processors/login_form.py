from django.contrib.auth.forms import AuthenticationForm

def login_form(request):
    if request.user.is_authenticated:
        return {}
    return { 'login_form': AuthenticationForm() }
