from core.forms import ContactForm
from pages.models import Page
from django.conf import settings

def contact_form(request):
    try:
        contact_page = Page.objects.get(name='contact')
        contact_page_content = contact_page.content
    except Page.DoesNotExist:
        contact_page_content = ''

    try:
        imprint_page = Page.objects.get(name='imprint')
        imprint_page_slug = imprint_page.pagemount_set.first().slug
    except Page.DoesNotExist:
        imprint_page_slug = None
        
    conference_id = None
    if request.resolver_match:
        conference_id = request.resolver_match.kwargs.get('conference_id', None)
    
    url = request.build_absolute_uri()
    if request.user is not None and request.user.is_authenticated():
        f = ContactForm(initial={
            'name': request.user.username,
            'email': request.user.email,
            'url': url,
            'conference_id': conference_id,
        })
    else:
        f = ContactForm(initial={
            'url': url,
            'conference_id': conference_id,
        })
    return {'contact_form': f, 'contact_default_email': settings.ADMINS[0][1], 'contact_page_content': contact_page_content, 'imprint_page_slug': imprint_page_slug}
