from core.models import Venue
from django.db.models import Min

def partners(request):
    return {'partners': Venue.objects.filter(conference__is_active=True).annotate(first_date=Min('conference__conferenceday__date')).order_by('first_date').distinct()}
