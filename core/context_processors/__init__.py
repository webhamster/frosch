from .settings import _settings as settings
from .contact_form import contact_form
from .login_form import login_form
from .partners import partners
from .conferences import conferences
