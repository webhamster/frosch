from django.conf import settings
def _settings(request):
    s = dict((name, getattr(settings, name)) for name in dir(settings) if name.startswith('FROSCH_'))
    #print s
    return s
