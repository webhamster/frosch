from core.view_utils import ConferenceQuerysets


def conferences(request):
    return {
        'active_conferences': ConferenceQuerysets.active_conferences(),
        'archived_conferences': ConferenceQuerysets.archived_conferences(),
    }
