from django import template
from django.urls import reverse, resolve
from django.utils import translation
from django.urls.exceptions import Resolver404
from pages.models import PageMount

register = template.Library()


@register.simple_tag(name='translate_url', takes_context=True)
def do_translate_url(context, language):
    if 'translated_urls' in context:
        return context['translated_urls'][language]

    viewname = 'news:index'
    kwargs = {}

    if 'request' in context:
        path = context['request'].path
        if not path == '/':
            try:
                view = resolve(path)
                viewname = view.view_name
                kwargs = view.kwargs
                if viewname in ['page:page', 'page:conference_page'] and 'pagemount_slug' in kwargs:
                    pm = PageMount.objects.filter(slug=kwargs['pagemount_slug']).first()
                    if pm:
                        with translation.override(language):
                            kwargs['pagemount_slug'] = pm.slug
            except Resolver404 as e:
                pass
    with translation.override(language):
        url = reverse(viewname, kwargs=kwargs)
    return url
