from django import template
import re

register = template.Library()


YOUTUBE_MATCHER = re.compile(r'.*(?:youtube\.com\/\S*(?:(?:\/e(?:mbed))?\/|watch\?(?:\S*?&?v\=))|youtu\.be\/)([a-zA-Z0-9_-]{6,11})')
#YOUTUBE_MATCHER = re.compile(r'.*(youtube)')

YOUTUBE_URL = "https://www.youtube.com/embed/%(yt_id)s"

@register.inclusion_tag('includes/video_embed.html')
def video_embed(video_url):
    yt_m = YOUTUBE_MATCHER.search(video_url)

    if yt_m:
        yt_id = yt_m.group(1)
        
        context = { 'youtube_url': YOUTUBE_URL % {'yt_id': yt_id, }, }

    else:
        context = { 'video_url': video_url, }

    return context
