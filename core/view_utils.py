import math

from datetime import date, timedelta, datetime
from django.db.models import Q, Max
from core.forms import UserAvailabilityForm
from core.models import UserAvailability
from core.models import Conference
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from active_login_required import active_login_required
from django.shortcuts import redirect
from django.conf import settings
from django.utils import translation
from django.urls import reverse, resolve
from django.core.cache import cache


class ConferenceQuerysets():
    def active_conferences():
        query = Conference.objects.filter(is_active=True, conferenceday__date__gte=datetime.now(), conferenceday__public_start__isnull=False).annotate(end=Max('conferenceday__date')).order_by('end').all
        return cache.get_or_set('frosch_active_conferences', query, settings.FROSCH_CACHE_TIMEOUT)

    def archived_conferences():
        query = Conference.objects.filter(is_active=True, conferenceday__date__lt=datetime.now(), conferenceday__public_start__isnull=False).annotate(end=Max('conferenceday__date')).order_by('end').all
        return cache.get_or_set('frosch_archived_conferences', query, settings.FROSCH_CACHE_TIMEOUT)


class UserAvailabilityViewMixin():
    prefix = 'availability'
    exclude_user_days = Q(date__lt=date.today())
    
    def get_availability_forms(self, request, conference):
        forms = []
        has_full_availability = True
        for d in conference.conferenceday_set.exclude(self.exclude_user_days).order_by('date').all():
            i, created = UserAvailability.objects.get_or_create(
                user=request.user,
                day=d)
            if created:
                has_full_availability = False
            form = UserAvailabilityForm(
                request.POST if request.method == 'POST' else None,
                prefix=self.prefix + '-day-{}'.format(d.id),
                instance=i)
            forms.append((d, form))
        return forms, has_full_availability
    
    @method_decorator(active_login_required)
    def post(self, request, conference_id):
        conference = get_object_or_404(Conference, url_snippet=conference_id)
        forms, __ = self.get_availability_forms(request, conference)
        errors = False
        for day, form in forms:
            if form.is_valid():
                form.save()
            else:
                errors = True
        if errors:
            messages.error(request, _("There have been some problems with your availability data."))
        else:
            messages.success(request, _("Thanks for submitting your availability."))
        
        return redirect(self.return_to, conference_id=conference_id)


class ScheduleLayer():
    def __init__(self, objects, augmentation_callback=None):
        self.objects = objects
        self.model = self.objects.model
        self.meta = self.model._meta
        self.template = 'core/mixins/schedule-{}.html'.format(self.meta.model_name)
        self.augmentation_callback = augmentation_callback

    def augment(self, instance):
        if self.augmentation_callback is not None:
            self.augmentation_callback(instance)

        
class ConferenceScheduleView():
    step = settings.FROSCH_SCHEDULE_STEP
    bigstep = settings.FROSCH_SCHEDULE_BIG_STEP
    
    def __init__(
            self,
            conference,
            days,
            rooms,
            active_layer,
            passive_layers=[],
            editable=False,
            public_times=False):
        self.conference = conference
        self.days = days
        self.rooms = rooms
        
        self.active_layer = active_layer
        self.passive_layers = passive_layers
        
        self.editable = editable
        self.public_times = public_times
        self.schedule_views = {day.id: ConferenceDayScheduleView(self, day) for day in days.all()}

    def get_days(self):
        return self.schedule_views.values()

    def get_initial_conflicts_json(self):
        out = {}
        for elem in self.active_layer.objects.all():
            out.update(elem.get_conflicts().as_dict())
        return out

    def handle_post(self, request):
        scheduleable = self.active_layer.objects.get(id=request.POST['id'])
        copy = request.POST['copy'] == '1'
        if copy:
            scheduleable.pk = None  # copy object
            scheduleable.auto_generated = False  # copied objects are never auto-generated
            
        if not copy:
            before_conflict_targets = scheduleable.get_conflicts().get_conflicting(self.active_layer.model)
            
        day = int(request.POST['day'])
        if day == 0:
            scheduleable.day = None
            scheduleable.time = None
            scheduleable.room = None
            scheduleable.save()
        else:
            self.schedule_views[day].change_scheduleable(scheduleable, request.POST)
            
        if copy:  # When an event was copied, the whole schedule is reloaded anyway
            return {}

        new_conflicts = scheduleable.get_conflicts()
        new_conflict_targets = new_conflicts.get_conflicting(self.active_layer.model)
        new_conflicts_dict = new_conflicts.as_dict()
        
        for e in before_conflict_targets | new_conflict_targets:
            new_conflicts_dict.update(self.active_layer.model.objects.get(id=e).get_conflicts().as_dict())

        return new_conflicts_dict

    def get_unused_active_scheduleables(self):
        for e in self.active_layer.objects.filter(
                Q(day=None) | Q(start_datetime=None) |
                (~Q(room_id__in=self.rooms.values_list('id', flat=True)) & ~Q(room_id=None)),
                length__gt=0).all():
            e.length_ = math.ceil(int(e.length) / self.step)
            yield e

    
class ConferenceDayScheduleView():
    def __init__(self, parent, day):
        self.parent = parent
        self.day = day
        self.rooms = list(self.parent.rooms.all())
        self.content_columns = len(self.rooms)
        if self.parent.public_times:
            self.start = self.day.get_public_start_datetime()
            self.end = self.day.get_public_end_datetime()
        else:
            self.start = self.day.get_start_datetime()
            self.end = self.day.get_end_datetime()
        self.content_rows = math.ceil(
            ((self.end - self.start).seconds / 60) / self.parent.step)

    def next_big_step(self, dt):
        dt = dt - timedelta(seconds=dt.second)
        minutes = dt.minute
        target_minutes = math.ceil(float(minutes) / self.parent.bigstep) * self.parent.bigstep
        diff = target_minutes - minutes
        return dt + timedelta(minutes=diff)

    def get_row_from_time(self, time):
        diff = time - self.start
        return math.floor((diff.seconds / 60) / self.parent.step + 1)

    def get_column_from_room(self, room):
        if room in self.rooms:
            return self.rooms.index(room), 1
        else:
            return 0, len(self.rooms)

    def get_room_from_column(self, column):
        return self.rooms[column]

    def get_time_from_row(self, row):
        return self.start + timedelta(minutes=self.parent.step * (row - 1))

    def get_time_markers(self):
        start = self.start
        end = self.end
        i = self.next_big_step(start)
        while i <= end:
            s = ScheduleTimeMarker()
            s.row = self.get_row_from_time(i)
            s.time = i
            yield s
            i += timedelta(minutes=self.parent.bigstep)

    def get_background_elements(self):
        return range(self.content_rows * self.content_columns)

    def get_active_scheduleables(self):
        yield from self.get_scheduleables(self.parent.active_layer)
    
    def get_passive_scheduleables(self):
        for layer in self.parent.passive_layers:
            yield (layer, self.get_scheduleables(layer))

    def get_scheduleables(self, layer):
        for e in layer.objects.filter(
                ~Q(start_datetime=None),
                Q(room__in=self.rooms) | Q(room_id=None),
                day=self.day,
                length__gt=0).all():
            e.row = self.get_row_from_time(e.start_datetime)
            e.column, e.column_span = self.get_column_from_room(e.room)
            e.length_ = math.ceil(int(e.length) / self.parent.step)
            layer.augment(e)
            yield e

    def has_active_scheduleables(self):
        if self.parent.active_layer.objects.filter(
                ~Q(start_datetime=None),
                Q(room__in=self.rooms) | Q(room_id=None),
                day=self.day,
        length__gt=0).count():
            return True
        return False

    def change_scheduleable(self, s, data):
        assert(self.parent.editable)
        s.set_start_datetime(self.get_time_from_row(int(data['row'])))
        s.room = self.get_room_from_column(int(data['column']))
        s.save()

        
class ScheduleTimeMarker():
    row = 0
    time = None
    offset = True

    
class DetailViewTranslationURLProviderMixin():

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        view = resolve(self.request.path)

        translated_urls = {}
        for lang, _ in settings.LANGUAGES:
            obj = self.get_object()
            with translation.override(lang):
                url = reverse(view.view_name, kwargs={self.slug_url_kwarg: getattr(obj, self.slug_field)})
            translated_urls[lang] = url

        context['translated_urls'] = translated_urls
        return context

    
class PreviousConferenceMixin():

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            context['previous_conference'] = ConferenceQuerysets.archived_conferences().reverse()[0]
        except IndexError:
            pass
        return context
