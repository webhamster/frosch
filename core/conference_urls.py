from django.conf.urls import url
from core import views
from django.views.decorators.cache import cache_page
from django.conf import settings
from .sitemaps import ConferenceIndexViewSitemap, ConferencePublicScheduleDownloadViewSitemap, ConferencePublicEventViewSitemap

app_name = 'core'

sitemaps = {
    'core_conference': ConferenceIndexViewSitemap(),
#    'core_conference_public_schedule_day': ConferencePublicScheduleDayViewSitemap(),
    'core_conference_public_schedule_download': ConferencePublicScheduleDownloadViewSitemap(),
    'core_conference_public_event': ConferencePublicEventViewSitemap(),
}


urlpatterns = [
    url(
        r'^next/.*$',
        cache_page(settings.FROSCH_CACHE_TIMEOUT)(views.RedirectToNextConference.as_view()),
        name='next_conference'),
    url(
        r'^(?P<conference_id>[^/]+)/$',
        views.ConferenceDetail.as_view(),
        name='conference'),
    url(
        r'^(?P<conference_id>[^/]+)/schedule/download$',
        views.PublicScheduleDownload.as_view(),
        name='conference_public_schedule_download'),
    url(
        r'^(?P<conference_id>[^/]+)/schedule/event/(?P<event_id>\d+)/popup?$',
        views.PublicEventPopup.as_view(),
        name='conference_public_event_popup'),
    url(
        r'^(?P<conference_id>[^/]+)/schedule/event/(?P<event_id>\d+)/?$',
        views.PublicEvent.as_view(),
        name='conference_public_event'),
]
