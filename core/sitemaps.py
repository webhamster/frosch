from django.contrib.sitemaps import Sitemap
from django.urls import reverse
from .models import Conference, ConferenceDay
from django.db.models import Q
from callforcontributions.models import Event, EventStatus
from .views import PeopleDetailPage


class StaticCoreViewSitemap(Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return ['core:people',]

    def location(self, item):
        return reverse(item)


class PeopleDetailViewSitemap(Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return PeopleDetailPage().get_queryset()


class ConferenceIndexViewSitemap(Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return Conference.objects.filter(is_active=True)


#class ConferencePublicScheduleDayViewSitemap(Sitemap):
#    priority = 0.5
#    changefreq = 'daily'
#
#    def items(self):
#        return ConferenceDay.objects.filter(conference__is_active=True, conference__eventsettings__last_draft_released__isnull=False)


class ConferencePublicScheduleDownloadViewSitemap(Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return Conference.objects.filter(is_active=True, eventsettings__last_draft_released__isnull=False)

    def location(self, conference):
        return reverse('core_conference:conference_public_schedule_download', args=[conference.url_snippet])

    
class ConferencePublicEventViewSitemap(Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return Event.objects.filter(
            Q(status=EventStatus.CONFIRMED) | Q(status=EventStatus.UNCONFIRMED),
            conference__is_active=True,
            version=Event.RELEASED
        )
