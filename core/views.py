from django.conf import settings
from django.contrib import messages
from django.contrib.auth import (login as auth_login, logout as do_logout,
                                 update_session_auth_hash)
from django.contrib.auth.models import Group
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.views import LoginView
from django.db.models import Q, Count, Min
from django.http import (Http404, HttpResponseRedirect,
                         JsonResponse)
from django.shortcuts import get_object_or_404, redirect, render, resolve_url
from django.template import RequestContext
from django.utils.http import is_safe_url, urlencode
from django.utils.translation import override, ugettext_lazy as _

from .forms import ContactForm
from .view_utils import ConferenceQuerysets
from active_login_required import active_login_required
from callforcontributions.models import Event, EventStatus
from core.view_utils import ConferenceScheduleView, ScheduleLayer
from core.forms import (UserChangeForm, UserCreationForm,
                        UserPasswordResetForm)
from core.models import (Conference, User, UserAvailability,
                         send_templated_mail_to_frosch_admins)
from django.urls import reverse
from django.views import View
from django.views.generic import DetailView, TemplateView, ListView, FormView
from .models import Room
from post_office import mail
from sponsormanager.view_utils import SponsorViewMixin
from core.models import Scheduleable
from django.views.decorators.http import condition



class ConferenceContextMixin():

    def get_conference(self):
        if not hasattr(self, 'conference'):
            self.conference = get_object_or_404(Conference, url_snippet=self.kwargs.get('conference_id'))
        return self.conference
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'conference': self.get_conference()
        })
        return context


def send_register_mail(request, user, redirect_to):
    url = (
        reverse(
            'accounts:unlock', args=[user.id, user.secret_token]
        ) + "?" + urlencode({
            'next': redirect_to
        })
    )
    user.send_templated_mail(request, 'core.register', {
        'unlock_url': request.build_absolute_uri(url),
    })

    
def get_public_schedule_view(conference, day):
    # Schedule view
    events = Event.objects.filter(
        conference=conference,
        version=Event.RELEASED,
        status__in=[EventStatus.CONFIRMED, EventStatus.UNCONFIRMED],
        day_id=day.id,
    ).order_by('day__start', 'start_datetime', 'room__sort_key', 'room__name')
    rooms = Room.objects.filter(
        event__in=events.filter(scheduled=Event.SS_DTR)
    ).distinct().order_by('sort_key', 'name')

    active_layer = ScheduleLayer(events)
    active_layer.template = 'core/mixins/schedule-public-event.html'

    view = ConferenceScheduleView(
        conference,
        conference.conferenceday_set.filter(id=day.id),
        rooms,
        active_layer,
        public_times=True,
    )
    events_notime = events.filter(scheduled=Event.SS_DR)
    return (day, view, events_notime)
    

def register(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('accounts:profile'))
    redirect_to = request.POST.get('next', request.GET.get('next', ''))
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            send_register_mail(request, new_user, redirect_to)
            return HttpResponseRedirect(reverse('accounts:thanks'))
    else:
        form = UserCreationForm()

    return render(request, "registration/register.html", {
        'form': form,
        'next': redirect_to
    })


def thanks(request):
    if request.user.is_authenticated() and request.user.is_active:
        return HttpResponseRedirect(reverse('accounts:profile'))
    return render(request, "registration/thanks.html")


def login(request):
    if request.user.is_authenticated() and not request.user.is_active:
        send_register_mail(request, request.user, reverse('accounts:profile'))
        return HttpResponseRedirect(reverse('accounts:thanks'))
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('accounts:profile'))
    
    class CustomAuthenticationForm(AuthenticationForm):
        def __init__(self, *args, **kwargs):
            super(CustomAuthenticationForm, self).__init__(*args, **kwargs)
            self.error_messages['invalid_login'] = _(
                "Your email address and password didn't match. "
                "Note that both fields may be case-sensitive.")
    return LoginView.as_view(request, authentication_form=CustomAuthenticationForm)


def logout(request):
    do_logout(request)
    return HttpResponseRedirect(resolve_url(settings.LOGIN_REDIRECT_URL))


def unlock(request, user_id, user_secret_token):
    if request.user.is_authenticated() and request.user.is_active:
        return HttpResponseRedirect(reverse('accounts:profile'))
    if len(user_secret_token) == 0:
        raise Http404(_("Secret token not provided."))
    user = get_object_or_404(User, id=user_id, secret_token=user_secret_token)
    redirect_to = request.GET.get('next', '')
    if not is_safe_url(url=redirect_to, host=request.get_host()):
        redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

    if user.is_active:
        return HttpResponseRedirect(redirect_to)
    else:
        user.is_active = True
        user.save()
        # We now log the user in (for this one time only!)
        user.backend = 'django.contrib.auth.backends.ModelBackend'
        auth_login(request, user)

        mail.send(
            template='core.notify-user-registration',
            recipients=[x[1] for x in settings.ADMINS],
            context=RequestContext(request, {
                'user': user,
                'FROSCH_NAME': settings.FROSCH_NAME,

            })
        )

        return render(request, "registration/unlocked.html", {
            'user': user,
            'next': redirect_to
        })

    
def reset_password(request, user_id=None, user_password_reset_token=None):
    if request.user.is_authenticated() and request.user.is_active:
        return HttpResponseRedirect(reverse('accounts:profile'))
    redirect_to = request.POST.get('next', request.GET.get('next', ''))
    if not is_safe_url(url=redirect_to, host=request.get_host()):
        redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

    if request.method == 'POST' and request.POST.get('identifier', '') != '':
        users = User.objects.filter(Q(username=request.POST['identifier']) | Q(email=request.POST['identifier']))
        for u in users.all():  # there might be multiple users (same mail address for different users)
            u.generate_password_reset_token()
            u.save()
            reset_url = request.build_absolute_uri(reverse(
                'accounts:new-password', args=[u.id, u.password_reset_token]) + 
                                                   "?" + urlencode({'next': redirect_to}))
            u.send_templated_mail(request, 'core.reset-password', {
                    'reset_url': reset_url,
            })
        return render(request, "registration/reset-password.html", {
            'identifier': request.POST['identifier'],
            'results': users.count(),
            'next': redirect_to
        })
    return render(request, "registration/reset-password.html", {
        'next': redirect_to
    })


def new_password(request, user_id, user_password_reset_token):
    if request.user.is_authenticated() and request.user.is_active:
        return HttpResponseRedirect(reverse('accounts:profile'))
    user = get_object_or_404(User, id=user_id, password_reset_token=user_password_reset_token)

    redirect_to = request.POST.get('next', request.GET.get('next', ''))
    if not is_safe_url(url=redirect_to, host=request.get_host()):
        redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

    if request.method == 'POST':
        form = UserPasswordResetForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, user)
            user.is_active = True
            user.save()
            # We now log the user in (for this one time only!)
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            auth_login(request, user)
            return HttpResponseRedirect(redirect_to)
    else:
        form = UserPasswordResetForm(instance=user)
    return render(request, "registration/new-password.html", {
        'form': form,
        'next': redirect_to,
        'recovery_user': user
    })


@active_login_required
def profile(request):
    def get_instance(conference_day):
        return UserAvailability.objects.get_or_create(
            user=request.user,
            day=conference_day,
            defaults={
                'start': conference_day.start,
                'end': conference_day.end
            }
        )[0]

    if request.method == 'POST':
        form = UserChangeForm(request.POST, request.FILES, instance=request.user)
        if form.is_valid():
            form.save()
            if request.POST.get('password1', ''):
                update_session_auth_hash(request, request.user)
            messages.success(request, _("Changes saved."))
            return HttpResponseRedirect(resolve_url('accounts:profile'))
    else:
        form = UserChangeForm(instance=request.user)
    return render(request, 'registration/profile.html', {
        'form': form,
    })


class ConferenceDetail(ConferenceContextMixin, SponsorViewMixin, TemplateView):
    template_name = 'core/conference_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        conference = self.get_conference()
        viewsets = []
        for d in conference.public_days.all():
            viewsets.append(get_public_schedule_view(conference, d))

        # Schedule download
        pentabarf_url = self.request.build_absolute_uri(
            reverse('core_conference:conference_public_schedule_download', kwargs={
                'conference_id': conference.url_snippet}))
            
        context.update({
            'scheduleviewsets': viewsets,
            'pentabarf_url': pentabarf_url,
        })
        return context
            

class Contact(FormView):
    template_name = 'core/contact_page.html'
    form_class = ContactForm

    def get_initial(self):
        initial = super(Contact, self).get_initial()

        if self.request.user is not None and self.request.user.is_authenticated():
            initial['name'] = self.request.user.username
            initial['email'] = self.request.user.email

        return initial
    
    def form_invalid(self, form):
        response = super(Contact, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response
    
    def form_valid(self, form):
        conference = None
        conference_id = form.cleaned_data.get('conference_id', False)
        if conference_id:
            conference = Conference.objects.get(
                url_snippet=conference_id)
        data = {
            'request': self.request,
            'template': 'core.contact_form',
            'context': {
                'form_data': form.cleaned_data,
                'conference': conference,
            },
        }
        if conference is not None:
            conference.send_templated_mail_to_admins(**data)
        else:
            send_templated_mail_to_frosch_admins(**data)

        if self.request.is_ajax():
            return JsonResponse({'status': 'ok'})
        else:
            return redirect('news:index')


class RedirectToMostRecentArchivedConference(View):
    def get(self, request):
        c = ConferenceQuerysets.archived_conferences().last()
        return redirect('core_conference:conference', c.url_snippet)


class RedirectToNextConference(View):
    def get(self, request):
        c = ConferenceQuerysets.active_conferences().first()
        if c is None:
            c = ConferenceQuerysets.archived_conferences().last()
        conf_url = resolve_url('core_conference:conference', c.url_snippet)
        return HttpResponseRedirect(conf_url)


class PublicScheduleDownload(ConferenceContextMixin, TemplateView):
    template_name = 'core/conference_public_schedule.xml'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        return self.render_to_response(context, content_type="text/xml; charset=utf-8")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        conference = self.get_conference()
        events = Event.objects.filter(
            conference=conference,
            version=Event.RELEASED,
            status__in=[EventStatus.CONFIRMED, EventStatus.UNCONFIRMED],
        ).order_by('day__start', 'start_datetime', 'room__sort_key', 'room__name')
        rooms = Room.objects.filter(
            event__in=events.filter(scheduled=Event.SS_DTR)
        ).distinct().order_by('sort_key', 'name')

        grid = []
        for d in conference.public_days.all():
            daygrid = []
            for room in rooms.all():
                ev = events.filter(room=room, day=d).all()
                daygrid.append((room, list(ev)))
                
            grid.append((d, daygrid))

        context.update({
            'eventstatus': EventStatus,
            'conferenceeventsettings': conference.eventsettings,
            'grid': grid,
        })
        return context

    def dispatch(self, request, *args, **kwargs):
        def last_modified(request, conference_id):
            conference = get_object_or_404(Conference, url_snippet=self.kwargs.get('conference_id'))
            return conference.eventsettings.last_draft_released
        @condition(last_modified_func=last_modified)
        def _dispatch(request, *args, **kwargs):
            return super(PublicScheduleDownload, self).dispatch(request, *args, **kwargs)
        return _dispatch(request, *args, **kwargs)


class PeopleDetailPage(DetailView):
    slug_url_kwarg = 'user_id'
    slug_field = 'id'
    context_object_name = 'detailuser'
    model = User
    queryset = User.objects.exclude(event__id=None).filter(event__version=Scheduleable.RELEASED).annotate(eventcount=Count('event__id'))
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.get_object()
        contributions = user.events.filter(version=Event.RELEASED).exclude(event_type__hide_in_lists=True)
        conferences = Conference.objects.filter(contacts__user__id=user.id).annotate(start=Min('conferenceday__date')).order_by('start')
        context.update({
            'contributions': contributions,
            'organizer': conferences,
        })
        return context
    

class PeoplePage(TemplateView):
    model = User
    template_name = 'core/user_list.html'

    def get_context_data(self, **kwargs):
        g = Group.objects.exclude(conference__is_active=False)
        users = PeopleDetailPage().get_queryset().order_by('username')
        users_organizers = users.filter(groups__in=g.values_list('id', flat=True)).all()
        users_others = users.exclude(id__in=users_organizers).all()
        context = {
            'users_organizers': users_organizers,
            'users_others': users_others,
        }
        return context


class ContributionsPage(ListView):
    model = Event
    template_name = 'core/contributions_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['search'] = self.request.GET.get('search', '')
        return context

    def get_queryset(self):
        objects = Event.objects.filter(version=Event.RELEASED).exclude(event_type__hide_in_lists=True)

        search = self.request.GET.get('search', '')
        if search:
            objects = objects.filter(
                Q(title__icontains=search) |
                Q(description__icontains=search) |
                Q(speaker__username__icontains=search) |
                Q(speaker__affiliation__icontains=search) |
                Q(speaker__about__icontains=search)
            ) 
            
        return objects.order_by('event_type__sort_key', 'title', 'start_datetime').distinct().all()


class PublicEvent(ConferenceContextMixin, DetailView):
    slug_url_kwarg = 'event_id'
    slug_field = 'draft_id'
    context_object_name = 'event'
    model = Event

    template_name = 'core/conference_event_detail.html'


class PublicEventPopup(ConferenceContextMixin, DetailView):
    slug_url_kwarg = 'event_id'
    slug_field = 'draft_id'
    context_object_name = 'event'
    model = Event

    template_name = 'core/event.html'
    

class SitemapLangIndex(TemplateView):
    # note that this template is located in the django.contrib.sitemaps library
    template_name = 'sitemap_index.xml'
    content_type='application/xml'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        sitemaps = []
        for lang, _ in settings.LANGUAGES:
            with override(lang):
                path = reverse('django.contrib.sitemaps.views.sitemap')
                sitemaps.append(f"{settings.FROSCH_BASE_URL}{path}")
        
        context['sitemaps'] = sitemaps
        return context
