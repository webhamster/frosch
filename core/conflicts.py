from django.db.models import Q, Model
from django.utils.translation import ugettext as __, ugettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist


class Conflict():
    pass
        
       
class UserAvailabilityConflict(Conflict):
    idstr = 'user_availability'
    
    @staticmethod
    def check_user_and_scheduleable(user, scheduleable):
        try:
            user.useravailability_set.get(
                day=scheduleable.day,
                not_available=True
            )
        except ObjectDoesNotExist:
            pass
        else:
            return UserAvailabilityConflictWholeDay(user)

        try:
            data = user.useravailability_set.get(
                Q(start_datetime__gt=scheduleable.start_datetime) |
                Q(end_datetime__lt=scheduleable.end_datetime),
                day=scheduleable.day,
                not_available=False,
            )
        except ObjectDoesNotExist:
            return None
        else:
            problems = (
                (data.start_datetime > scheduleable.start_datetime) if data.start_datetime is not None else False,
                (data.end_datetime < scheduleable.end_datetime) if data.end_datetime is not None else False,
            )
            return UserAvailabilityConflictTimeslot(user, data, *problems)

        
class UserAvailabilityConflictWholeDay(UserAvailabilityConflict):
    def __init__(self, user):
        self.user = user

    def __str__(self):
        return __("User {0} is not available on this day.").format(self.user.username)
        
        
class UserAvailabilityConflictTimeslot(UserAvailabilityConflict):
    def __init__(self, user, data, start_conflict, end_conflict):
        self.user = user
        self.data = data
        self.start_conflict = start_conflict
        self.end_conflict = end_conflict

    def __str__(self):
        problems = []
        if self.start_conflict:
            problems.append(_("not available before {0}").format(self.data.start_datetime))
        if self.end_conflict:
            problems.append(_("not available after {0}").format(self.data.end_datetime))
        
        return __("User {0} is ").format(self.user.username) + _(" and ").join(problems) + "."
        

class UserDutiesConflict(Conflict):
    idstr = 'user_duties'
    
    def __init__(self, users, scheduleable):
        self.users = users
        self.scheduleable = scheduleable

    def __str__(self):
        if len(self.users) == 1:
            s = __("The user {0} is needed at {1}.")
        else:
            s = __("The users {0} are needed at {1}.")
        return s.format(
            ", ".join(map(str, self.users)),
            self.scheduleable
        )
    

class RoomConflict(Conflict):
    idstr = 'room'
    
    def __init__(self, room, scheduleable):
        self.room = room
        self.scheduleable = scheduleable

    def __str__(self):
        return __("Room {0} is occupied by {1}.").format(
            self.room,
            self.scheduleable
        )
    

class ConflictCollection(list):
    def __init__(self, scheduleable_id, *args):
        super().__init__(*args)
        self.scheduleable_id = scheduleable_id

    def get_conflicting(self, model):
        out = []
        for c in self:
            if hasattr(c, 'scheduleable'):
                if isinstance(c.scheduleable, model):
                    out.append(c.scheduleable.id)
        return set(out)

    def as_dict(self):
        """Return a dict mapping conflict idstrings to lists of stringified conflicts."""
        return {
            self.scheduleable_id: {
                CLS.idstr: list(
                    str(conflict) for conflict in filter(lambda el: isinstance(el, CLS), self)
                ) for CLS in Conflict.__subclasses__()
            }
        }
