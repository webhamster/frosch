from datetime import date, time, timedelta
from django_webtest import WebTest
from .management.commands.load_email_templates import Command as LoadEmailTemplatesCommand
from django.test import override_settings

from .models import Conference, ConferenceDay, Room, User, UserAvailability


@override_settings(POST_OFFICE = {
    'BACKENDS': {
        'default': 'django.core.mail.backends.locmem.EmailBackend'
    }
})
class CoreTestSetup(WebTest):
    @classmethod
    def setUpTestData(cls):
        cls.conference = Conference.objects.create(
            long_name="long name test",
            short_name="short name test",
            url_snippet="test1234",
#            notification_email_addresses="kontakt@cmd-ev.de",

        )
        cls.cds = [ConferenceDay.objects.create(
            conference=cls.conference,
            date=date.today() - timedelta(days=offset),
            start=time(9, 0, 0),
            end=time(18, 0, 0),
            public_start=(time(10, 30, 0) if offset == 0 else None),
            public_end=(time(17, 0, 0) if offset == 0 else None),
        ) for offset in (-1, 0, 1)]
        cls.users = (
            cls.alice,
            cls.bob,
            cls.charlie,
            cls.daniel) = [
                User.objects.create_user(
                    username="Alice",
                    email="alice@example.com",
                ), User.objects.create_user(
                    username="Bob",
                    email="bob@example.com",
                ), User.objects.create_user(
                    username="Charlie",
                    email="charlie@example.com",
                ), User.objects.create_user(
                    username="Daniel",
                    email="daniel@example.com",
                )]
        cls.admin_user = User.objects.create_superuser('admin', 'admin@example.com', 'admin')
        cls.rooms = [Room.objects.create(
            name=n,
            sort_key=-1
        ) for n in ('Main room', 'Workshop room', 'Something else')]

        cls.availabilities = [
            UserAvailability.objects.create(
                user=cls.alice,
                day=cls.cds[1],
                start=time(10, 30, 0),
            )
        ]

        LoadEmailTemplatesCommand().handle()
