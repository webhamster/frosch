from django.apps import AppConfig


class SponsormanagerConfig(AppConfig):
    name = 'sponsormanager'
