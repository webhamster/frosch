import math
from django.conf import settings


class SponsorViewMixin:
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        conference = self.get_conference()

        sponsors = conference.sponsor_set.order_by('-level__sort_key', 'level__name', 'name').all()
        max_virtual_width = float('-inf')

        # we first calculcate a virtual size for each logo
        for s in sponsors:
            area = s.level.size
            width_before = s.image.width
            height_before = s.image.height
            ratio = float(height_before) / width_before
            new_width = math.sqrt(float(area) / ratio)
            new_height = ratio * new_width
            s.virtual_width = new_width
            s.virtual_height = new_height
            max_virtual_width = max(new_width, max_virtual_width)

        # we now take the maximum virtual width to calculate a ratio to get down to the actual maximum width
        available_width = settings.FROSCH_SPONSOR_GRID_GRANULARITY / 2
        ratio = available_width / max_virtual_width
        output = []
        for s in sponsors:
            width = round(s.virtual_width * ratio)
            output.append({
                'span': round(width * 2) + 1,
                'start': round(settings.FROSCH_SPONSOR_GRID_GRANULARITY / 2 - width) + 1,
                'sponsor': s,
            })
            # print (f"{s.name}: {s.image.width}x{s.image.height}, virtual_width={s.virtual_width}, res_width={width}, span={output[-1]['span']}, start={output[-1]['start']}, grid_gran={settings.FROSCH_SPONSOR_GRID_GRANULARITY}")

        context.update({
            'sponsors': output
        })

        return context
