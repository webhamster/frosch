from django.contrib import admin
from .models import Sponsor, SponsorLevel
from modeltranslation.admin import TranslationAdmin


@admin.register(Sponsor)
class SponsorAdmin(TranslationAdmin):
    list_display = ('name', 'conference', 'link', )

    
@admin.register(SponsorLevel)
class SponsorLevelAdmin(TranslationAdmin):
    list_display = ('name', 'size', 'sort_key', )
    ordering = ('-sort_key', )
