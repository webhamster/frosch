from django.db import models
from core.models import Conference
from filer.fields.image import FilerImageField


class SponsorLevel(models.Model):
    name = models.CharField(max_length=200)
    size = models.IntegerField(help_text="Maximum logo size in pixels²")
    sort_key = models.FloatField(default=1.0)

    def __str__(self):
        return f"{self.name} ({self.size})"


class Sponsor(models.Model):
    name = models.CharField(max_length=200)
    conference = models.ForeignKey(Conference, on_delete=models.CASCADE)
    level = models.ForeignKey(SponsorLevel, on_delete=models.CASCADE)
    link = models.URLField()
    image = FilerImageField(blank=True, null=True, related_name='sponsor_image', on_delete=models.CASCADE)
    save_as = True
    save_on_top = True
    
