from modeltranslation.translator import register, TranslationOptions
from .models import Sponsor, SponsorLevel


@register(Sponsor)
class SponsorTranslationOptions(TranslationOptions):
    fields = ('name', )

    
@register(SponsorLevel)
class SponsorLevelTranslationOptions(TranslationOptions):
    fields = ('name', )
    
    
