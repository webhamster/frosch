from modeltranslation.translator import register, TranslationOptions

from staffregistration.models import ConferenceStaffSettings, Skill


@register(ConferenceStaffSettings)
class ConferenceStaffSettingsTranslationOptions(TranslationOptions):
    fields = ('description', )

    
@register(Skill)
class SkillTranslationOptions(TranslationOptions):
    fields = ('name', )

    
