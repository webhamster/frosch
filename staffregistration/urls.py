from django.conf.urls import url
from staffregistration import views
from .sitemaps import StaffregistrationIndexViewSitemap

app_name = 'staffregistration'

sitemaps = {
    'staff_index': StaffregistrationIndexViewSitemap(),
}

urlpatterns = [
    url(r'^$', views.Index.as_view(), name='index'),
    url(r'^agenda/(?P<version>(released|draft|None))?$', views.Agenda.as_view(), name='agenda'),
    url(r'^board/(?P<job_type_id>\d+)$', views.Board.as_view(), name='board'),
    url(r'^board$', views.Board.as_view(), name='board'),
    url(r'^availability-skills$', views.AvailabilitySkills.as_view(), name='availability-skills'),
    url(r'^ics$', views.ICSDownload.as_view(), name='ics'),
]
