class JobGeneratorAdminExtension:
    def actions(self):
        return (self.generate_jobs, )
    
    @staticmethod
    def generate_jobs(admin, request, queryset):
        for conference in queryset.all():
            conference.conferencestaffsettings.auto_generate()
