from datetime import timedelta

from core.models import User
from django.utils import timezone
from .models import (ConferenceStaff, ConferenceStaffSettings, Job, JobType,
                     Skill)
from callforcontributions.tests import CallforcontributionsTestSetup


class StaffregistrationTest(CallforcontributionsTestSetup):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.skills = (
            Skill.objects.create(
                name='Skill 1'),
            Skill.objects.create(
                name='Skill 2'),
            Skill.objects.create(
                name='Skill 3')
        )
        cls.skills[0].users = User.objects.filter(username='Alice')
        cls.skills[0].save()
        cls.skills[2].users = User.objects.filter(username__contains='e')
        cls.skills[2].save()
        assert cls.skills[2].users.count() == 3
        
        cls.conferencestaff = (
            ConferenceStaff.objects.create(
                conference=cls.conference,
                user=User.objects.get(username='Alice')),
            ConferenceStaff.objects.create(
                conference=cls.conference,
                user=User.objects.get(username='Bob'))
        )
            
    def test_create_job_simple(self):
        c = ConferenceStaffSettings.objects.create(
            conference=self.conference,
            description="bla"
        )
        self.assertEquals(ConferenceStaffSettings.objects.count(), 1)
        jt1 = JobType.objects.create(
            conference=self.conference,
            name="simple job, no auto generation",
            short_description="x",
        )
        self.assertEquals(JobType.objects.count(), 1)
        c.auto_generate()  # should not do anything
        assert(Job.objects.count() == 0)
        Job.objects.create(
            conference=self.conference,
            job_type=jt1,
            start_datetime=timezone.now(),
            length=(60 * 5),
        )
        c.auto_generate()  # should not do anything
        assert(Job.objects.count() == 1)

    def test_autocreate_one_job(self):
        c = ConferenceStaffSettings.objects.create(
            conference=self.conference,
            description="bla"
        )
        self.assertEquals(ConferenceStaffSettings.objects.count(), 1)
        jt2 = JobType.objects.create(
            conference=self.conference,
            name="one job of this type should be created per event",
            short_description="",
        )
        jt2.auto_generate_for_event_type = [self.event_type_talk]
        self.assertEquals(JobType.objects.count(), 1)
        c.auto_generate()
        self.assertEqual(Job.objects.count(), 1)
        j = Job.objects.get()
        self.assertEqual(j.conference, self.conference)
        self.assertEqual(j.job_type, jt2)
        self.assertEqual(j.room, self.event_long_talk_scheduled.room)
        self.assertEqual(j.start_datetime, self.event_long_talk_scheduled.start_datetime)
        self.assertEqual(j.day, self.event_long_talk_scheduled.day)
        self.assertEqual(j.length, self.event_long_talk_scheduled.length.length)
        self.assertEqual(j.event, self.event_long_talk_scheduled)
        self.assertEqual(j.auto_generated, True)
            
    def test_autocreate_three_jobs(self):
        c = ConferenceStaffSettings.objects.create(
            conference=self.conference,
            description="bla"
        )
        self.assertEquals(ConferenceStaffSettings.objects.count(), 1)
        jt2 = JobType.objects.create(
            conference=self.conference,
            name="one job of this type should be created per event",
            short_description="",
            split_length=119,  # split after 119 minutes, so the 120 minutes event should be split!
            default_length=40,  # split into three parts of 40 minutes each
        )
        jt2.auto_generate_for_event_type = [self.event_type_talk]
        self.assertEquals(JobType.objects.count(), 1)
        c.auto_generate()
        self.assertEqual(Job.objects.count(), 3)
        jobs = Job.objects.order_by('start_datetime').all()
        for num, j in zip(range(len(jobs)), jobs):
            self.assertEqual(j.conference, self.conference)
            self.assertEqual(j.job_type, jt2)
            self.assertEqual(j.room, self.event_long_talk_scheduled.room)
            if num == 0:
                self.assertEqual(
                    j.start_datetime,
                    self.event_long_talk_scheduled.start_datetime)
            else:
                self.assertEqual(
                    j.start_datetime,
                    jobs[num - 1].start_datetime + timedelta(minutes=jobs[num - 1].length))
            self.assertEqual(j.day, self.event_long_talk_scheduled.day)
            self.assertEqual(j.length, 40)
            self.assertEqual(j.event, self.event_long_talk_scheduled)
            self.assertEqual(j.auto_generated, True)

    def test_delete_job(self):
        pass
        
    def test_user_conflict(self):
        c = ConferenceStaffSettings.objects.create(
            conference=self.conference,
            description="bla"
        )
        
        jt2 = JobType.objects.create(
            conference=self.conference,
            name="one job of this type should be created per event",
            short_description="",
            split_length=119,  # split after 119 minutes, so the 120 minutes event should be split!
            default_length=40,  # split into three parts of 40 minutes each
        )
        jt2.auto_generate_for_event_type = [self.event_type_talk]
        self.assertEquals(JobType.objects.count(), 1)
        c.auto_generate()
        self.assertEqual(Job.objects.count(), 3)
        # There should now be three jobs parallel to self.event_long_talk_scheduled
        # All users are already a speaker of this event, so we remove one.
        self.event_long_talk_scheduled.speaker.remove(self.alice)
        job = jt2.job_set.all()[0]
        job.staff_assigned = [self.alice]
        job.save()
        conflicts = job.get_conflicts()
        self.assertEqual(conflicts.user_availability, {self.alice})
        self.assertEqual(conflicts.user_duties, set())
        self.assertEqual(conflicts.room, set())
        self.event_long_talk_scheduled.speaker.add(self.alice)
        conflicts = job.get_conflicts()
        self.assertEqual(conflicts.user_duties, {self.event_long_talk_scheduled, })

    def test_can_add(self):
        c = ConferenceStaffSettings.objects.create(
            conference=self.conference,
            description="bla"
        )
        
        jt2 = JobType.objects.create(
            conference=self.conference,
            name="one job of this type should be created per event",
            short_description="",
            split_length=119,  # split after 119 minutes, so the 120 minutes event should be split!
            default_length=40,  # split into three parts of 40 minutes each
        )
        jt2.auto_generate_for_event_type = [self.event_type_talk]
        self.assertEquals(JobType.objects.count(), 1)
        c.auto_generate()
        self.assertEqual(Job.objects.count(), 3)
        # There should now be three jobs parallel to self.event_long_talk_scheduled
        
        job = jt2.job_set.all()[0]
        job.add_user(self.alice)
        
