from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def is_staff_subnav_enabled(context):
    request = context['request']
    user = request.user
    conference = context['conference']
    if not user.is_authenticated():
        return False
    if user.conferencestaff_set.filter(conference__id=conference.id).exists():
        return True
    return False
