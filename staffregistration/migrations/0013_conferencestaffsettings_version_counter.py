# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-17 20:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('staffregistration', '0012_conferencestaffsettings_last_draft_released'),
    ]

    operations = [
        migrations.AddField(
            model_name='conferencestaffsettings',
            name='version_counter',
            field=models.IntegerField(default=0),
        ),
    ]
