# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-06 21:46
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0015_auto_20171206_2246'),
        ('staffregistration', '0006_auto_20171127_2159'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConferenceStaff',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('notes', models.TextField()),
                ('conference', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Conference')),
                ('job_types', models.ManyToManyField(blank=True, help_text='This user wants to do these types of jobs.', to='staffregistration.JobType')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RenameModel(
            old_name='ConferenceStaffSettings',
            new_name='ConferenceStaffregistrationSettings',
        ),
        migrations.RemoveField(
            model_name='staffskills',
            name='job_types',
        ),
        migrations.RemoveField(
            model_name='staffskills',
            name='user',
        ),
    ]
