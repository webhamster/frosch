from django.contrib.sitemaps import Sitemap
from django.urls import reverse
from .models import ConferenceStaffSettings
from core.view_utils import ConferenceQuerysets


class StaffregistrationIndexViewSitemap(Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        active_conferences = ConferenceQuerysets.active_conferences()
        return ConferenceStaffSettings.objects.filter(conference__is_active=True, is_public=True, conference_id__in=active_conferences)

