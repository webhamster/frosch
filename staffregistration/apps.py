from django.apps import AppConfig

class StaffregistrationAppConfig(AppConfig):
    name = 'staffregistration'
    verbose_name = 'Registration for Staff'
