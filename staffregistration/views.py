from .models import Skill, Job, ConferenceStaff, JobType, StaffException
from .forms import UserSkillForm, ConferenceStaffForm
from django.contrib.auth.mixins import LoginRequiredMixin
from core.models import Conference
from core.utils import make_ical
from django.shortcuts import get_object_or_404, redirect

from django.views.generic import View
from django.views.generic.base import TemplateResponseMixin
from django.utils.decorators import method_decorator
from active_login_required import active_login_required
from django.utils.translation import ugettext_lazy as _

from core.view_utils import UserAvailabilityViewMixin, ConferenceScheduleView, ScheduleLayer
from django.contrib import messages

from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist

from callforcontributions.models import Event


from django.http import HttpResponse


class PostAutoDispatchMixin():
    @method_decorator(active_login_required)
    def post(self, request, conference_id, **kwargs):
        if 'submit' not in request.POST:
            s = self.submit_default  # forms submitted via JS are sent without submit value
        else:
            s = request.POST['submit']
        for p in self.__class__.__bases__:
            if hasattr(p, 'prefix') and s == p.prefix:
                return p.post(self, request, conference_id, **kwargs)
        assert(False)


class SkillViewMixin():
    prefix = 'skillset'
    
    def get_skill_form(self, request, conference) -> UserSkillForm:
        skills_required = Skill.objects.filter(
            jobtype__conference=conference).distinct().order_by('-can_self_assign', 'name').all()
        return UserSkillForm(
            request.user,
            skills_required,
            request.POST if request.method == 'POST' else None,
            prefix=SkillViewMixin.prefix)

    @method_decorator(active_login_required)
    def post(self, request, conference_id):
        conference = get_object_or_404(Conference, url_snippet=conference_id)
        form = self.get_skill_form(request, conference)
        assert(form.is_valid())
        form.save()
        messages.success(request, _("Thanks for updating your skills."))
        return redirect(self.return_to, conference_id=conference_id)

    
class ConferenceStaffMixin():
    prefix = 'conferencestaff'
    
    def get_conferencestaff_form(self, request, conference):
        return ConferenceStaffForm(
            request.user,
            conference,
            request.POST if request.method == 'POST' else None,
            prefix=ConferenceStaffMixin.prefix)

    @method_decorator(active_login_required)
    def post(self, request, conference_id):
        conference = get_object_or_404(Conference, url_snippet=conference_id)
        form = self.get_conferencestaff_form(request, conference)
        assert(form.is_valid())
        form.save()
        if form.cleaned_data['is_available']:
            messages.success(request, _("Thanks for offering your help for {}!").format(conference.short_name))
        else:
            messages.success(request, _("Thanks for letting us know!"))
        return redirect(self.return_to, conference_id=conference_id)

    
class JobAssignMixin():
    prefix = 'jobassign'
    
    @method_decorator(active_login_required)
    def post(self, request, conference_id, *args, **kwargs):
        add = f"{JobAssignMixin.prefix}-action-add"
        remove = f"{JobAssignMixin.prefix}-action-remove"
        
        if remove in request.POST:
            job = get_object_or_404(
                Job,
                id=request.POST[remove],
                version=Job.DRAFT)
            job.staff_assigned.remove(request.user)
        if add in request.POST:
            job = get_object_or_404(
                Job,
                id=request.POST[add],
                version=Job.DRAFT)
            try:
                job.add_user(request.user)
            except StaffException as e:
                messages.error(request, str(e))
            else:
                messages.success(request, _("Successfully assigned to job."))
        return redirect(self.return_to, conference_id=conference_id, **kwargs)


class JobExclusionMixin():
    prefix = 'excludejob'

    @method_decorator(active_login_required)
    def post(self, request, conference_id, *args, **kwargs):
        job_type = get_object_or_404(JobType, id=request.POST[JobExclusionMixin.prefix + '-jobtype'])
        if request.POST[JobExclusionMixin.prefix + '-action'] == 'exclude':
            job_type.excluded_users.add(request.user)
            messages.warning(request, _("Job type will not be auto-assigned to you: {}.").format(job_type))
        if request.POST[JobExclusionMixin.prefix + '-action'] == 'allow':
            if JobType.objects.filter(excluded_users=request.user).exists():
                job_type.excluded_users.remove(request.user)
                messages.success(request, _("Job type enabled: {}.").format(job_type))
        job_type.save()
        return redirect(self.return_to, conference_id=conference_id, **kwargs)
                

class Index(
        TemplateResponseMixin,
        ConferenceStaffMixin,
        JobAssignMixin,
        PostAutoDispatchMixin,
        View):
    template_name = 'staffregistration/index.html'
    return_to = 'staff:index'
    submit_default = 'conferencestaff'

    def get(self, request, conference_id):
        conference = get_object_or_404(Conference, url_snippet=conference_id)
        conference_staff_settings = conference.conferencestaffsettings

        step = 0
        if not request.user.is_authenticated():
            conferencestaff_form = None
        else:
            conferencestaff_form = self.get_conferencestaff_form(request, conference)
            if not conferencestaff_form.entry_exists:
                step = 1
            elif not request.user.has_full_availability(conference):
                step = 2
            elif not Job.objects.filter(
                    conference=conference,
                    version=Job.DRAFT,
                    staff_assigned=request.user).exists():
                step = 3
            elif not Job.objects.filter(
                    conference=conference,
                    version=Job.RELEASED).exists():
                step = 4
            else:
                step = 5
            
        context = {
            'conference': conference,
            'conference_staff_settings': conference_staff_settings,
            'conferencestaff_form': conferencestaff_form,
            'step': step,
        }

        return self.render_to_response(context)

            
class Agenda(LoginRequiredMixin, TemplateResponseMixin, View):
    template_name = 'staffregistration/schedule-agenda.html'

    def get(self, request, conference_id, version='released'):
        conference = get_object_or_404(Conference, url_snippet=conference_id)

        if version not in ['released', 'draft']:
            version = 'released'

        if not ConferenceStaff.objects.filter(
                conference=conference,
                user=request.user,
        ).exists():
            return redirect('staff:index', conference_id=conference_id)
        
        conferencestaffsettings = conference.conferencestaffsettings

        if conferencestaffsettings.last_draft_released is None:
            version = 'draft'

        qv = Job.RELEASED if version == 'released' else Job.DRAFT
        
        jobs = Job.objects.filter(
            Q(staff_assigned=request.user),
            conference=conference,
            version=qv).exclude(Q(start_datetime=None) | Q(room=None)).all()

        active_layer = ScheduleLayer(jobs)
        active_layer.template = 'staffregistration/mixins/schedule-job-agenda.html'
        
        view = ConferenceScheduleView(
            conference,
            conference.conferenceday_set.order_by('date'),
            conference.rooms,
            active_layer,
            editable=False
        )

        context = {
            'jobs': jobs,
            'conference': conference,
            'conferencestaffsettings': conferencestaffsettings,
            'scheduleset': view,
            'version': version,
        }
         
        return self.render_to_response(context)


class ICSDownload(LoginRequiredMixin, View):
    def get(self, request, conference_id):
        conference = get_object_or_404(Conference, url_snippet=conference_id)
        staff = get_object_or_404(ConferenceStaff,
                conference=conference,
                user=request.user,
        )
        conferencestaffsettings = conference.conferencestaffsettings
        jobs = Job.objects.filter(
            Q(staff_assigned=request.user),
            conference=conference,
            version=Job.RELEASED).exclude(Q(start_datetime=None) | Q(room=None)).all()

        cal = make_ical(jobs)
        res = HttpResponse(cal, content_type="text/calendar")
        res['Content-Disposition'] = f'inline; filename="{conference.url_snippet}-jobs-version-{conferencestaffsettings.get_version()}.ics"'
        return res

    
class Board(
        LoginRequiredMixin,
        TemplateResponseMixin,
        PostAutoDispatchMixin,
        JobExclusionMixin,
        JobAssignMixin,
        View):
    template_name = 'staffregistration/schedule-board.html'
    return_to = 'staff:board'

    def get(self, request, conference_id, job_type_id=None):
        conference = get_object_or_404(Conference, url_snippet=conference_id)

        if not ConferenceStaff.objects.filter(
                conference=conference,
                user=request.user,
        ).exists():
            return redirect('staff:index', conference_id=conference_id)
        
        conferencestaffsettings = conference.conferencestaffsettings

        unskills = Skill.objects.filter(~Q(users=request.user)).all()
            
        job_types = JobType.objects.filter(
            ~Q(excluded_users=request.user),
            ~Q(requires_skills__in=unskills),
            conference=conference).order_by('name').all()
        job_types_unskilled = JobType.objects.filter(
            conference=conference,
            requires_skills__in=unskills).order_by('name').all()
        job_types_excluded = JobType.objects.filter(
            conference=conference,
            excluded_users=request.user
        ).exclude(id__in=job_types_unskilled).order_by('name').all()
        for job in job_types_unskilled:
            job.lacking_skills = filter(lambda x: x in unskills, job.requires_skills.all())

        try:
            current_job_type = JobType.objects.get(
                ~Q(requires_skills__in=unskills),
                id=job_type_id,
            )
        except ObjectDoesNotExist:
            jobs = jobs_background = view = current_job_type = None
        else:
            jobs = Job.objects.filter(
                job_type__id=job_type_id,
                conference=conference,
                version=Job.DRAFT).exclude(
                    Q(start_datetime=None) |
                    Q(room=None)).order_by(
                        'start_datetime').prefetch_related(
                            'staff_assigned').select_related('job_type').all()

            def canadd(job):
                job.active_layer = True
                try:
                    job.can_add_user(request.user)
                except StaffException as e:
                    job.can_add = False
                    job.can_add_whynot = str(e)
                else:
                    job.can_add = job.can_add_whynot = True

            active_layer = ScheduleLayer(jobs, canadd)
            active_layer.template = 'staffregistration/mixins/schedule-job-board.html'
            
            jobs_background = Job.objects.filter(
                staff_assigned=request.user,  # | Q(staff_waitlist=request.user),
                conference=conference,
                version=Job.DRAFT
            ).exclude(
                Q(start_datetime=None) | Q(room=None) | Q(id__in=jobs)
            ).select_related('job_type').all()

            passive_layer_jobs = ScheduleLayer(jobs_background)
            passive_layer_jobs.template = 'staffregistration/mixins/schedule-job-board.html'

            events_background = Event.objects.filter(
                speaker=request.user,
                conference=conference,
                version=Event.DRAFT
            ).exclude(
                Q(start_datetime=None) | Q(room=None)
            ).select_related('event_type').all()
            passive_layer_events = ScheduleLayer(events_background)
            passive_layer_events.template = 'staffregistration/mixins/schedule-event-board.html'

            passive_layers = [passive_layer_events, passive_layer_jobs, ]
            
            view = ConferenceScheduleView(
                conference,
                conference.conferenceday_set.order_by('date'),
                conference.rooms,
                active_layer,
                passive_layers,
                editable=False,
            )
                    
        context = {
            'current_job_type': current_job_type,
            'conference': conference,
            'conferencestaffsettings': conferencestaffsettings,
            'scheduleset': view,
            'job_types': job_types,
            'job_types_excluded': job_types_excluded,
            'job_types_unskilled': job_types_unskilled,
        }
                
        return self.render_to_response(context)


class AvailabilitySkills(
        LoginRequiredMixin,
        TemplateResponseMixin,
        PostAutoDispatchMixin,
        UserAvailabilityViewMixin,
        SkillViewMixin,
        View):
    template_name = 'staffregistration/availability-skills.html'
    return_to = 'staff:availability-skills'

    def get(self, request, conference_id):
        conference = get_object_or_404(Conference, url_snippet=conference_id)

        if not ConferenceStaff.objects.filter(
                conference=conference,
                user=request.user,
        ).exists():
            return redirect('staff:index', conference_id=conference_id)
        
        availability_forms, has_full_availability = self.get_availability_forms(request, conference)
        skill_form = self.get_skill_form(request, conference)
        context = {
            'conference': conference,
            'useravailability_forms': availability_forms,
            'skill_form': skill_form,
        }

        return self.render_to_response(context)
