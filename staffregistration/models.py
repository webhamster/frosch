from datetime import timedelta
from django.db import models, transaction
from django.db.models import Count, Q
from django.utils.functional import lazy
from django.utils.safestring import mark_safe
from django.utils.html import format_html
from django.utils.translation import ugettext as _
from math import ceil

from callforcontributions.models import Event
from core.models import ConferenceDay, Scheduleable, VersionableContainer, HasConflicts, User
from django.conf import settings
from core.conflicts import UserAvailabilityConflict
from django.urls import reverse


mark_safe_lazy = lazy(mark_safe, str)


class Skill(models.Model):
    """Users can self-declare skills after signing up. Example skills:
 - Certificate for first aid
 - Language skills"""
    name = models.CharField(max_length=200)
    can_self_assign = models.BooleanField(
        default=True,
        help_text="Check if users should be able to self-declare this skill for themselves.")
    users = models.ManyToManyField('core.User', blank=True)

    def __str__(self):
        return self.name

    
class ConferenceStaff(models.Model):
    """If an object (conference, user) exists, the user is helping at the conference."""

    class Meta:
        verbose_name = "Staff"
        verbose_name_plural = "Staff"
    
    conference = models.ForeignKey('core.Conference', on_delete=models.CASCADE)
    user = models.ForeignKey('core.User', on_delete=models.CASCADE)
    notes = models.TextField(blank=True)

    
class JobType(models.Model):
    name = models.CharField(max_length=40)
    long_description = models.TextField()
    staff_needed = models.IntegerField(default=1)
    staff_max = models.IntegerField(
        default=1,
        blank=True,
        null=True,
        help_text="Maximum number of staff members assigned to this job. Set to empty to remove limit.")
    conference = models.ForeignKey('core.Conference', on_delete=models.CASCADE)
    # requires_jobs = models.ManyToManyField('JobType', blank=True)
    requires_skills = models.ManyToManyField(Skill, blank=True)
    excluded_users = models.ManyToManyField('core.User', blank=True)
    can_self_assign = models.BooleanField(
        default=True,
        help_text="Check if users should be able to assign themselves to this job.")
    auto_generate_for_event_type = models.ManyToManyField(
        'callforcontributions.EventType',
        help_text="For selected event types, auto-create a job of this type parallel "
        "to the event if the 'auto-generate jobs' button is clicked.",
        blank=True)
    auto_generate_for_room = models.ManyToManyField(
        'core.Room',
        help_text="For selected rooms, auto-create a job of this type. The job's length "
        "will be determined from the first and last event in that room.",
        blank=True)
    auto_generate_per_visitors = models.IntegerField(
        null=True,
        blank=True,
        help_text="Generate one job per this number of planned visitors.")
    split_length = models.IntegerField(
        help_text="Automatically generated jobs will be split after this length. "
        "Should be less than 2x default length.",
        default=0)
    default_length = models.IntegerField(help_text="Default length.", default=60)

    def __str__(self):
        return f"{self.name} ({self.conference.short_name})"

    def auto_generate(self):
        for event_type in self.auto_generate_for_event_type.all():
            self._auto_generate_for_event_type(event_type)
        for room in self.auto_generate_for_room.all():
            self._auto_generate_for_room(room)
        self._auto_generate_per_visitors()

    def _auto_generate_for_event_type(self, event_type):
        events = self.conference.event_set.filter(
            event_type=event_type,
            version=Event.DRAFT,
            scheduled=Event.SS_DTR,
        )
        for e in events:
            if not e.has_datetime:
                continue
            existing = Job.objects.filter(
                job_type=self,
                conference=self.conference,
                event=e,
                auto_generated=True
            ).order_by('start_datetime').all()
            
            self._generate_or_change_jobs(
                e.start_datetime,
                e.end_datetime,
                existing,
                {
                    'event': e,
                    'room': e.room
                }
            )
                    
    def _auto_generate_for_room(self, room):
        # do this for all conference days
        days = ConferenceDay.objects.filter(conference=self.conference).all()
        for d in days:
            events = list(Event.objects.filter(
                conference=self.conference,
                room=room,
                day=d,
                version=Event.DRAFT,
                scheduled=Event.SS_DTR,
            ).all())
            if len(events) == 0:
                continue
            start_datetime = events[0].start_datetime
            end_datetime = events[-1].end_datetime
            
            existing = Job.objects.filter(
                conference=self.conference,
                room=room,
                day=d,
                job_type=self,
                auto_generated=True,
            ).order_by('start_datetime').all()
            self._generate_or_change_jobs(
                start_datetime,
                end_datetime,
                existing,
                {
                    'room': room
                }
            )
            
    def _auto_generate_per_visitors(self):
        pass

    def _generate_or_change_jobs(self, start_datetime, end_datetime, existing_jobs, properties):
        length = (end_datetime - start_datetime).seconds // 60
        
        if length <= self.default_length or self.split_length == 0:
            expected_jobs = [{'start_datetime': start_datetime, 'length': length}]
        else:
            number_jobs = length // self.default_length
            grid_size = settings.FROSCH_SCHEDULE_STEP
            # Calculate new job length as length of whole
            # event divided by number of required jobs, then
            # round this up to the schedule grid size (e.g., 5
            # minutes)
            job_length_in_minutes = ceil((length // number_jobs) // grid_size) * grid_size
            expected_jobs = []
            # calculate new time and day as follows:
            # first, use combined conference day and event time to have a proper date/time value
            # then, add the job_length_in_minutes as timedelta
            # finally, try to find a conference day matching the (potentially changed) day of the new start datetime
            start = start_datetime
            for i in range(number_jobs):
                expected_jobs.append({
                    'start_datetime': start,
                    'length': job_length_in_minutes  # todo: put date here as well, then when adding time below, move event to proper day if needed.
                })
                start += timedelta(minutes=job_length_in_minutes)

        for num, expected in enumerate(expected_jobs):
            if len(existing_jobs) > num:
                j = existing_jobs[num]
            else:
                j = Job(
                    conference=self.conference,
                    job_type=self,
                    locked=False,
                    auto_generated=True,
                )
            if j.locked:
                continue
            j.set_start_datetime(expected['start_datetime'])
            j.length = expected['length']
            for key, value in properties.items():
                setattr(j, key, value)
            j.save()

        for j in existing_jobs[len(expected_jobs):]:
            j.delete()

    def count_open_jobs(self):
        q = self.job_set.filter(
            version=Job.DRAFT
        ).exclude(
            start_datetime=None,
            room=None)
        if self.staff_max is not None:
            q = q.annotate(
                cnt=Count('staff_assigned')
            ).filter(
                cnt__lt=self.staff_max)
        return q.distinct().count()


class StaffException(Exception):
    pass


class ScheduleReleasedException(StaffException):
    def __str__(self):
        return _("This is a job in the released schedule that cannot be changed.")

    
class NoSelfAssignException(StaffException):
    def __str__(self):
        return _("You cannot assign yourself to jobs of this type.")

    
class StaffLimitException(StaffException):
    def __str__(self):
        return _("The staff limit for this job is already reached.")

    
class SkillLackException(StaffException):
    def __init__(self, lacking):
        super().__init__()
        self.lacking = lacking

    def __str__(self):
        return _("You are lacking the following skills: ") + (', '.join(self.lacking))


class StaffDutiesException(StaffException):
    def __init__(self, conflict):
        self.conflict = conflict
    
    def __str__(self):
        return _(f"You are already have other duties during this time: {self.conflict}")

    
"""
class NeedsPrerequisiteJobException(StaffException):
    def __init__(self, prerequisite):
        self.prerequisite = prerequisite

    def __str__(self):
        return __("You need to sign up for a job of this type before: {}".format(self.prerequisite.name))
"""


class AlreadyAssignedException(StaffException):
    def __str__(self):
        return _("You are already assigned to this job.")

    
"""
class UserNotInConferenceException(StaffException):
    def __str__(self):
        return __("You must first become a staff member for this conference.")
"""


class ExcludedJobException(StaffException):
    def __str__(self):
        return _("Job type not allowed for this user!")

    
class Job(Scheduleable):
    
    schedule_select_related = ('job_type', 'room', 'event', 'day')
    schedule_prefetch_related = ('staff_assigned',)

    conference = models.ForeignKey('core.Conference', db_index=True, on_delete=models.CASCADE)
    job_type = models.ForeignKey(JobType, on_delete=models.CASCADE)
    length = models.IntegerField(default=15)
    staff_assigned = models.ManyToManyField('core.User', blank=True, related_name='jobs_assigned')
    # staff_waitlist = models.ManyToManyField('core.User', blank=True, related_name='jobs_waitlisted')
    event = models.ForeignKey(
        'callforcontributions.Event',
        null=True,
        blank=True,
        help_text="If this job is tied to a specific event, select it here.",
        on_delete=models.CASCADE)
    # room.help_text="If this job is tied to a specific room, select it here."
    locked = models.BooleanField(
        default=True,
        help_text="If checked, this job can't be moved and will not be moved by auto generator. "
        "Does not affect whether this can be made released or draft.")
    auto_generated = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.job_type.name}" + (f" ({self.event.title})" if self.event is not None else "")

    # todo: abfangen wenn zeit geändert wird, personen benachrichtigen
    def get_persons_required(self):
        return self.staff_assigned

    get_persons_required.prefetch = 'staff_assigned'

    def save(self, *args, **kwargs):
        if self.id and self.job_type_id:
            self.conference = self.job_type.conference
        super().save(*args, **kwargs)

    def get_css_classes(self):
        required = self.job_type.staff_needed
        max = self.job_type.staff_max
        assigned = self.staff_assigned.count()
        
        if required > assigned:
            return "job-needs-persons"
        elif max is not None and assigned > max:
            return "job-too-many-persons"
        else:
            return "job-normal"

    def get_room_required(self):
        return None

    def get_type(self):
        return self.job_type
    
    def can_add_user(self, user, admin_assign=False):
        if self.version == self.RELEASED:
            raise ScheduleReleasedException()
        if not admin_assign and not self.job_type.can_self_assign:
            raise NoSelfAssignException()
        if self.staff_assigned.filter(id=user.id).exists():
            raise AlreadyAssignedException()
        if not admin_assign and self.job_type.staff_max is not None and self.staff_assigned.count() >= self.job_type.staff_max:
            raise StaffLimitException()
        if user in self.job_type.excluded_users.all():
            raise ExcludedJobException()
            
        has_skills = user.skill_set.values_list('id', flat=True).all()
        lacks_skills = self.job_type.requires_skills.exclude(id__in=has_skills).all()
        
        if len(lacks_skills) > 0:
            raise SkillLackException(lacks_skills)

        filter = (
            Q(version=Scheduleable.DRAFT) &
            Q(conference_id=self.conference_id) &
            Q(day_id=self.day_id) &
            (
                Q(end_datetime__gt=self.start_datetime, end_datetime__lte=self.end_datetime) |
                Q(start_datetime__gte=self.start_datetime, start_datetime__lt=self.end_datetime) |
                Q(start_datetime__lte=self.start_datetime, end_datetime__gte=self.end_datetime)))

        try:
            conflict = Job.objects.filter(filter).filter(
                staff_assigned=user
            ).all()[0]
        except (Job.DoesNotExist, IndexError):
            pass  # everything is fine!
        else:
            raise StaffDutiesException(f"{conflict.job_type} {conflict}")

        try:
            conflict = Event.objects.filter(filter).filter(
                speaker=user
            ).all()[0]
        except (Event.DoesNotExist, IndexError):
            pass  # everything is fine
        else:
            raise StaffDutiesException(f"{conflict.event_type} {conflict.title}")

        if admin_assign:
            if UserAvailabilityConflict.check_user_and_scheduleable(user, self) is not None:
                raise StaffException()
                
    @transaction.atomic
    def add_user(self, user):
        self.can_add_user(user)
        self.staff_assigned.add(user)
        
    def remove_user(self, user):
        if user in self.staff_assigned:
            self.staff_assigned.remove(user)
        # if user in self.staff_waitlist:
        #     self.staff_waitlist.remove(user)

    def person_list_html(self, for_admin=False):
        required = self.job_type.staff_needed
        max = self.job_type.staff_max
        assigned = self.staff_assigned.count()

        out = []

        for p in self.staff_assigned.order_by('username').all():
            if p not in self.job_type.excluded_users.all():
                s = "{} "
            else:
                s = "<span style='color: red;' title='User does not want this job!'>{} <b>↯</b></span> "

            if for_admin:
                s += f"""<button onclick="addremove_user('remove', {self.id}, {p.id});">-</button>"""

            user_string = format_html('<a href="{}">{}</a>', reverse('admin:core_user_change', args=(p.id,)), p.username)
                
            out.append(s.format(user_string))
        
        if required > assigned:
            out.append(f"<strong>+{required-assigned} needed</strong>")
        elif max is not None and assigned > max:
            out.append(f"<strong>{assigned-max} too many</strong>")
        
        return mark_safe("<br>".join(out))

    @property
    def person_list_html_admin(self):
        return self.person_list_html(for_admin=True)

    @property
    def available_users(self):
        users = User.objects.filter(conferencestaff__conference=self.conference).order_by('username').all()
        for u in users:
            try:
                self.can_add_user(u, admin_assign=True)
            except StaffException:
                continue
            yield u
                
    """
    @transaction.atomic
    def check_waitlist(self):
        while self.staff_assigned.count() < self.job_type.staff_max and self.staff_waitlist.count() > 0:
            for user in self.staff_waitlist.order_by('?').all():
                can, __ = self.can_add_user(user)
                if can:
                    self.add_user(user)
                    break
            else:
                # We did not find any suitable user.
                return
    """

        
class ConferenceStaffSettings(VersionableContainer):
    conference = models.OneToOneField('core.Conference', on_delete=models.CASCADE)
    description = models.TextField()
    is_public = models.BooleanField(_("Publicly visible"), default=False)
    versionable_model = Job
    
    def auto_generate(self):
        job_types = self.conference.jobtype_set.all()
        for j in job_types:
            j.auto_generate()

    def get_absolute_url(self):
        return reverse('staff:index', kwargs={'conference_id': self.conference.url_snippet})
