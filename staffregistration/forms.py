from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import ConferenceStaff, Job


class UserSkillForm(forms.Form):
    def __init__(self, user, skillset, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.skillset = skillset
        for skill in skillset:
            f = self.fn(skill)
            self.fields[f] = forms.BooleanField(
                label=skill.name,
                initial=(user in skill.users.all()),
                required=False,
                disabled=(not skill.can_self_assign))
            self.fields[f].instance = skill

    @staticmethod
    def fn(skill):
        return 'skill-{}'.format(skill.id)

    def save(self):
        self.clean()
        for skill in self.skillset:
            if not skill.can_self_assign:
                continue
            if self.cleaned_data[self.fn(skill)]:
                skill.users.add(self.user)
            else:
                skill.users.remove(self.user)


class ConferenceStaffForm(forms.Form):
    def __init__(self, user, conference, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.conference = conference
        self.entry = ConferenceStaff.objects.filter(
            conference=conference,
            user=user
        )
        self.entry_exists = self.entry.exists()
        self.jobs_assigned = Job.objects.filter(
            job_type__conference=conference,
            staff_assigned=user,
            version=Job.RELEASED
        ).exists()
        self.disabled = self.entry_exists and self.jobs_assigned
        self.fields['is_available'] = forms.BooleanField(
            label=_("I want to help out at this event!"),
            initial=self.entry_exists,
            required=False,
            disabled=self.disabled,
        )
        self.fields['is_available'].widget.attrs['class'] = 'css-checkbox'
        self.fields['notes'] = forms.CharField(
            required=False,
            initial=self.entry.get().notes if self.entry_exists else "",
            widget=forms.Textarea,
            label=_("Notes for the conference organizers:"))
            
    def save(self):
        self.clean()
        if self.disabled:
            return
        if not self.cleaned_data['is_available']:
            if self.entry_exists:
                self.entry.delete()
        else:
            o, created = ConferenceStaff.objects.get_or_create(
                conference=self.conference,
                user=self.user
            )
            o.notes = self.cleaned_data['notes']
            o.save()

