from django.contrib.sitemaps import Sitemap
from django.urls import reverse
from .views import IndexView, ArticleView
from django.core.paginator import Paginator

class NewsIndexViewSitemap(Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        objects = IndexView().get_queryset().all()
        paginator = Paginator(objects, IndexView.paginate_by)
        return paginator.page_range

    def location(self, page):
        if page == 1:
            return reverse('news:index')
        return reverse('news:index', kwargs={'page': page})


class NewsArticleViewSitemap(Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return ArticleView().get_queryset()
