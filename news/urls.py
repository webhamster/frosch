from django.conf.urls import  url
from .views import IndexView, ArticleView
from .feeds import NewsFeed
from .sitemaps import NewsIndexViewSitemap, NewsArticleViewSitemap

app_name = 'news'

sitemaps = {
    'news_static': NewsIndexViewSitemap(),
    'news_article': NewsArticleViewSitemap(),
}

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^news/page/(?P<page>[0-9]+)/$', IndexView.as_view(), name='index'),
    url(r'^news/article/(?P<slug>[^/]+)', ArticleView.as_view(), name='article-detail'),
    url(r'news/feed/', NewsFeed(), name='feed'),
]
