from .models import Article
from django.views.generic import ListView, DetailView
from core.view_utils import DetailViewTranslationURLProviderMixin, PreviousConferenceMixin


class IndexView(PreviousConferenceMixin, ListView):
    template_name = 'news/index.html'
    queryset = Article.objects.filter(is_active=True)
    ordering = '-published'
    paginate_by = 20
    context_object_name = 'articles'
    

class ArticleView(DetailViewTranslationURLProviderMixin, DetailView):
    model = Article
    queryset = Article.objects.filter(is_active=True)
