from django.contrib import admin
from .models import Article
from modeltranslation.admin import TranslationAdmin
from pagedown.widgets import AdminPagedownWidget
from django.db import models


@admin.register(Article)
class ArticleAdmin(TranslationAdmin):
    list_display = ('title', 'published', 'is_active',)
    list_editable = ('is_active',)
    ordering = ('published',)
    prepopulated_fields = {"slug": ("title",)}
    formfield_overrides = {
        models.TextField: {'widget': AdminPagedownWidget},
    }
