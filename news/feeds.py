from django.contrib.syndication.views import Feed
from django.urls import reverse
from .models import Article
from django.utils.translation import gettext as _
from .views import IndexView
from django.conf import settings
import markdown_deux


class NewsFeed(Feed):
    title = f"{settings.FROSCH_NAME}"
    link = "/"
    description = ""

    def items(self):
        return IndexView().get_queryset()[:5]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return markdown_deux.markdown(item.text)

    def item_link(self, item):
        return reverse('news:article-detail', args=[item.slug])

    def item_pubdate(self, item):
        return item.published
