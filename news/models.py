from django.db import models
from django.utils import timezone
from filer.fields.image import FilerImageField
from django.urls import reverse


class Article(models.Model):
    title = models.CharField(max_length=200)
    published = models.DateTimeField(default=timezone.now)
    slug = models.SlugField(max_length=100)
    text = models.TextField()
    is_active = models.BooleanField(default=False)
    image = FilerImageField(blank=True, null=True, on_delete=models.SET_NULL)
    video = models.URLField(blank=True, null=True)

    def get_absolute_url(self):
        return reverse('news:article-detail', args=[str(self.slug)])
