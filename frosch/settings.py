"""
Django settings for frosch project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ThisIsJustADummy'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

INTERNAL_IPS = ['127.0.0.1', '::']

# Application definition

INSTALLED_APPS = (
    'modeltranslation',
    'core',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'django.forms',
    'compressor',
    'leaflet',
    'djgeojson',
    'debug_toolbar',
    'visitorregistration',
    'callforcontributions',
    'staffregistration',
    'sponsormanager',
    'captcha',
    'djangoformsetjs',
    'pagedown',
    'markdown_deux',
    'post_office',
    'bootstrap4',
    'hijack',
    'hijack_admin',
    'compat',
    'easy_thumbnails',
    'filer',
    'mptt',
    'news',
    'pages',
    'robots',
    'regex_redirects',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    #    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'regex_redirects.middleware.RedirectFallbackMiddleware',
)

MIDDLEWARE = MIDDLEWARE_CLASSES

MESSAGE_TAGS = {
    messages.ERROR: 'danger'
}

ROOT_URLCONF = 'frosch.urls'

WSGI_APPLICATION = 'frosch.wsgi.application'

SITE_ID = 1

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en'

BADGE_LANGUAGE = 'de'

TIME_ZONE = 'Europe/Berlin'

USE_I18N = True

USE_TZ = True

USE_L10N = True

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

LANGUAGES = (
    ('de', _('German')),
    ('en', _('English')),
)


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            'templates'
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                "django.template.context_processors.request",
                'django.contrib.messages.context_processors.messages',
                "core.context_processors.settings",
                "core.context_processors.conferences",
                "core.context_processors.contact_form",
                "core.context_processors.login_form",
                "pages.context_processors.pagemounts",
                "core.context_processors.partners",
            ],
        },
    },
]


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR,'log', 'frosch.log'),
        },
    },
}


FORM_RENDERER = 'django.forms.renderers.TemplatesSetting'

FROSCH_CACHE_TIMEOUT = 600

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': os.path.join(BASE_DIR, 'cache'),
    },
    'file_upload_cache': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': os.path.join(BASE_DIR, 'cache'),
    },
}

SUIT_CONFIG = {
    'LIST_PER_PAGE': 100
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

AUTH_USER_MODEL = 'core.User'

EMAIL_BACKEND = 'post_office.EmailBackend'

POST_OFFICE = {
    'BACKENDS': {
        'default': 'django.core.mail.backends.console.EmailBackend'
    }
}

DEFAULT_FROM_EMAIL = 'kontakt@piandmore.de'

ADMINS = [('PAM', 'kontakt@piandmore.de'), ]

# CAPTCHA

CAPTCHA_FLITE_PATH = '/usr/bin/flite'

CAPTCHA_CHALLENGE_FUNCT = 'captcha.helpers.math_challenge'

CAPTCHA_FONT_SIZE = 35

CAPTCHA_LETTER_ROTATION = (-20, 20)

FROSCH_NAME = _('Pi and More')

FROSCH_MAX_UPLOAD_SIZE = 10 * 1024 * 1024

FROSCH_PIWIK_COOKIE_DOMAIN = '*.piandmore.de'

FROSCH_PIWIK_LOCATION = '//piwik.gtrs.de/'

FROSCH_PIWIK_SITE_ID = 6

FORMAT_MODULE_PATH = 'frosch.formats'

FROSCH_BASE_URL = 'https://piandmore.de'

FROSCH_CONFERENCE_PREFIX = 'conference/'

PAGEDOWN_WIDGET_CSS = ('pagedown.css',)

FILE_UPLOAD_TEMP_DIR = os.path.join(BASE_DIR, 'tmp')

MARKDOWN_DEUX_STYLES = {
    "default": {
        "extras": {
            "code-friendly": None,
        },
        "safe_mode": "escape",
    },
    "trusted": {
        "extras": {
            "code-friendly": None,
        },
        # Allow raw HTML (WARNING: don't use this for user-generated
        # Markdown for your site!).
        "safe_mode": False,
    }
}

TEST_RUNNER = "django_nose.NoseTestSuiteRunner"

# Dump the schedule to files each time a new version of the schedule is released in the backend.
SCHEDULE_DUMP = True
# Using these settings you can control the dumping of the schedule into external files.
SCHEDULE_DUMP_CONFIG = {
    'html': [ # suitable to dump every day into a list of events
        {
            'TEMPLATE': 'callforcontributions/schedule-dump/schedule.html', # path to template, can be relative to template dir of callforcontributions app (see django documentation on how to determine template paths)
            'OUTPUT': '/tmp/schedule-{conference}-{lang}-day{day}-list.html', # output filename, can be relative to working directory of frosch or absolute. Use placeholders {lang}, {day}, and {name} (name from above)
        },
        #{
        #    'TEMPLATE': 'callforcontributions/schedule-dump/schedule-grid.html',
        #    'OUTPUT': '/tmp/schedule-{conference}-{lang}-day{day}-grid.html',
        #}
    ],
    'xml': [ # suitable to dump all events and conference day into one file
        {
            'TEMPLATE': 'callforcontributions/schedule-dump/schedule.xml',
            'OUTPUT': '/tmp/schedule-{conference}.xml'
        }
    ]
}

FROSCH_SCHEDULE_STEP = 5
FROSCH_SCHEDULE_BIG_STEP = 30

""" Install django debug toolbar """
DEBUG_TOOLBAR_PATCH_SETTINGS = False
DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
    'debug_toolbar.panels.profiling.ProfilingPanel',
]

HIJACK_REGISTER_ADMIN = False
HIJACK_ALLOW_GET_REQUESTS = True
HIJACK_USE_BOOTSTRAP = True

THUMBNAIL_HIGH_RESOLUTION = True

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

MEDIA_URL = '/media/'

FILER_STORAGES = {
    'public': {
        'main': {
            'ENGINE': 'filer.storage.PublicFileSystemStorage',
            'OPTIONS': {
                'location': os.path.join(MEDIA_ROOT, 'files'),
                'base_url': f'{MEDIA_URL}/files/',
            },
            'UPLOAD_TO': 'filer.utils.generate_filename.randomized',
            'UPLOAD_TO_PREFIX': 'filer_public',
        },
        'thumbnails': {
            'ENGINE': 'filer.storage.PublicFileSystemStorage',
            'OPTIONS': {
                'location': os.path.join(MEDIA_ROOT, 'thumbnails'),
                'base_url': f'{MEDIA_URL}/thumbnails/',
            },
        },
    },
}

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'sass_processor.finders.CssFinder',
    'compressor.finders.CompressorFinder',
]

STATIC_ROOT = 'static/'

STATICFILES_DIRS = [
    ('bootstrap', os.path.join(BASE_DIR, 'lib/bootstrap/dist/js')),
    ('awesome-markers', os.path.join(BASE_DIR, 'lib/Leaflet.awesome-markers/dist')),
    ('iscroll', os.path.join(BASE_DIR, 'lib/iscroll/build')),
    ('lightbox', os.path.join(BASE_DIR, 'lib/lightbox/dist')),
    ('jquery', os.path.join(BASE_DIR, 'lib/jquery')),
    ('jquery-spin', os.path.join(BASE_DIR, 'lib/jquery-spin')),
    ('Lato2OFLWeb', os.path.join(BASE_DIR, 'lib/Lato2OFLWeb')),
    ('fontawesome', os.path.join(BASE_DIR, 'lib/fontawesome-free-5.1.0-web')),
]

FROSCH_SPONSOR_GRID_GRANULARITY = 32

ROBOTS_SITEMAP_VIEW_NAME = 'sitemap_index'

ROBOTS_CACHE_TIMEOUT = 60*60*24

LIBSASS_PRECISION = 8

LIBSASS_OUTPUT_STYLE = 'compressed'

COMPRESS_PRECOMPILERS = (
    ('text/x-scss', 'django_libsass.SassCompiler'),
)

COMPRESS_CSS_FILTERS = [
    'compressor.filters.css_default.CssAbsoluteFilter',
]

COMPRESS_CSS_HASHING_METHOD = 'mtime'


### NO DEFAULT SETTING BELOW THIS POINT ###
try:
    from .settings_local import *
except ImportError:
    print("Not using local settings file settings_local.py")

