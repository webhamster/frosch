from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.views.i18n import set_language
from django.conf import settings
from django.views.static import serve as static_serve
from django.contrib.sitemaps import views as sitemap_views
from core.views import SitemapLangIndex, RedirectToNextConference
from core.urls import sitemaps as core_sitemaps
from core.conference_urls import sitemaps as core_conference_sitemaps
from news.urls import sitemaps as news_sitemaps
from pages.urls import sitemaps as pages_sitemaps
from callforcontributions.urls import sitemaps as call_sitemaps
from callforcontributions.conference_urls import sitemaps as call_conference_sitemaps
from visitorregistration.urls import sitemaps as registration_sitemaps
from staffregistration.urls import sitemaps as staff_sitemaps
from django.views.decorators.cache import cache_page


sitemaps = {}
sitemaps.update(core_sitemaps)
sitemaps.update(core_conference_sitemaps)
sitemaps.update(news_sitemaps)
sitemaps.update(pages_sitemaps)
sitemaps.update(call_sitemaps)
sitemaps.update(call_conference_sitemaps)
sitemaps.update(registration_sitemaps)
sitemaps.update(staff_sitemaps)



urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^lang/', set_language, name="set-lang"),
    url(r'^sitemap\.xml$', SitemapLangIndex.as_view(), name='sitemap_index'),
    url(r'^robots\.txt', include('robots.urls')),
]

conf_pref = settings.FROSCH_CONFERENCE_PREFIX #}|past-events/|archiv/)"

urlpatterns += i18n_patterns(
    url(r'', include('core.urls', namespace='core')),
    url(fr'^{conf_pref}', include('core.conference_urls', namespace='core_conference')),
    url(
        r'^next/.*$',
        cache_page(settings.FROSCH_CACHE_TIMEOUT)(RedirectToNextConference.as_view()),
        name='next_conference'),
    url(fr'^{conf_pref}(?P<conference_id>[^/]+)/call/',
        include('callforcontributions.conference_urls', namespace='call')),
    url(r'', include('callforcontributions.urls', namespace='noconfcall')),
    url(fr'^{conf_pref}(?P<conference_id>[^/]+)/registration/',
        include('visitorregistration.urls', namespace='visitorregistration')),
    url(fr'^{conf_pref}(?P<conference_id>[^/]+)/staff/',
        include('staffregistration.urls', namespace='staff')),
    url(r'^accounts/', include('core.account_urls', namespace='accounts')),
    url(r'^hijack/', include('hijack.urls', namespace='hijack')),
    url(r'', include('news.urls', namespace='news')),
    url(r'', include('pages.urls', namespace='page')),
    url(r'^sitemap\.xml$', sitemap_views.sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
        url(r'^media/(?P<path>.*)$',
            static_serve, {'document_root': settings.MEDIA_ROOT}),
    ]

# set the error handlers to defaults to prevent other modules (e.g., suit) to overwrite these handlers
handler400 = 'django.views.defaults.bad_request'
handler403 = 'django.views.defaults.permission_denied'
handler404 = 'django.views.defaults.page_not_found'
handler500 = 'django.views.defaults.server_error'
