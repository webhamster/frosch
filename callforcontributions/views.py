from datetime import date
from django.contrib import messages
from django.db import transaction
from django.db.models import Q
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, reverse

from .utils import invite_speaker
from active_login_required import active_login_required
from callforcontributions.forms import (EventLinkFormSet, EventMediaForm,
                                        EventUserForm, InviteSpeakersForm, WithdrawForm)
from callforcontributions.models import (Event, EventMedia, EventStatus,
                                         EventType)
from core.models import Conference
from core.view_utils import UserAvailabilityViewMixin
from django.forms import Form
from django.forms.models import inlineformset_factory
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.views.generic import ListView, View
from django.views.generic.base import TemplateResponseMixin


def notify_admins(request, template, event, reason='', changes=None):
    context = {
        'event': event,
        'conference': event.conference,
        'reason': reason,
        'changes': changes,
        'url': request.build_absolute_uri(reverse(
            'admin:callforcontributions_event_change', args=[event.id]))
    }
    event.conference.send_templated_mail_to_admins(
        request,
        'callforcontributions.notify-%s' % template,
        context)


def handle_submission_form(request, instance, conference, event_type, return_to):
    is_new = instance is None

    if request.GET.get('action', request.POST.get('action', '')) == 'check-and-add':
        action = 'check-and-add'
        extra = 1
    else:
        action = ''
        extra = 0
    EventMediaFormSet = inlineformset_factory(
        Event, EventMedia, EventMediaForm, max_num=10, extra=extra, validate_max=True
    )

    if request.method == 'POST':
        if conference.eventsettings.is_archived:
            raise Http404()
        changes = {}
        if is_new:
            instance = Event(conference=conference, event_type=event_type)
        form = EventUserForm(conference, event_type, request.POST, instance=instance)
        mediaset = EventMediaFormSet(request.POST, request.FILES, instance=instance)
        linkset = EventLinkFormSet(request.POST, instance=instance)
        invite = InviteSpeakersForm(request.POST)
        if mediaset.is_valid():
            mediaset.save()
        if form.is_valid() and mediaset.is_valid() and linkset.is_valid() and invite.is_valid():
            form.save()
            if is_new:
                form.instance.speaker.add(request.user)
                form.instance.save()
            mediaset.save()
            linkset.save()
            # Invite Speakers
            invite_speakers = invite.cleaned_data['invite']
            if len(invite_speakers):
                for speaker in invite_speakers:
                    invite_speaker(request, instance, speaker)
                messages.success(request, _("Invited the following speakers to the event: %(speakers)s") % {
                    'speakers': ', '.join(invite_speakers)
                })

            if is_new:
                # Send mails
                notify_admins(request, 'submit', form.instance)
                request.user.send_templated_mail(
                    request,
                    'callforcontributions.submission',
                    {
                        'event': form.instance,
                        'conference': conference,
                        'edit_url': request.build_absolute_uri(reverse(
                            'call:edit', args=[conference.url_snippet, form.instance.id]))
                    })
                messages.success(request, _("Thanks for submitting \"%(title)s\"!") % {'title': form.instance.title})
            else:
                # Send mails if changes were made ...
                if form.has_changed() or mediaset.has_changed() or linkset.has_changed():
                    changes = {}
                    for field in form.changed_data:
                        try:
                            changes[field] = str(getattr(instance, field))
                        except StopIteration as e:
                            # We assume that this must be a manager object.
                            try:
                                changes[field] = ', '.join(str(x) for x in getattr(instance, field).all())
                            except Exception as e:
                                # And we notify ourselves if this assumption was wrong ...
                                changes[field] = "Exception while retrieving changes: %s" % e
                            
                    if mediaset.has_changed():
                        changes['mediaset'] = _("Changed attached media")
                    if linkset.has_changed():
                        changes['linkset'] = _("Changed attached links")

                    notify_admins(request, 'change', instance, changes=changes)
                    messages.success(request, _('Event "%(title)s" saved.') % {'title': instance.title})
            if action != 'check-and-add':
                return HttpResponseRedirect(reverse('call:index', kwargs={'conference_id': conference.url_snippet}))
            else:
                return HttpResponseRedirect(
                    reverse(
                        'call:edit',
                        kwargs={
                            'conference_id': conference.url_snippet,
                            'event_id': form.instance.id
                        }) + "?action=check-and-add#mediaset")
        else:
            messages.error(request, _('Please check your submission.'))
    else:
        form = EventUserForm(conference, event_type, instance=instance)
        mediaset = EventMediaFormSet(instance=instance)
        linkset = EventLinkFormSet(instance=instance)
        invite = InviteSpeakersForm()
    return render(request, 'callforcontributions/submission.html', {
        'form': form,
        'mediaset': mediaset,
        'linkset': linkset,
        'invite_form': invite,
        'conference': conference,
        'event_type': event_type,
        'associate_speakers': request.POST.get('associate_speakers', ''),
        'return_to': return_to,
    })


class Index(TemplateResponseMixin, UserAvailabilityViewMixin, View):
    template_name = 'callforcontributions/index.html'
    exclude_user_days = Q(public_start=None) | Q(public_end=None) | Q(date__lt=date.today())
    return_to = 'call:index'

    def get(self, request, conference_id):
        conference = get_object_or_404(Conference, url_snippet=conference_id)
        context = {
            'conference': conference,
            'logged_in': False,
        }
        if request.user.is_authenticated() and request.user.is_active:
            submissions = Event.objects.filter(conference=conference, speaker=request.user, version=Event.DRAFT)
            forms, has_full_availability = self.get_availability_forms(request, conference)
            context.update({
                'logged_in': True,
                'submissions': submissions,
                'action': request.GET.get('action', ''),
                'useravailability_forms': forms,
                'has_full_availability': has_full_availability,
            })
        return self.render_to_response(context)

            
@active_login_required
def submit(request, conference_id, event_type_id):
    conference = get_object_or_404(
        Conference,
        url_snippet=conference_id,
        eventsettings__call_open__lte=timezone.now(),
        eventsettings__call_close__gte=timezone.now()
    )
    event_type = get_object_or_404(EventType, url_snippet=event_type_id)
    return handle_submission_form(
        request,
        instance=None,
        conference=conference,
        event_type=event_type,
        return_to=reverse('call:submit', args=(conference.url_snippet, event_type.url_snippet)))


@active_login_required
def edit(request, conference_id, event_id):
    conference = get_object_or_404(Conference, url_snippet=conference_id)
    event = get_object_or_404(
        Event,
        conference=conference,
        speaker=request.user,
        id=event_id,
        version=Event.DRAFT,
    )
    return handle_submission_form(
        request,
        instance=event,
        conference=conference,
        event_type=event.event_type,
        return_to=reverse('call:edit', args=(conference.url_snippet, event_id)))


@transaction.atomic
@active_login_required
def confirm(request, conference_id, event_id):
    conference = get_object_or_404(Conference, url_snippet=conference_id)
    event = get_object_or_404(
        Event,
        conference=conference,
        speaker=request.user,
        id=event_id,
        version=Event.DRAFT,
    )
    
    if event.status == EventStatus.CONFIRMED:
        messages.success(request, _("This event was already confirmed."))
        return HttpResponseRedirect(reverse('call:index', kwargs={'conference_id': conference.url_snippet}))
    elif event.status != EventStatus.UNCONFIRMED:
        messages.error(request, _("This event cannot be confirmed."))
        return HttpResponseRedirect(reverse('call:index', kwargs={'conference_id': conference.url_snippet}))
        
    if request.method == 'POST':
        if conference.eventsettings.is_archived:
            raise Http404()
        form = Form(request.POST)
        if form.is_valid():
            event.status = EventStatus.CONFIRMED
            event.save()
            notify_admins(request, 'confirm', event)
            messages.success(request, _('Confirmed event "%(title)s".') % {'title': event.title})
            return HttpResponseRedirect(reverse('call:index', kwargs={'conference_id': conference.url_snippet}))
    
    else:
        form = Form()
    return render(request, 'callforcontributions/ask-confirm.html', {
        'form': form,
        'conference': conference,
        'event': event,
    })


@transaction.atomic
@active_login_required
def withdraw(request, conference_id, event_id):
    conference = get_object_or_404(Conference, url_snippet=conference_id)
    event = get_object_or_404(
        Event,
        conference=conference,
        speaker=request.user,
        id=event_id,
        status__lt=EventStatus.CANCELED,
        version=Event.DRAFT,
    )
    if conference.eventsettings.is_archived:
        raise Http404()
    if request.method == 'POST':
        form = WithdrawForm(request.POST)
        if form.is_valid():
            event.withdraw_or_cancel()
            notify_admins(request, 'withdraw', event, reason=form['reason'].value)
            messages.success(request, _('Withdrew event "%(title)s".') % {'title': event.title})
            return HttpResponseRedirect(reverse('call:index', kwargs={'conference_id': conference.url_snippet}))
    
    else:
        form = WithdrawForm()
    return render(request, 'callforcontributions/ask-withdraw.html', {
        'form': form,
        'conference': conference,
        'event': event,
    })


@transaction.atomic
@active_login_required
def delete(request, conference_id, event_id):
    conference = get_object_or_404(Conference, url_snippet=conference_id)
    event = get_object_or_404(
        Event,
        conference=conference,
        speaker=request.user,
        id=event_id,
        status__lt=EventStatus.CANCELED,
        version=Event.DRAFT,
    )
    if conference.eventsettings.is_archived:
        raise Http404()
    if request.method == 'POST':
        form = WithdrawForm(request.POST)
        if form.is_valid():
            notify_admins(request, 'delete', event, reason=form['reason'].value)
            messages.success(request, _('Deleted event "%(title)s".') % {'title': event.title})
            event.delete()
            return HttpResponseRedirect(reverse('call:index', kwargs={'conference_id': conference.url_snippet}))
    
    else:
        form = WithdrawForm()
    return render(request, 'callforcontributions/ask-delete.html', {
        'form': form,
        'conference': conference,
        'event': event,
    })
    

@transaction.atomic
@active_login_required
def undo_withdraw(request, conference_id, event_id):
    conference = get_object_or_404(Conference, url_snippet=conference_id)
    event = get_object_or_404(
        Event,
        conference=conference,
        speaker=request.user,
        id=event_id,
        status=EventStatus.WITHDRAWN,
        version=Event.DRAFT,
    )
    if conference.eventsettings.is_archived:
        raise Http404()
    if request.method == 'POST':
        form = Form(request.POST)
        if form.is_valid():
            event.status = EventStatus.NEW
            event.save()
            notify_admins(request, 'undo-withdraw', event)
            messages.success(request, _('Event "%(title)s" restored.') % {'title': event.title})
            return HttpResponseRedirect(reverse('call:index', kwargs={'conference_id': conference.url_snippet}))
    
    else:
        form = Form()
    return render(request, 'callforcontributions/ask-undo-withdraw.html', {
        'form': form,
        'conference': conference,
        'event': event,
    })


@transaction.atomic
@active_login_required
def associate(request, conference_id, event_id, token):
    conference = get_object_or_404(Conference, url_snippet=conference_id)
    event = get_object_or_404(
        Event,
        conference=conference,
        id=event_id,
        associate_token=token,
        version=Event.DRAFT,
    )
    if conference.eventsettings.is_archived:
        raise Http404()
    if request.method == 'POST':
        form = Form(request.POST)
        if form.is_valid():
            event.speaker.add(request.user)
            event.save()
            notify_admins(request, 'associate', event)
            messages.success(request, _('You are now a co-speaker for the event "%(title)s".') % {'title': event.title})
            return HttpResponseRedirect(reverse('call:index', kwargs={'conference_id': conference.url_snippet}))
    
    else:
        form = Form()
    return render(request, 'callforcontributions/ask-associate.html', {
        'form': form,
        'conference': conference,
        'event': event,
        'token': token,
    })


@active_login_required
def download_event_media(request, conference_id, media_id):
    print ("XXXXXX")
    conference = get_object_or_404(Conference, url_snippet=conference_id)
    media = get_object_or_404(
        EventMedia,
        event__speaker=request.user,
        event__conference=conference,
        event__version=Event.DRAFT,
        file=media_id)
    response = HttpResponse(media.file, content_type='application/octet-stream')
    response['Content-Disposition'] = 'attachment; filename=%s' % media.filename()
    return response


def download_event_media_public(request, conference_id, media_id):
    conference = get_object_or_404(Conference, url_snippet=conference_id)
    media = get_object_or_404(
        EventMedia,
        is_public=True,
        event__conference=conference,
        id=int(media_id))
    response = HttpResponse(media.file, content_type='application/octet-stream')
    response['Content-Disposition'] = 'attachment; filename=%s' % media.filename()
    return response


class MyEventList(ListView):
    model = Event
    context_object_name = 'events'
    template_name = 'callforcontributions/myeventlist.html'

    def get_queryset(self):
        return self.request.user.events.filter(version=Event.DRAFT).prefetch_related('conference')
