from django.template import RequestContext
from django.template.loader import get_template

from django.urls import reverse
from frosch import settings
from .models import ConferenceEventSettings, Event, EventStatus
from django.utils.translation import get_language, override
from post_office import mail
from core.view_utils import ScheduleLayer, ConferenceScheduleView
from core.models import User, Room


def notify_authors(request, event, template, allow_accept, additional_data={}, dry_run=False):
    if allow_accept:
        accept_url = request.build_absolute_uri(reverse(
            'call:confirm', args=[event.conference.url_snippet, event.id]))
    else:
        accept_url = ''

    data = {
        'event': event,
        'conference': event.conference,
        'accept_url': accept_url
    }
    data.update(additional_data)
            
    for user in event.speaker.all():
        yield user.send_templated_mail(request, template, data, dry_run=dry_run)


def invite_speaker(request, event, email):
    url = request.build_absolute_uri(reverse(
        'call:associate', args=[event.conference.url_snippet, event.id, event.associate_token]))
    
    # We hack the URL to remove the language prefix
    url = url.replace('/%s/' % get_language(), '/')
    mail.send(
        template='callforcontributions.invite-speaker',
        recipients=[email],
        bcc=event.conference.get_notification_addresses(),
        context=RequestContext(
            request,
            {
                'event': event,
                'conference': event.conference,
                'accept_url': url,
                'user': request.user,
                'FROSCH_NAME': settings.FROSCH_NAME,
            })
    )

        
class ScheduleDumper():
    """Outputs the schedule of a conference as an embeddable HTML
    document, Pentabarf XML and possibly more."""

    CONFIG = {}  # Set this using SCHEDULE_DUMP_CONFIG in settings (see there for example)

    def __init__(self, conference):
        self.conference = conference
        if getattr(settings, 'SCHEDULE_DUMP_CONFIG', False):
            self.CONFIG.update(settings.SCHEDULE_DUMP_CONFIG)
    
    def dump(self):
        self.conferenceeventsettings = ConferenceEventSettings.objects.get(conference=self.conference)
        if self.conferenceeventsettings.last_draft_released == None:
            v = Event.DRAFT
        else:
            v = Event.RELEASED
            
        # all events that are confirmed or accepted
        self.events = Event.objects.filter(
            conference=self.conference,
            status__in=[EventStatus.CONFIRMED, EventStatus.UNCONFIRMED, EventStatus.CANCELED],
            version=v
            ).order_by('day__start', 'start_datetime', 'room__sort_key', 'room__name')

        for format_, configs in self.CONFIG.items():
            getattr(self, "dump_" + format_)(configs)

    def dump_html(self, configs):
        dayno = 0
        for d in self.conference.conferenceday_set.exclude(public_start=None).exclude(public_end=None).order_by('date').all():

            events = self.events.filter(day_id=d.id).all()

            speakers = User.objects.filter(event__in=self.events.filter(hide_speakers=False)).distinct().order_by('username').all()

            rooms = Room.objects.filter(event__in=events.filter(scheduled=Event.SS_DTR)).distinct().order_by('sort_key', 'name')
            
            active_layer = ScheduleLayer(events.exclude(status=EventStatus.CANCELED).all())
            active_layer.template = 'callforcontributions/schedule-dump/schedule-event.html'
            scheduleset = ConferenceScheduleView(
                self.conference,
                self.conference.conferenceday_set.filter(id=d.id),
                rooms,
                active_layer,
                public_times=True,
            )

            context = {
                'eventstatus': EventStatus,
                'conferenceeventsettings': self.conferenceeventsettings,
                'events': events,
                'events_schedule': events.filter(scheduled=Event.SS_DTR),
                'events_allday': events.filter(scheduled=Event.SS_DR),
                'scheduleset': scheduleset,
                'speakers': speakers,
            }
        
            for lang in settings.LANGUAGES:
                with override(lang[0]):
                    for config in configs:
                        fn = config['OUTPUT'].format(
                            conference=self.conference.url_snippet,
                            lang=lang[0],
                            day=dayno,
                        )
                        t = get_template(config['TEMPLATE'])
                        with open(fn, 'w', encoding='utf-8') as f:
                            f.write(str(t.render(context)))

            dayno += 1

        
    def dump_xml(self, configs):
        events = self.events.exclude(status=EventStatus.CANCELED)
        grid = []
        for d in self.conference.conferenceday_set.exclude(public_start=None).exclude(public_end=None).order_by('date').all():
            daygrid = []
            for room in self.conferenceeventsettings.conference.rooms.all():
                ev = events.filter(room=room, day=d).all()
                daygrid.append((room, list(ev)))
                
            grid.append((d, daygrid))

        context = {
            'eventstatus': EventStatus,
            'start_date': grid[0][0].date,
            'end_date': grid[-1][0].date,
            'conferenceeventsettings': self.conferenceeventsettings,
            'grid': grid,
            'settings': settings,
        }
        
        for config in configs:
            fn = config['OUTPUT'].format(
                conference=self.conference.url_snippet,
            )
            t = get_template(config['TEMPLATE'])
            with open(fn, 'w', encoding='utf-8') as f:
                f.write(str(t.render(context)))
    
