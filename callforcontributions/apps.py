from django.apps import AppConfig

class CallforcontributionsAppConfig(AppConfig):
    name = 'callforcontributions'
    verbose_name = 'Call for Contributions'
