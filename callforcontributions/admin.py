from django import forms
from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.db.models import Q
from django.db import models
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from django.template import defaultfilters
from django.utils.html import mark_safe
from django.utils.translation import override, ugettext_lazy as _
from pagedown.widgets import AdminPagedownWidget

from callforcontributions.models import (
    ConferenceEventSettings, Event,
    EventAsset, EventLanguage, EventLength, EventLink, EventMedia,
    EventStatus, EventTag, EventTrack, EventType)
# from callforcontributions.utils import ScheduleDumper, notify_authors
from core.admin_utils import ConferenceListFilter
from core.models import User, Conference, Room
from django.urls import reverse
from modeltranslation.admin import TranslationAdmin, TranslationStackedInline
from .utils import notify_authors
from core.view_utils import ScheduleLayer, ConferenceScheduleView
from datetime import datetime


class EventLinkInline(admin.TabularInline):
    model = EventLink

    
class EventMediaInline(admin.TabularInline):
    model = EventMedia

    
class EventAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EventAdminForm, self).__init__(*args, **kwargs)

    class Meta:
        widgets = {
            'description': AdminPagedownWidget()
        }

        
class EventTypeAdminForm(forms.ModelForm):
    class Meta:
        widgets = {}
        widgets.update(dict(
            ("%s_%s" % (name, lang), AdminPagedownWidget())
            for name in ['short_description', 'long_description']
            for lang, _ in settings.LANGUAGES
        ))

        
@admin.register(EventType)
class EventTypeAdmin(TranslationAdmin):
    prepopulated_fields = {
        "url_snippet": ("name_en",),
    }
    form = EventTypeAdminForm
    list_display = ('url_snippet', 'name', 'used_in', 'css_class', 'booklet_headline', 'sort_key', )
    fields = ('name', 'url_snippet', 'css_class', 'short_description', 'long_description',
              'allowed_lengths', 'allowed_assets', 'allowed_tags', 'booklet_headline', 'sort_key',
              'hide_in_lists')
    list_editable = ('sort_key', )

    def used_in(self, eventtype):
        return ", ".join(eventtype.conferenceeventsettings_set.values_list('conference__short_name', flat=True))


@admin.register(EventAsset)
class EventAssetAdmin(TranslationAdmin):
    list_display = ('name', 'used_in', )
    
    def used_in(self, eventasset):
        c = Conference.objects.filter(
            eventsettings__event_types__allowed_assets=eventasset.id
        ).distinct().values_list('short_name', flat=True)
        return ", ".join(c)

    
for C in (EventTag, EventLanguage, EventLength, EventTrack):
    @admin.register(C)
    class DummyAdmin(TranslationAdmin):
        pass

    
admin.site.register(EventMedia)


class EventStatusListFilter(admin.SimpleListFilter):
    NOT_CONFIRMED = 'notconfirmed'
    ACCEPTED = 'accepted'
    
    title = "Status"
    parameter_name = 'status'
    
    def lookups(self, request, model_admin):
        return ((self.NOT_CONFIRMED, _("Not Accepted/Confirmed")),
                (self.ACCEPTED, _("Accepted")),) + EventStatus.descriptions
                
    def queryset(self, request, queryset):
        if self.value() is not None:
            if self.value() == self.NOT_CONFIRMED:
                return queryset.exclude(status=EventStatus.CONFIRMED)
            elif self.value() == self.ACCEPTED:
                return queryset.filter(Q(status=EventStatus.CONFIRMED) | Q(status=EventStatus.UNCONFIRMED))
            else:
                return queryset.filter(status=self.value())
        return queryset


class EventTypeListFilter(admin.SimpleListFilter):
    title = "Type"
    parameter_name = 'type'
    
    def lookups(self, request, model_admin):
        return [(x.url_snippet, x.name) for x in EventType.objects.order_by('name')]

    def queryset(self, request, queryset):
        if self.value() is not None:
            return queryset.filter(event_type__url_snippet=self.value())
        return queryset


class RoomListFilter(admin.SimpleListFilter):
    title = "Room"
    parameter_name = 'room'
    
    def lookups(self, request, model_admin):
        return [
            (x.id, x.name) for x in Room.objects.filter(
                event__isnull=False).distinct().order_by('name')]

    def queryset(self, request, queryset):
        if self.value() is not None:
            return queryset.filter(room=self.value())
        return queryset

    
class EventSpeakerListFilter(admin.SimpleListFilter):
    title = "Speaker"
    parameter_name = 'speaker'
    
    def lookups(self, request, model_admin):
        return [
            (x.id, x.username)
            for x in User.objects.filter(event__isnull=False).distinct().order_by('username')
        ]

    def queryset(self, request, queryset):
        if self.value() is not None:
            return queryset.filter(speaker=self.value())
        return queryset


def mailpreview(orig_func):
    template = 'core/admin/mailpreview.html'
    
    def actual_func(self, request, *args, **kwargs):
        if request.method == 'POST' and request.POST.get('run_now'):
            emails = list(orig_func(self, request, *args, dry_run=False, **kwargs))
            self.message_user(request, f"Sent {len(emails)} mails.")
        else:
            emails = list(orig_func(self, request, *args, dry_run=True, **kwargs))
            return render(request, template, {
                'emails': emails
            })
    return actual_func

    
@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    ordering = (
        'status',
        'title'
    )
    list_display = (
        'title',
        'event_type',
        'show_speakers',
        'show_languages',
        'track',
        'show_datetime',
        'room',
        'status',
        'status_colorizer',
    )
    list_editable = (
        'status',
        'room',
        'track',
    )
    list_filter = (
        ConferenceListFilter,
        EventStatusListFilter,
        EventTypeListFilter,
        RoomListFilter,
        EventSpeakerListFilter,)
    actions = [
        'send_accept_reject_mail',
        'final_confirm_event',
        'print_events',
        'print_events_public',
        'remove_from_schedule',
    ]
    inlines = [
        EventMediaInline,
        EventLinkInline
    ]

    form = EventAdminForm
    
    filter_horizontal = ['speaker']

    search_fields = (
        'title',
        'description',
        'notes',
        'internal_notes',
        'speaker__username',
        'speaker__email',
        
    )

    fieldsets = (
        (None, {
            'fields': ('conference', 'event_type', 'status',)
        }),
        ('Title, Description and Speakers', {
            'fields': ('title', 'subtitle', 'description', 'speaker')
        }),
        ('Time and Date', {
            'fields': ('length', 'day', 'time', 'room', 'track')
        }),
        ('Language(s)', {
            'fields': ('languages', 'language')
        }),
        ('Notes, Tags, Assets', {
            'fields': ('notes', 'internal_notes', 'selected_tags', 'assets'),
        }),
        ('Presentation', {
            'fields': (
                'hide_speakers',
                'teaser_image',
                'video',
                'schedule_grid_column_span',
                'schedule_grid_css_class',
                'schedule_grid_description_html',)
        })
    )

    save_as = True
    
    save_on_top = True
    
    def get_queryset(self, request):
        return super().get_queryset(request).filter(version=Event.DRAFT)

    def show_speakers(self, event):
        return mark_safe(", ".join([
            """<a href="%s">%s</a>""" % (reverse('admin:core_user_change', args=[x.id]), x)
            for x in event.speaker.all()
        ]))

    show_speakers.short_description = "Speaker(s)"
    
    def show_languages(self, event):
        langs = (
            (str(x) if x != event.language else "<strong>%s</strong>" % x)
            for x in event.languages.all())
        
        return mark_safe(", ".join(langs))

    show_languages.short_description = "Language(s)"

    def status_colorizer(self, event):
        o = '0.2'
        colors = {
            EventStatus.NEW: f"rgba(140, 137, 230, {o})",
            EventStatus.WITHDRAWN: f"rgba(252, 136, 150, {o})",
            EventStatus.UNCONFIRMED: f"rgba(255, 232, 138, {o})",
            EventStatus.CONFIRMED: f"rgba(154, 242, 131, {o})",
            EventStatus.CANCELED: f"rgba(153, 153, 153, {o})",
            EventStatus.REJECTED: f"rgba(153, 153, 153, {o})"
        }
        return mark_safe(
            """<script>django.jQuery(document.currentScript.parentElement).parent().find('td, th').css('background-color', '%s');</script>"""
            % (colors[event.status]))

    status_colorizer.short_description = ""

    def show_datetime(self, event):
        if event.has_datetime:
            return mark_safe(
                "%s &ndash; %s" %
                (defaultfilters.date(event.start_datetime, "Y-m-d H:i"),
                 defaultfilters.date(event.end_datetime, "H:i")))
        elif event.has_date:
            return defaultfilters.date(event.day.date, "Y-m-d")
        elif not event.is_scheduled:
            if event.status in (EventStatus.UNCONFIRMED, EventStatus.CONFIRMED):
                return mark_safe("""<span style="color: red;">Not scheduled</span>""")
            else:
                return mark_safe("""Not scheduled""")
        else:
            return "-"

    show_datetime.short_description = "Date/Time"
    show_datetime.admin_order_field = "start_datetime"

    @mailpreview
    def send_accept_reject_mail(self, request, queryset, dry_run):
        for event in queryset:
            if event.status == EventStatus.UNCONFIRMED:
                yield from notify_authors(request, event, 'callforcontributions.accept', True, dry_run=dry_run)
            elif event.status == EventStatus.REJECTED:
                yield from notify_authors(request, event, 'callforcontributions.reject', True, dry_run=dry_run)

    @mailpreview
    def final_confirm_event(self, request, queryset, dry_run):
        for event in queryset:
            yield from notify_authors(
                request, event, 'callforcontributions.final-confirmation', allow_accept=False, dry_run=dry_run)

    send_accept_reject_mail.short_description = "Send acceptance/rejection mails to speakers"
    final_confirm_event.short_description = "Send final confirmation mail to speakers"

    def print_events(self, request, queryset):
        speakers = User.objects.filter(event__id__in=[x.id for x in queryset.all()]).distinct()
        return render(request, 'callforcontributions/admin/print.html', {
            'events': queryset,
            'speakers': speakers,
            'opts': self.model._meta,
        })

    def print_events_public(self, request, queryset):
        return render(request, 'callforcontributions/admin/print_public.html', {
            'events': queryset.order_by('start_datetime', 'room'),
            'opts': self.model._meta,
        })

    print_events_public.short_description = "Print contributions (public info)"
    print_events.short_description = "Print contributions (for organizers)"

    def get_urls(self):
        urls = super(EventAdmin, self).get_urls()
        my_urls = [
            url(
                r'(?P<event_id>\d+)/event_media/(?P<filename>.+)$',
                self.admin_site.admin_view(self.download_event_media),
                name="download_event_media")]
        return my_urls + urls

    def download_event_media(self, request, event_id, filename):
        media = get_object_or_404(
            EventMedia,
            file='event_media/%s' % filename)
        response = HttpResponse(media.file, content_type='application/octet-stream')
        response['Content-Disposition'] = 'attachment; filename=%s' % media.filename()
        return response

    def remove_from_schedule(self, request, queryset):
        for event in queryset:
            event.room = event.time = event.day = None
            event.save()

        
class MatrixAdminExtension:
    def __init__(self, parent):
        self.parent = parent

    def links(self, conference):
        links = (
            ('contributions', 'tag/asset matrix', reverse('admin:conference_matrix_view', args=[conference.id])),
        )
        return links
        
    def urls(self):
        return (
            url(
                r'(.*)/matrix$',
                self.parent.admin_site.admin_view(self.matrix_view), name="conference_matrix_view"),
        )

    def actions(self):
        return []
        
    def matrix_view(self, request, conference_id):
        conferenceeventsettings = get_object_or_404(
            ConferenceEventSettings,
            conference=conference_id)
        events = Event.objects.filter(
            conference=conference_id,
            version=Event.DRAFT).order_by('event_type', 'start_datetime').all()
        tags = set()
        assets = set()

        for e in events:
            tags |= set(e.selected_tags.all())
            assets |= set(e.assets.all())

        return render(request, 'callforcontributions/admin/matrix.html', {
            'conferenceeventsettings': conferenceeventsettings,
            'tags': tags,
            'assets': assets,
            'events': events,
            'colcount': len(assets) + len(tags) + 1,
        })


class PrintScheduleAdminExtension:
    def __init__(self, parent):
        self.parent = parent

    def links(self, conference):
        links = (
            ('print', 'schedule', reverse('admin:conference_print_schedule_view', args=[conference.id])),
        )
        return links
        
    def urls(self):
        return (
            url(
                r'(.*)/print_schedule$',
                self.parent.admin_site.admin_view(self.print_schedule_view), name="conference_print_schedule_view"),
        )

    def actions(self):
        return []
        
    def print_schedule_view(self, request, conference_id):
        conference = get_object_or_404(Conference, id=conference_id)
        conferenceeventsettings = conference.eventsettings
        if conferenceeventsettings.last_draft_released == None:
            v = Event.DRAFT
        else:
            v = Event.RELEASED
            
        scheduleviewset = []
        dayno = 0
        for d in conference.conferenceday_set.exclude(public_start=None).exclude(public_end=None).order_by('date').all():

            events = conference.event_set.filter(
                day_id=d.id,
                conference=conference,
                status__in=[EventStatus.CONFIRMED, EventStatus.UNCONFIRMED],
                version=v
            ).all()

            rooms = Room.objects.filter(event__in=events.filter(scheduled=Event.SS_DTR)).distinct().order_by('sort_key', 'name')
            
            active_layer = ScheduleLayer(events)
            active_layer.template = 'callforcontributions/schedule-dump/schedule-event.html'
            scheduleset = ConferenceScheduleView(
                conference,
                conference.conferenceday_set.filter(id=d.id),
                rooms,
                active_layer,
                public_times=True,
            )
            scheduleviewset.append(scheduleset)

            dayno += 1

        with override(settings.BADGE_LANGUAGE):
            return render(request, 'callforcontributions/admin/print-schedule.html', {
                'conferenceeventsettings': conferenceeventsettings,
                'scheduleviewset': scheduleviewset,
                'generationtime': datetime.now(),
            })


class PrintBookletAdminExtension:
    def __init__(self, parent):
        self.parent = parent

    def links(self, conference):
        links = (
            ('print', 'booklet', reverse('admin:conference_print_booklet_view', args=[conference.id])),
        )
        return links
        
    def urls(self):
        return (
            url(
                r'(.*)/print_booklet$',
                self.parent.admin_site.admin_view(self.print_booklet_view), name="conference_print_booklet_view"),
        )

    def actions(self):
        return []
        
    def print_booklet_view(self, request, conference_id):
        conference = get_object_or_404(Conference, id=conference_id)
        conferenceeventsettings = conference.eventsettings
        v = Event.RELEASED

        all_events = conference.event_set.filter(
            status__in=[EventStatus.CONFIRMED, EventStatus.UNCONFIRMED],
            version=v
        ).exclude(event_type__booklet_headline=None
        ).exclude(event_type__booklet_headline=""
        ).order_by('event_type__sort_key', 'start_datetime', 'room__sort_key', 'room__name').all()
            
        scheduleviewset = []
        dayno = 0
        for d in conference.conferenceday_set.exclude(public_start=None).exclude(public_end=None).order_by('date').all():

            events = conference.event_set.filter(
                day_id=d.id,
                conference=conference,
                status__in=[EventStatus.CONFIRMED, EventStatus.UNCONFIRMED],
                version=v
            ).all()

            rooms = Room.objects.filter(event__in=events.filter(scheduled=Event.SS_DTR)).distinct().order_by('sort_key', 'name')
            
            active_layer = ScheduleLayer(events)
            active_layer.template = 'callforcontributions/schedule-dump/schedule-event.html'
            scheduleset = ConferenceScheduleView(
                conference,
                conference.conferenceday_set.filter(id=d.id),
                rooms,
                active_layer,
                public_times=True,
            )
            scheduleviewset.append(scheduleset)

            dayno += 1

        with override(settings.BADGE_LANGUAGE):
            return render(request, 'callforcontributions/admin/print-booklet.html', {
                'conferenceeventsettings': conferenceeventsettings,
                'scheduleviewset': scheduleviewset,
                'generationtime': datetime.now(),
                'all_events': all_events,
            })        
    
    
class ConferenceEventSettingsInline(TranslationStackedInline):
    model = ConferenceEventSettings
    extra = 1
    formfield_overrides = {
        models.TextField: {'widget': AdminPagedownWidget},
    }


