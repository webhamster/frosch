from django.core.management.base import BaseCommand
from django.conf import settings
from django.utils.translation import activate
from callforcontributions.models import Event
from django.utils.translation import ugettext as _
from urllib.parse import urljoin
import re
import httplib2
import os
import sys
import copy

import google.oauth2.credentials

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from google_auth_oauthlib.flow import InstalledAppFlow


class Command(BaseCommand):
    help = 'Use the YouTube API to update the video descriptions of linked videos in the contributions. Requires a youtube API client secrets file in the root folder of the django project. See https://cloud.google.com/genomics/downloading-credentials-for-api-access'

    CLIENT_SECRETS_FILE = 'youtube_client_secrets.json'
    SCOPES = ['https://www.googleapis.com/auth/youtube.force-ssl']
    API_SERVICE_NAME = 'youtube'
    API_VERSION = 'v3'

    failed = {}
    not_changed = []
    not_authorized = []

    def __init__(self, *args, **kwargs):
        super().__init__(self, *args, **kwargs)
        self.client_secrets_file_path = os.path.abspath(self.CLIENT_SECRETS_FILE)
        self.youtube = self.get_authenticated_service()
        self.get_channels()

    def get_authenticated_service(self):
        flow = InstalledAppFlow.from_client_secrets_file(self.client_secrets_file_path, self.SCOPES)
        credentials = flow.run_console()
        return build(self.API_SERVICE_NAME, self.API_VERSION, credentials=credentials)

    
    def get_channels(self):
        channels = self.youtube.channels().list(
            mine=True,
            part='id',
        ).execute()
        self.channels = [item['id'] for item in channels['items']]
        
    def handle(self, *args, **kwargs):

        events = Event.objects.filter(version=Event.RELEASED).filter(video__contains='youtube').all()
        print (f"Found {len(events)} events with potential youtube videos.")
        for event in events:
            print (f" - Updating event {event.id} '{event.title}'")
            self.try_update_description(event)

        print ("FINISHED!")
        print (f"Updated: {len(events)-len(self.not_changed)-len(self.failed)-len(self.not_authorized)}")
        print (f"Not owned by authorized user: {len(self.not_authorized)}")
        print (f"Not changed: {len(self.not_changed)}")
        print (f"Failed: {len(self.failed)}")

    def try_update_description(self, event):
        videolink = event.video
        match = re.match(r"""^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$""", videolink)
        
        if match == None:
            print (f"   Video {videolink} in event does not look like a valid youtube URL")
            return
        
        video_id = match.group(5)
        print (f"   Found video id {video_id}, contacting API")

        # Call the API's videos.list method to retrieve the video resource.
        videos_list_response = self.youtube.videos().list(
            id=video_id,
            part='snippet,status'
        ).execute()
        
        # If the response does not contain an array of "items" then the video was
        # not found.
        if not videos_list_response["items"]:
            print ("   Video '%s' was not found." % options.video_id)
            return
            
        # Since the request specified a video ID, the response only contains one
        # video resource. This code extracts the snippet from that resource.

        snippet = videos_list_response["items"][0]["snippet"]
        status = videos_list_response["items"][0]["status"]

        if snippet['channelId'] not in self.channels:
            self.not_authorized.append(event)
            print("   Video not owned by authorized user.")
            return

        if event.language is not None:
            lang = event.language.code
        else:
            lang = settings.LANGUAGES[0][0]
        activate(lang)
        
        title = f"{event.title} – {event.conference.long_name}"
        if len(title) > 100:
            title = f"{event.title} – {event.conference.short_name}"
        if len(title) > 100:
            title = f"{event.title}"[:100]
        speakers = ", ".join(s.username for s in event.speaker.all())
        if event.hide_speakers or not speakers:
            line_contrib = ""
        else:
            line_contrib = _("{event_type} by {speakers} at {conf_name}\n").format(event_type=event.event_type, speakers=speakers, conf_name=event.conference.long_name)
        line_pamlink = _("More information about Pi and More at https://piandmore.de")
        line_eventlink = _("More details at {link}").format(link=urljoin(settings.FROSCH_BASE_URL, event.get_absolute_url()))
        line_subtitle = f"{event.title}\n{event.subtitle}\n" if event.subtitle else ""
        description = f"""{line_subtitle}{line_eventlink}
{line_contrib}---
{event.description[:4000]}
---
{line_pamlink}"""

        # < and > are not allowed in youtube video descriptions; replacing them by the closest equivalents...
        description = description.replace('>', '≥').replace('<', '≤')

        snippet_new = copy.copy(snippet)
        status_new = copy.copy(status)
        
        snippet_new['title'] = title
        snippet_new['description'] = description

        status_new['privacyStatus'] = 'public'
        
        if snippet_new == snippet and status == status_new:
            print ("   No changes needed, continuing")
            self.not_changed.append(event.id)
            return

        print ("   Updating snippet: ")
        
        # Update the video resource by calling the videos.update() method.
        try:
            videos_update_response = self.youtube.videos().update(
                part='snippet,status',
                body=dict(
                    snippet=snippet_new,
                    status=status_new,
                    id=video_id
                )).execute()
        except Exception as e:
            print ("   Failed updating video: " + str(e))
            print ("Data:" + repr(snippet_new))
            self.failed[event.id] = e
            

        
        
