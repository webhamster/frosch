from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _, ugettext as __
from django.conf import settings
from django.utils.safestring import mark_safe
from django.urls import reverse
from core.models import Scheduleable, VersionableContainer
from django.core.files.base import ContentFile
import random
import string
import os
import re
import logging

logger = logging.getLogger(__name__)



from django.utils.functional import lazy
mark_safe_lazy = lazy(mark_safe, str)


class EventStatus():
    NEW, WITHDRAWN, UNCONFIRMED, CONFIRMED, CANCELED, REJECTED = range(6)
    descriptions = ((NEW, __("New")),
                    (WITHDRAWN, __("Withdrawn")),
                    (UNCONFIRMED, __("Accepted/Unconfirmed")),
                    (CONFIRMED, __("Accepted/Confirmed")),
                    (CANCELED, __("Canceled")),
                    (REJECTED, __("Rejected")))
    css = ((NEW, 'new'),
           (WITHDRAWN, 'withdrawn'),
           (UNCONFIRMED, 'unconfirmed'),
           (CONFIRMED, 'confirmed'),
           (CANCELED, 'canceled'),
           (REJECTED, 'rejected'))

    
class EventType(models.Model):
    name = models.CharField(max_length=30)
    short_description = models.TextField()
    long_description = models.TextField()
    url_snippet = models.SlugField(max_length=20, unique=True)
    allowed_lengths = models.ManyToManyField('EventLength', blank=True)
    allowed_tags = models.ManyToManyField('EventTag', blank=True)
    allowed_assets = models.ManyToManyField('EventAsset', blank=True)
    css_class = models.SlugField(default='default', max_length=20)
    booklet_headline = models.CharField(max_length=100, blank=True, null=True)
    sort_key = models.FloatField(default=0)
    hide_in_lists = models.BooleanField(verbose_name=_("Hide events of this type in event lists."), default=False)

    def is_schedulable(self):
        return self.allowed_lengths.count() > 0

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Contribution Type"

        
class EventTag(models.Model):
    name = models.CharField(max_length=40)
    description = models.TextField()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Tag"

        
class EventLength(models.Model):
    name = models.CharField(max_length=50)
    length = models.IntegerField()
    
    def __str__(self):
        return "%s (%d min)" % (self.name, self.length)

    def __int__(self):
        return self.length

    def get_hours_and_minutes(self):
        return "%02d:%02d" % divmod(self.length, 60)

    class Meta:
        verbose_name = "Length"

        
class EventLanguage(models.Model):
    name = models.CharField(max_length=20)
    code = models.CharField(max_length=4, default='en')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Language"

        
class EventAsset(models.Model):
    name = models.CharField(max_length=40)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Asset"

        
class EventTrack(models.Model):
    name = models.CharField(max_length=40)
    css_class = models.SlugField(max_length=30, default='')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Track"

        
def create_associate_token():
    return ''.join(
        random.choice(string.ascii_letters + string.digits) for _ in range(10))


description_help_text = mark_safe_lazy(_("""
Describe your contribution. Make sure to answer at least the following questions:
<ul class="help-block">
<li>What is the topic of your presentation?
<li>How do you plan to communicate it? (E.g., will it be more theory or more practice?)
<li>What is your target audience? Beginner or advanced users? Professionals, makers, teachers?
</ul>
<p class="help-block">This description will be used in the official schedule
if your contribution is accepted. You can make changes to this description
until the official program is published. Any non-public information or notes
for the organizer team can be added below.</p>
"""))


class Event(Scheduleable):
        
    class Meta:
        ordering = ['conference', 'created']
        verbose_name = "contribution"

    COPY_MANYTOONE = ['link_set', 'media_set']
        
    schedule_select_related = ('event_type', 'room', 'day', 'length', 'language', )
    schedule_prefetch_related = ('speaker', 'languages', 'released', 'released__room')

    event_type = models.ForeignKey(
        EventType,
        verbose_name=_("Type of Submission"),
        on_delete=models.CASCADE)
    track = models.ForeignKey(
        EventTrack,
        blank=True,
        null=True,
        on_delete=models.CASCADE)
    speaker = models.ManyToManyField(
        'core.User',
        blank=True)
    hide_speakers = models.BooleanField(default=False, verbose_name=_("Hide Speakers in Public Schedule"))
    title = models.CharField(_("Title"), max_length=100, help_text=_("The title of your contribution. Public."))
    subtitle = models.CharField(
        _("Subtitle"),
        max_length=100,
        blank=True,
        help_text=_("An optional subtitle of your contribution. Public."))
    description = models.TextField(_("Description"), help_text=description_help_text)
    teaser_image = models.FileField(
        upload_to="event_media",
        verbose_name=_("Teaser Image (PNG)"),
        help_text=_("PNG Image used in public schedule."),
        blank=True,
        null=True)
    schedule_grid_column_span = models.PositiveIntegerField(
        _("Column span in schedule grid"),
        default=1,
        help_text=_("Small hack to span this event over several columns (rooms) in the schedule grid."))
    schedule_grid_css_class = models.CharField(
        _("CSS class in schedule grid"),
        max_length=100,
        blank=True,
        help_text=_("CSS class of this event in schedule grid."))
    schedule_grid_description_html = models.TextField(
        _("Replacement for description in schedule grid (HTML)"),
        blank=True,
        help_text=_("This HTML code replaces the entire content of the event in the schedule grid. "
                    "To produce a normal title, use <div class=\"title\">Title goes here</div>."))
    notes = models.TextField(
        _("Notes"),
        blank=True,
        help_text=_("These notes will not be published. "
                    "Include any information here that is needed to evaluate your submission."))
    internal_notes = models.TextField(_("Internal Notes"), blank=True)
    length = models.ForeignKey(
        EventLength,
        blank=True,
        null=True,
        on_delete=models.CASCADE)
    languages = models.ManyToManyField(EventLanguage)
    language = models.ForeignKey(
        EventLanguage,
        blank=True,
        null=True,
        verbose_name=_("From the list of languages offered by the submitter, "
                       "this one is preferred by the conference organizers."),
        related_name="selected_language",
        on_delete=models.CASCADE)
    status = models.SmallIntegerField(choices=EventStatus.descriptions, default=EventStatus.NEW)
    selected_tags = models.ManyToManyField(EventTag, blank=True)
    assets = models.ManyToManyField(EventAsset, blank=True)
    associate_token = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        default=create_associate_token,
        editable=False)
    video = models.URLField(blank=True, null=True)

    created = models.DateTimeField()
    changed = models.DateTimeField()

    @property
    def is_accepted(self):
        return self.status in [EventStatus.UNCONFIRMED, EventStatus.CONFIRMED, EventStatus.CANCELED]
    
    @property
    def to_be_published(self):
        return self.is_scheduled and self.is_accepted

    @property
    def public_links(self):
        return self.link_set.filter(is_public=True)

    @property
    def public_media(self):
        return self.media_set.filter(is_public=True)
        
    
    def get_persons_required(self):
        return self.speaker

    get_persons_required.prefetch = 'speaker'

    def get_room_required(self):
        return self.room

    def get_type(self):
        return self.event_type

    def get_css_classes(self):
        return "event-type-{type} event-status-{status} event-track-{track} ".format(
            type=self.event_type.css_class,
            status=EventStatus.css[self.status][1],
            track=self.track.css_class if self.track else 'none') + self.schedule_grid_css_class

    def get_status(self):
        return EventStatus.descriptions[self.status][1]

    def confirm(self):
        self.status = EventStatus.CONFIRMED
        self.save()
        
    def save(self, *args, **kwargs):
        if self.id and self.languages.count() == 1:
            self.language = self.languages.all()[0]
        if not self.id:
            self.created = timezone.now()
        self.changed = timezone.now()
        super().save(*args, **kwargs)

    def get_teaser_image_as_base64(self):
        if not self.teaser_image:
            return ""
        with open(self.teaser_image.name, 'r') as f:
            return f.read().encode('base64')

    def withdraw_or_cancel(self):
        if self.to_be_published and self.released is not None:
            self.status = EventStatus.CANCELED
        else:
            self.status = EventStatus.WITHDRAWN
        self.save()

    def get_absolute_url(self):
        return reverse('core_conference:conference_public_event', kwargs={'conference_id': self.conference.url_snippet, 'event_id': self.draft_id})

    def __str__(self):
        return "%s" % (self.title)

    
def filter_filename(instance, filename):
    filename = re.sub(r"[^a-zA-Z0-9-_.]", '', filename)
    path = os.path.join(settings.MEDIA_ROOT, 'event_media', filename)
    counter = 0
    while os.path.exists(path):
        filenames = filename.split('.')
        filenames[-1] = "%d." % counter + filenames[-1]
        filename_new = '.'.join(filenames)
        path = os.path.join(settings.MEDIA_ROOT, 'event_media', filename_new)
        counter += 1
        if counter > 1000:
            break
    else:
        return path
    raise Exception("Could not find a filename that is free for this file: %s" % path)
    
    
class EventMedia(models.Model):
    file = models.FileField(upload_to=filter_filename)
    name = models.CharField(_("Description"), max_length=40, null=True, blank=True)
    is_public = models.BooleanField(_("Public"), default=True)
    event = models.ForeignKey(Event, related_name='media_set', null=True, blank=True, on_delete=models.CASCADE)

    def filename(self):
        return os.path.basename(self.file.name)

    def __str__(self):
        return "'%s'" % self.name

    def get_copy(self):
        if not self.file.storage.exists(self.file.name):
            # if the actual media file does not exist on the host (for whatever reason), then we skip the copy operation
            logger.warning(f"EventMedia file '{self.file.name}' does not exist in storage!")
            return None
        copy = self
        copy.pk = None
        copy.id = None
        filename = str(self.file.name)
        #print ("Filename is" + filename)
        copy.file = ContentFile(self.file.read())
        copy.file.name = filename
        copy.save()
        return copy
    
    class Meta:
        verbose_name = "Media File"

        
class EventLink(models.Model):
    link = models.CharField(max_length=400)
    name = models.CharField(_("Description"), max_length=40)
    is_public = models.BooleanField(_("Public"), default=True)
    event = models.ForeignKey(Event, related_name='link_set', on_delete=models.CASCADE)

    def get_copy(self):
        copy = self
        copy.pk = None
        copy.id = None
        copy.save()
        return copy

    class Meta:
        verbose_name = "Link"

        
class ConferenceEventSettings(VersionableContainer):
    conference = models.OneToOneField(
        'core.Conference',
        primary_key=True,
        related_name="eventsettings",
        on_delete=models.CASCADE)
    description = models.TextField()
    event_types = models.ManyToManyField(EventType, blank=True)
    tracks = models.ManyToManyField(EventTrack, blank=True)
    languages = models.ManyToManyField(EventLanguage, blank=True)
    call_open = models.DateTimeField()
    call_close = models.DateTimeField()
    notification_date = models.DateField(
        help_text=_("Latest reject/accept notification date, leave blank to hide it."),
        blank=True,
        null=True)
    link_title = models.CharField(
        max_length=40,
        help_text=_("Used for navigation links. Leave empty to hide this Call for Contributions."),
        blank=True)
    is_archived = models.BooleanField("Archive mode (deny changes by users)", default=False)
    is_public = models.BooleanField(_("Public"), default=True)
    versionable_model = Event

    def is_call_open(self):
        return self.call_open < timezone.now() < self.call_close

    def get_absolute_url(self):
        return reverse('call:index', kwargs={'conference_id': self.conference.url_snippet})

    def __str__(self):
        return "Settings for %s" % self.conference

    class Meta:
        verbose_name = "Call for Contribution Setting"

# things to warn for: accepted but not scheduled
