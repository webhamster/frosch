from django.conf.urls import url

from callforcontributions import views
from active_login_required import active_login_required

app_name = 'callforcontributions'

sitemaps = {}

urlpatterns = [
    url(r'^myevents/$', active_login_required(views.MyEventList.as_view()), name='myeventlist'),
]
