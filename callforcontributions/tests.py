from core.tests import CoreTestSetup
from datetime import timedelta, time
from django.contrib.admin import ACTION_CHECKBOX_NAME
from django.test.client import RequestFactory, Client

from callforcontributions.models import ConferenceEventSettings
from core.models import Scheduleable
from django.core import mail
from django.utils import timezone
from .models import Event, EventLanguage, EventLength, EventType
from .utils import ScheduleDumper


rf = RequestFactory()
get_request = rf.get('/')


class CallforcontributionsTestSetup(CoreTestSetup):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.event_lengths = [EventLength.objects.create(
            name="short",
            length=60,
        ), EventLength.objects.create(
            name="long",
            length=120
        )]
        cls.event_length_short, cls.event_length_long = cls.event_lengths
        cls.event_types = [EventType.objects.create(
            name=n,
            short_description=n,
            long_description=n,
            url_snippet=n,
        ) for n in ('Talk', 'Workshop')]
        for e in cls.event_types:
            e.allowed_lengths = cls.event_lengths
            e.save()
        cls.event_type_talk = cls.event_types[0]
        cls.event_type_workshop = cls.event_types[1]
        
        cls.event_languages = [EventLanguage.objects.create(
            name="English"
        )]
        cls.conference_event_settings = ConferenceEventSettings.objects.create(
            conference=cls.conference,
            event_types=cls.event_types,
            call_open=timezone.now(),
            call_close=timezone.now() + timedelta(days=2),
        )
        cls.events = (
            cls.event_short_talk_unscheduled,
            cls.event_long_talk_scheduled,
            cls.event_long_workshop_scheduled
        ) = [
            Event.objects.create(
                conference=cls.conference,
                event_type=cls.event_type_talk,
                title="Test 1",
                description="Test 1",
                length=cls.event_length_short,
            ),
            Event.objects.create(
                conference=cls.conference,
                event_type=cls.event_type_talk,
                title="Test 2",
                description="Test 2",
                length=cls.event_length_long,
                day=cls.cds[1],
                time=time(10, 0, 0),
                room=cls.rooms[0],
            ),
            Event.objects.create(
                conference=cls.conference,
                event_type=cls.event_type_workshop,
                title="Test 3",
                description="Test 3",
                length=cls.event_length_long,
                day=cls.cds[1],
                time=time(12, 0, 0),
                room=cls.rooms[1],
            )
        ]
        for e in cls.events:
            e.speaker = cls.users
            e.languages = cls.event_languages
            e.save()


class CallforcontributionsTest(CallforcontributionsTestSetup):
    client = None

    def iterate_through_event_changes(self):
        # Initial release of schedule - all events are "new"
        yield

        # "Empty" release - nothing should change.
        for e in self.events:
            e.save()
        yield

        # One event will be changed, one will be removed from schedule
        self.events[1].time = time(11, 0, 0)
        self.events[2].time = None
        for e in self.events:
            e.save()
        yield

        # Only one event will change.
        self.events[2].time = time(1, 0, 0)
        for e in self.events:
            e.save()
        yield

    def test_release_events_mailer(self):
        states = self.iterate_through_event_changes()
        
        next(states)
        mail.outbox = []
        self.release_events_through_admin()
        print("\n\n".join("Subject: {0.subject}\nBody: {0.body}\n".format(x) for x in mail.outbox))
        self.assertEqual(len(mail.outbox), 8)
        for e in self.events:
            e.refresh_from_db()
        # reloaded events from db, now check if released data matches draft data
        self.assertEqual(self.events[2].start_datetime, self.events[2].released.start_datetime)
        self.assertEqual(self.events[1].room, self.events[1].released.room)

        next(states)
        mail.outbox = []
        self.release_events_through_admin()
        self.assertEqual(len(mail.outbox), 0, msg=self.show_mails())  # no changes expected

        next(states)
        mail.outbox = []
        self.release_events_through_admin()
        self.assertEqual(len(mail.outbox), 8, msg=self.show_mails())

        next(states)
        mail.outbox = []
        self.release_events_through_admin()
        self.assertEqual(len(mail.outbox), 4, msg=self.show_mails())
        mail.outbox = []

    def release_events(self):
        events = Event.objects.filter(conference=self.conference)
        self.assertEqual(events.filter(version=Scheduleable.DRAFT).count(), 3)
        return Event.make_draft_released(events)

    def test_release_events_detail(self):
        states = self.iterate_through_event_changes()

        next(states)
        cd = self.release_events()
        expected = {
            self.events[0]: Event.CHANGED_NONE,
            self.events[1]: Event.CHANGED_NEW,
            self.events[2]: Event.CHANGED_NEW,
        }
        self.cmp_changes(expected, cd)

        next(states)
        cd = dict(self.release_events())
        expected = {
            self.events[0]: Event.CHANGED_NONE,
            self.events[1]: Event.CHANGED_NONE,
            self.events[2]: Event.CHANGED_NONE,
        }
        self.cmp_changes(expected, cd)
        
        next(states)
        cd = dict(self.release_events())
        expected = {
            self.events[0]: Event.CHANGED_NONE,
            self.events[1]: Event.CHANGED_MOVED,
            self.events[2]: Event.CHANGED_REMOVED,
        }
        self.cmp_changes(expected, cd)
        
        next(states)
        cd = dict(self.release_events())
        expected = {
            self.events[0]: Event.CHANGED_NONE,
            self.events[1]: Event.CHANGED_NONE,
            self.events[2]: Event.CHANGED_NEW,
        }
        self.cmp_changes(expected, cd)

    def cmp_changes(self, expected, changes):
        exp = {e.id: change for e, change in expected.items()}
        changes = {e: change for (e, change) in changes.items()}
        self.assertEquals(exp, changes)

    def release_events_through_admin(self):
        self.client = Client()
        assert(self.client.login(email=self.admin_user.email, password='admin'))

        # print ("Releasing events")
        # res = self.client.get('/admin/callforcontributions/conferenceeventsettings/', follow=True)
        data = {
            'action': 'make_draft_released',
            ACTION_CHECKBOX_NAME: self.conference_event_settings.pk,
        }
        
        res = self.client.post('/admin/core/conference/', data, follow=True)
        self.assertEquals(res.status_code, 200)
        
    def show_mails(self):
        return "Mails:\n" + "\n\n".join("Subject: %s\n%s" % (m.subject, m.body) for m in mail.outbox)
    
    def test_dump_schedule(self):
        self.release_events()
        s = ScheduleDumper(self.conference)
        s.dump()

    def test_conflicts(self):
        e1 = Event.objects.create(
            conference=self.conference,
            event_type=self.event_type_talk,
            title="x",
            description='y',
            length=self.event_length_short,
        )
        e1.set_start_datetime(self.event_long_talk_scheduled.start_datetime + timedelta(seconds=60))
        e1.speaker = [self.alice]
        e1.room = self.event_long_talk_scheduled.room
        e1.save()
        conflicts = self.event_long_talk_scheduled.get_conflicts()
        self.assertEqual(conflicts.room, {e1.room})
        self.assertEqual(conflicts.user_duties, {e1})
        self.assertEqual(conflicts.user_availability, {self.alice})
        
