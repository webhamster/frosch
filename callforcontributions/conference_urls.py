from django.conf.urls import url
from callforcontributions import views
from .sitemaps import CallConferenceIndexViewSitemap

app_name = 'callforcontributions'

sitemaps = {
    'call_conference_index': CallConferenceIndexViewSitemap(),
}

urlpatterns = [
    url(r'^$', views.Index.as_view(), name='index'),
    url(r'^submit/(?P<event_type_id>[^/]+)/$', views.submit, name='submit'),
    url(r'^edit/(?P<event_id>\d+)/$', views.edit, name='edit'),
    url(r'^confirm/(?P<event_id>\d+)/$', views.confirm, name='confirm'),
    url(r'^withdraw/(?P<event_id>\d+)/$', views.withdraw, name='withdraw'),
    url(r'^undo-withdraw/(?P<event_id>\d+)/$', views.undo_withdraw, name='undo-withdraw'),
    url(r'^delete/(?P<event_id>\d+)/$', views.delete, name='delete'),
    url(r'^associate/(?P<event_id>\d+)/(?P<token>[a-zA-Z0-9]+)/$', views.associate, name='associate'),
    url(r'^download-media/(?P<media_id>.+)$', views.download_event_media, name='download-media'),
    url(r'^public-media/(?P<media_id>.+)$', views.download_event_media_public, name='public-media'),
]
