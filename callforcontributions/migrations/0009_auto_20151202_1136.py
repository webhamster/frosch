# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import callforcontributions.models


class Migration(migrations.Migration):

    dependencies = [
        ('callforcontributions', '0008_event_teaser_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventmedia',
            name='file',
            field=models.FileField(upload_to=callforcontributions.models.filter_filename),
        ),
    ]
