# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import callforcontributions.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConferenceEventSettings',
            fields=[
                ('conference', models.OneToOneField(related_name='eventsettings', primary_key=True, serialize=False, to='core.Conference')),
                ('call_open', models.DateTimeField()),
                ('call_close', models.DateTimeField()),
                ('notification_date', models.DateField(help_text='Latest reject/accept notification date, leave blank to hide it.', null=True, blank=True)),
                ('schedule_grid', models.IntegerField(default=5, help_text='Grid granularity in the schedule view in minutes.')),
                ('schedule_grid_labels', models.IntegerField(default=30, help_text='Labels every x minutes in schedule grid.')),
                ('link_title', models.CharField(help_text='Used for navigation links. Leave empty to hide this Call for Contributions.', max_length=40, blank=True)),
                ('link_title_de', models.CharField(help_text='Used for navigation links. Leave empty to hide this Call for Contributions.', max_length=40, null=True, blank=True)),
                ('link_title_en', models.CharField(help_text='Used for navigation links. Leave empty to hide this Call for Contributions.', max_length=40, null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Call for Contribution Setting',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text='The title of your contribution. Public.', max_length=100, verbose_name='Title')),
                ('subtitle', models.CharField(help_text='An optional subtitle of your contribution. Public.', max_length=100, verbose_name='Subtitle', blank=True)),
                ('description', models.TextField(help_text='\nDescribe your contribution. Make sure to answer at least the following questions:\n<ul class="help-block">\n<li>What is the topic of your presentation?\n<li>How do you plan to communicate it? (E.g., will it be more theory or more practice?)\n<li>What is your target audience? Beginner or advanced users? Professionals, makers, teachers?\n</ul>\n<p class="help-block">Use Markdown syntax to format your inputs. This description will be used in the official schedule if your contribution is accepted. Any non-public information or notes for the organizer team can be added below.</p>\n', verbose_name='Description')),
                ('notes', models.TextField(help_text='These notes will not be published. Include any information here that is needed to evaluate your submission.', verbose_name='Notes', blank=True)),
                ('internal_notes', models.TextField(verbose_name='Internal Notes', blank=True)),
                ('status', models.SmallIntegerField(default=0, choices=[(0, 'New'), (1, 'Withdrawn'), (2, 'Not confirmed yet'), (3, 'Confirmed'), (4, 'Canceled'), (5, 'Rejected')])),
                ('time', models.TimeField(null=True, blank=True)),
                ('start_datetime', models.DateTimeField(null=True, editable=False, blank=True)),
                ('end_datetime', models.DateTimeField(null=True, editable=False, blank=True)),
                ('associate_token', models.CharField(default=callforcontributions.models.create_associate_token, max_length=10, null=True, blank=True)),
                ('created', models.DateTimeField()),
                ('changed', models.DateTimeField()),
            ],
            options={
                'ordering': ['conference', 'created'],
                'verbose_name': 'Contribution',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EventAsset',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('name_de', models.CharField(max_length=40, null=True)),
                ('name_en', models.CharField(max_length=40, null=True)),
            ],
            options={
                'verbose_name': 'Asset',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EventLanguage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20)),
                ('name_de', models.CharField(max_length=20, null=True)),
                ('name_en', models.CharField(max_length=20, null=True)),
            ],
            options={
                'verbose_name': 'Language',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EventLength',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('name_de', models.CharField(max_length=50, null=True)),
                ('name_en', models.CharField(max_length=50, null=True)),
                ('length', models.IntegerField()),
            ],
            options={
                'verbose_name': 'Length',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EventLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('link', models.URLField()),
                ('name', models.CharField(max_length=40, verbose_name='Description')),
                ('is_public', models.BooleanField(default=True, verbose_name='Public')),
                ('event', models.ForeignKey(related_name='link_set', to='callforcontributions.Event')),
            ],
            options={
                'verbose_name': 'Link',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EventMedia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.FileField(max_length=10485760, upload_to=b'event_media')),
                ('name', models.CharField(max_length=40, verbose_name='Description')),
                ('is_public', models.BooleanField(default=True, verbose_name='Public')),
                ('event', models.ForeignKey(related_name='media_set', to='callforcontributions.Event')),
            ],
            options={
                'verbose_name': 'Media File',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EventRoom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20)),
            ],
            options={
                'verbose_name': 'Room',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EventTag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('name_de', models.CharField(max_length=40, null=True)),
                ('name_en', models.CharField(max_length=40, null=True)),
                ('description', models.TextField()),
                ('description_de', models.TextField(null=True)),
                ('description_en', models.TextField(null=True)),
            ],
            options={
                'verbose_name': 'Tag',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EventTrack',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('name_de', models.CharField(max_length=40, null=True)),
                ('name_en', models.CharField(max_length=40, null=True)),
            ],
            options={
                'verbose_name': 'Track',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EventType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('name_de', models.CharField(max_length=30, null=True)),
                ('name_en', models.CharField(max_length=30, null=True)),
                ('short_description', models.TextField()),
                ('short_description_de', models.TextField(null=True)),
                ('short_description_en', models.TextField(null=True)),
                ('long_description', models.TextField()),
                ('long_description_de', models.TextField(null=True)),
                ('long_description_en', models.TextField(null=True)),
                ('url_snippet', models.SlugField(unique=True, max_length=20)),
                ('allowed_lengths', models.ManyToManyField(to='callforcontributions.EventLength', null=True, blank=True)),
                ('allowed_tags', models.ManyToManyField(to='callforcontributions.EventTag', null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Contribution Type',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='event',
            name='assets',
            field=models.ManyToManyField(to='callforcontributions.EventAsset', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='conference',
            field=models.ForeignKey(to='core.Conference'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='date',
            field=models.ForeignKey(blank=True, to='core.ConferenceDay', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='event_type',
            field=models.ForeignKey(verbose_name='Type of Submission', to='callforcontributions.EventType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='languages',
            field=models.ManyToManyField(to='callforcontributions.EventLanguage'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='length',
            field=models.ForeignKey(blank=True, to='callforcontributions.EventLength', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='room',
            field=models.ForeignKey(blank=True, to='callforcontributions.EventRoom', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='selected_tags',
            field=models.ManyToManyField(to='callforcontributions.EventTag', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='speaker',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='track',
            field=models.ForeignKey(blank=True, to='callforcontributions.EventTrack', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='conferenceeventsettings',
            name='assets',
            field=models.ManyToManyField(to='callforcontributions.EventAsset', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='conferenceeventsettings',
            name='event_types',
            field=models.ManyToManyField(to='callforcontributions.EventType', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='conferenceeventsettings',
            name='languages',
            field=models.ManyToManyField(to='callforcontributions.EventLanguage', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='conferenceeventsettings',
            name='rooms',
            field=models.ManyToManyField(to='callforcontributions.EventRoom', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='conferenceeventsettings',
            name='tracks',
            field=models.ManyToManyField(to='callforcontributions.EventTrack', blank=True),
            preserve_default=True,
        ),
    ]
