# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('callforcontributions', '0015_event_schedule_grid_description_html'),
    ]

    operations = [
        migrations.AddField(
            model_name='eventroom',
            name='name_de',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='eventroom',
            name='name_en',
            field=models.CharField(max_length=20, null=True),
        ),
    ]
