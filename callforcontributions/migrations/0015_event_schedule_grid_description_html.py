# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('callforcontributions', '0014_auto_20160524_1610'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='schedule_grid_description_html',
            field=models.TextField(help_text='This HTML code replaces the entire content of the event in the schedule grid. To produce a normal title, use <div class="title">Title goes here</div>.', verbose_name='Replacement for description in schedule grid (HTML)', blank=True),
        ),
    ]
