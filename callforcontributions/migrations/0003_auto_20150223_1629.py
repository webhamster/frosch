# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('callforcontributions', '0002_auto_20150222_2159'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventlink',
            name='link',
            field=models.CharField(max_length=400),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='eventmedia',
            name='event',
            field=models.ForeignKey(related_name='media_set', blank=True, to='callforcontributions.Event', null=True),
            preserve_default=True,
        ),
    ]
