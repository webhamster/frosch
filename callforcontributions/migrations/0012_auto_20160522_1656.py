# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('callforcontributions', '0011_conferenceeventsettings_is_archived'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='eventroom',
            options={'ordering': ['sort_key'], 'verbose_name': 'Room'},
        ),
        migrations.AddField(
            model_name='eventroom',
            name='sort_key',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='event',
            name='status',
            field=models.SmallIntegerField(default=0, choices=[(0, 'New'), (1, 'Withdrawn'), (2, 'Accepted/Unconfirmed'), (3, 'Accepted/Confirmed'), (4, 'Canceled'), (5, 'Rejected')]),
        ),
    ]
