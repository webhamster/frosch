# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-26 16:37
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('callforcontributions', '0003_auto_20171126_1737'),
    ]

    operations = [
        migrations.RenameField(
            model_name='event',
            old_name='_room',
            new_name='room',
        ),
    ]
