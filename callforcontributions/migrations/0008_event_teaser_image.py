# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('callforcontributions', '0007_event_hide_speakers'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='teaser_image',
            field=models.FileField(help_text='PNG Image used in public schedule.', upload_to=b'event_media', null=True, verbose_name='Teaser Image (PNG)', blank=True),
            preserve_default=True,
        ),
    ]
