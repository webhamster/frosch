# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('callforcontributions', '0013_auto_20160524_1521'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='schedule_grid_column_span',
            field=models.PositiveIntegerField(default=1, help_text='Small hack to span this event over several columns (rooms) in the schedule grid.', verbose_name='Column span in schedule grid'),
        ),
        migrations.AlterField(
            model_name='event',
            name='schedule_grid_css_class',
            field=models.CharField(help_text='CSS class of this event in schedule grid.', max_length=100, verbose_name='CSS class in schedule grid', blank=True),
        ),
    ]
