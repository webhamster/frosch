# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('callforcontributions', '0003_auto_20150223_1629'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventmedia',
            name='file',
            field=models.FileField(upload_to=b'event_media'),
            preserve_default=True,
        ),
    ]
