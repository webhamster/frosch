# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('callforcontributions', '0010_auto_20151207_1505'),
    ]

    operations = [
        migrations.AddField(
            model_name='conferenceeventsettings',
            name='is_archived',
            field=models.BooleanField(default=False, verbose_name=b'Archive mode (deny changes by users)'),
        ),
    ]
