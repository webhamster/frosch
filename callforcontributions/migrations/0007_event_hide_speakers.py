# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('callforcontributions', '0006_auto_20150226_1441'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='hide_speakers',
            field=models.BooleanField(default=False, verbose_name='Hide Speakers in Public Schedule'),
            preserve_default=True,
        ),
    ]
