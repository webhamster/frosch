# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('callforcontributions', '0012_auto_20160522_1656'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='schedule_grid_column_span',
            field=models.PositiveIntegerField(default=1, help_text=b'Quirk to span this event over several columns (rooms) in the schedule grid.', verbose_name='Column span in schedule grid'),
        ),
        migrations.AddField(
            model_name='event',
            name='schedule_grid_css_class',
            field=models.CharField(help_text='CSS class of this event in schedule grid.', max_length=100, verbose_name='', blank=True),
        ),
    ]
