# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('callforcontributions', '0005_auto_20150224_1219'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='description',
            field=models.TextField(help_text='\nDescribe your contribution. Make sure to answer at least the following questions:\n<ul class="help-block">\n<li>What is the topic of your presentation?\n<li>How do you plan to communicate it? (E.g., will it be more theory or more practice?)\n<li>What is your target audience? Beginner or advanced users? Professionals, makers, teachers?\n</ul>\n<p class="help-block">This description will be used in the official schedule if your contribution is accepted. You can make changes to this description until the official program is published. Any non-public information or notes for the organizer team can be added below.</p>\n', verbose_name='Description'),
            preserve_default=True,
        ),
    ]
