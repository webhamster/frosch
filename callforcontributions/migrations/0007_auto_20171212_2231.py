# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-12 21:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('callforcontributions', '0006_auto_20171206_2126'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='end_datetime',
            field=models.DateTimeField(blank=True, db_index=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='start_datetime',
            field=models.DateTimeField(blank=True, db_index=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='version',
            field=models.SmallIntegerField(db_index=True, default=0, editable=False),
        ),
    ]
