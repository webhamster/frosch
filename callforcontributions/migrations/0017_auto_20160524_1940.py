# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('callforcontributions', '0016_auto_20160524_1659'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventroom',
            name='name',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='eventroom',
            name='name_de',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='eventroom',
            name='name_en',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
