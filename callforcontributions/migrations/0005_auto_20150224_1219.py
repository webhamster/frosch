# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('callforcontributions', '0004_auto_20150223_1645'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventmedia',
            name='name',
            field=models.CharField(max_length=40, null=True, verbose_name='Description', blank=True),
            preserve_default=True,
        ),
    ]
