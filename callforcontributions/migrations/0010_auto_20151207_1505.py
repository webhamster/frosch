# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('callforcontributions', '0009_auto_20151202_1136'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='assets',
            field=models.ManyToManyField(to='callforcontributions.EventAsset', blank=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='selected_tags',
            field=models.ManyToManyField(to='callforcontributions.EventTag', blank=True),
        ),
        migrations.AlterField(
            model_name='eventtype',
            name='allowed_assets',
            field=models.ManyToManyField(to='callforcontributions.EventAsset', blank=True),
        ),
        migrations.AlterField(
            model_name='eventtype',
            name='allowed_lengths',
            field=models.ManyToManyField(to='callforcontributions.EventLength', blank=True),
        ),
        migrations.AlterField(
            model_name='eventtype',
            name='allowed_tags',
            field=models.ManyToManyField(to='callforcontributions.EventTag', blank=True),
        ),
    ]
