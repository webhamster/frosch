# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('callforcontributions', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='conferenceeventsettings',
            name='assets',
        ),
        migrations.AddField(
            model_name='eventtype',
            name='allowed_assets',
            field=models.ManyToManyField(to='callforcontributions.EventAsset', null=True, blank=True),
            preserve_default=True,
        ),
    ]
