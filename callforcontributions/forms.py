from django import forms
from callforcontributions.models import Event, EventLength, EventMedia, EventLink
from django.utils.html import mark_safe, conditional_escape
from django.forms.models import inlineformset_factory
from django.utils.translation import ugettext_lazy as _
from pagedown.widgets import PagedownWidget
from django.core.validators import validate_email, URLValidator
from django.conf import settings


class ModelMultipleChoiceFieldWithDescription(forms.ModelMultipleChoiceField):

    def label_from_instance(self, obj):
        label = conditional_escape(obj.name)
        description = conditional_escape(obj.description)
        return mark_safe("""<span class="choice-label">%s</span> <span class="text-muted">%s</span>""" % (label, description))

    
class EventUserForm(forms.ModelForm):
    def __init__(self, conference, event_type, *args, **kwargs):
        super(EventUserForm, self).__init__(*args, **kwargs)
        self.fields['languages'] = forms.ModelMultipleChoiceField(
            queryset=conference.eventsettings.languages.all(),
            widget=forms.CheckboxSelectMultiple,
            label=_("Languages"),
            help_text=_("The languages in which you can present your contribution.")
        )

        tags = event_type.allowed_tags.all()
        if len(tags):
            self.fields['selected_tags'] = ModelMultipleChoiceFieldWithDescription(
                queryset = tags, 
                widget = forms.CheckboxSelectMultiple, 
                required = False,
                label = _("Additional Information")
            )
        else:
            del self.fields['selected_tags']

        event = kwargs.get('instance', None)
        event_is_accepted = event.is_accepted if event else None
        types = event_type.allowed_lengths.all()
        type_admin_override = event and event.length and event.length not in types
        if type_admin_override or (event_is_accepted and event.length is not None):
            # special case: admin has set a non-allowed length or event has been accepted
            length = kwargs['instance'].length
            types = EventLength.objects.filter(id=length.id)
        length_help_text = None
        if type_admin_override:
            length_help_text = _("A special length has been set by the administrator.")
        if event_is_accepted:
            length_help_text = _("Your submission has been accepted for the conference. If you want to change the length of your contribution, please contact the organizers.")
        if len(types):
            self.fields['length'] = forms.ModelChoiceField(
                queryset = types, 
                widget = forms.RadioSelect, 
                empty_label = None,
                label = _("Length"),
                help_text = length_help_text,
                initial = types[0].id
            )
        else:
            del self.fields['length']

        assets = event_type.allowed_assets.all()
        if len(assets):
            self.fields['assets'] = forms.ModelMultipleChoiceField(
                queryset = assets, 
                widget = forms.CheckboxSelectMultiple,
                required = False,
                label = _("Hardware Requirements"),
                help_text = _("Please check any special equipment you need here.")
            )
        else:
            del self.fields['assets']

    def save(self, commit = True):
        super(EventUserForm, self).save(commit=commit)

        return self.instance

    class Meta:
        model = Event
        widgets = {
            'description': PagedownWidget(attrs={'class': 'form-control'})
        }
        fields = [
            'title', 
            'subtitle', 
            'description', 
            'notes', 
            'length', 
            'languages',  
            'assets', 
            'selected_tags'
        ]

class MultiEmailField(forms.CharField):
    def to_python(self, value):
        # Return an empty list if no input was given.
        if not value:
            return []
        return [x.strip() for x in value.split(',')]

    def validate(self, value):
        super(MultiEmailField, self).validate(value)

        for email in value:
            validate_email(email)

class InviteSpeakersForm(forms.Form):
    invite = MultiEmailField(label = _("Invite"), max_length = 1000, required = False, help_text = _("Enter email addresses of additional speakers, separated by commas, here. We will notify them by mail (which includes your email address) and if they confirm to become a co-speaker, they will be added as such. Additional speakers can change all event settings."))

class EventMediaForm(forms.ModelForm):
    class Meta:
        model = EventMedia
        fields = ('name', 'is_public', 'file')
        widgets = {
            'file': forms.FileInput() 
        }

    def clean_file(self):
         file = self.cleaned_data.get('file',False)
         if file:
             if file.size > settings.FROSCH_MAX_UPLOAD_SIZE:
                   raise forms.ValidationError(_("File is too large."))
             return file
         else:
             raise ValidationError(_("Couldn't read uploaded file."))

class EventLinkForm(forms.ModelForm):
    class Meta:
        model = EventLink
        fields = ('name', 'link', 'is_public')

    def clean_link(self):
        l = self.cleaned_data.get('link')
        if l and not l.lower().startswith('http://') and not l.lower().startswith('https://'):
            l = "http://%s" % l
        URLValidator()(l)
        return l

EventLinkFormSet = inlineformset_factory(
    Event, EventLink, EventLinkForm, max_num=10, extra=0, validate_max=True)

class WithdrawForm(forms.Form):
    reason  = forms.CharField(max_length = 1000, initial = '', required = False)
