from modeltranslation.translator import register, TranslationOptions
from callforcontributions.models import (
    EventTag,
    EventType,
    EventLength,
    EventLanguage,
    EventAsset,
    EventTrack,
    ConferenceEventSettings)


@register(EventType)
class EventTypeTranslationOptions(TranslationOptions):
    fields = ('name', 'short_description', 'long_description')


@register(EventTag)
class EventTagTranslationOptions(TranslationOptions):
    fields = ('name', 'description')


@register(EventLength)
class EventLengthTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(EventLanguage)
class EventLanguageTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(EventAsset)
class EventAssetTranslationOptions(TranslationOptions):
    fields = ('name', )


@register(EventTrack)
class EventTrackTranslationOptions(TranslationOptions):
    fields = ('name', )


@register(ConferenceEventSettings)
class ConferenceEventSettingsTranslationOptions(TranslationOptions):
    fields = ('link_title', 'description', )
