from django.conf.urls import url
from visitorregistration import views
from .sitemaps import VisitorregistrationStartViewSitemap

app_name = 'visitorregistration'

sitemaps = {
    'visitorregistration_start': VisitorregistrationStartViewSitemap(),
}

urlpatterns = [
    url(r'^$', views.StartView.as_view(), name='start'),
    url(r'^create$', views.CreateView.as_view(), name='create'),
    url(r'^edit/(?P<booking_id>[^/]+)$', views.EditView.as_view(), name='edit'),
    url(r'^show/(?P<booking_id>[^/]+)$', views.ShowView.as_view(), name='show'),
    url(r'^show/(?P<booking_id>[^/]+)/(?P<token>[^/]+)$', views.ShowWithTokenView.as_view(), name='show_with_token'),
    url(r'^request_confirmation_email/(?P<booking_id>[^/]+)$', views.RequestConfirmationEmailView.as_view(), name='request_confirmation_email'),
    url(r'^confirm_tickets/(?P<booking_id>[^/]+)$', views.ConfirmTicketsView.as_view(), name='confirm_tickets'),
    url(r'^request_email/$', views.RequestBookingTokens.as_view(), name='request_email'),
    url(r'^capacity$', views.RemainingCapacityView.as_view(), name='capacity'),
]
