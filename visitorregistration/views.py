from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, Http404
from core.models import Conference
from .models import Ticket, Visitor, Booking, MaxNumberOfVisitorsReachedError
from .forms import VisitorFormSet
import json
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.utils import timezone
from django.utils.html import escape
from post_office import mail
from core.utils import find_template
from django.views import View
from django.conf import settings
from core.view_utils import ConferenceQuerysets


def group_anon_visitors(anon_tickets):
    anon_visitors = []

    days = set()
    for state in anon_tickets:
        for day in anon_tickets[state]:
            days.add(day)

    # the following is probably possible in a nicer way
    def tickets_left(anon_tickets):
        for state in Ticket.ANONYMOUS_STATES:
            for day in days:
                if anon_tickets[state][day]:
                    return True
        return False
    while tickets_left(anon_tickets):
        anon_visitor_tickets = []
        for day in days:
            t = None
            for state in Ticket.ANONYMOUS_STATES:
                if anon_tickets[state][day]:
                    t = anon_tickets[state][day].pop()
                    break
            if not t:
                raise Exception('Booking %s contains not the same amount of anonymouys tickets for every day' % booking)
            anon_visitor_tickets.append(t)
        anon_visitors.append(anon_visitor_tickets)

    # this should be ordered by the states (PENDING first!)
    return anon_visitors


def create_visitorview_visitors(visitors, booking):
    visitorview = []
    for visitor in visitors:
        v = {
            'id': visitor.id,
            'name': visitor.name,
            'email': visitor.email,
            'subscribe_to_newsletter': visitor.subscribe_to_newsletter,
            'tickets': [],
            'ticket_ids': '',
            'toggle_cancel': False
            }
        for ticket in visitor.ticket_set.filter(booking__exact=booking):
            v['tickets'].append({
                'id': ticket.id,
                'day': ticket.day,
                'state': ticket.state,
                'state_expiration': ticket.state_expiration,
            })
            if v['ticket_ids']:
                v['ticket_ids'] += ','
            v['ticket_ids'] += str(ticket.id)
        visitorview.append(v)
        
    
    for visitor in visitorview:
        # infer visitor state and state expiration from her tickets
        visitor['state'] = visitor['tickets'][0]['state']
        visitor['state_expiration'] = visitor['tickets'][0]['state_expiration']
        for ticket in visitor['tickets']:
            visitor['state'] = Ticket.stronger_ticket_state(visitor['state'],ticket['state'])
            visitor['state_expiration'] = Ticket.earlier_ticket_state_expiration(visitor['state_expiration'], ticket['state_expiration'])
        if visitor['state_expiration']:
            visitor['state_expiration_delta_minutes'] = (visitor['state_expiration']- timezone.now()).seconds/60
        else:
            visitor['state_expiration_delta_minutes'] = None

    return visitorview

def create_visitorview_anon_visitors(anon_visitors, booking):
    visitorview = []    
    for anon_visitor in anon_visitors:
        v = {
            'tickets': [],
            'ticket_ids': '',
            'toggle_cancel': False
            }
        for ticket in anon_visitor:
            v['tickets'].append({
                'id': ticket.id,
                'day': ticket.day,
                'state': ticket.state,
                'state_expiration': ticket.state_expiration,
            })
            if v['ticket_ids']:
                v['ticket_ids'] += ','
            v['ticket_ids'] += str(ticket.id)
        visitorview.append(v)

    # infer visitor state and state expiration from her tickets
    for visitor in visitorview:
        visitor['state'] = visitor['tickets'][0]['state']
        visitor['state_expiration'] = visitor['tickets'][0]['state_expiration']
        for ticket in visitor['tickets']:
            visitor['state'] = Ticket.stronger_ticket_state(visitor['state'],ticket['state'])
            visitor['state_expiration'] = Ticket.earlier_ticket_state_expiration(visitor['state_expiration'], ticket['state_expiration'])
        if visitor['state_expiration']:
            visitor['state_expiration_delta_minutes'] = (visitor['state_expiration']- timezone.now()).seconds/60
        else:
            visitor['state_expiration_delta_minutes'] = None

    return visitorview

def create_visitorview(visitors, anon_visitors, booking):
    return create_visitorview_visitors(visitors, booking) + create_visitorview_anon_visitors(anon_visitors, booking)

def aggregate_states_from_visitorview(visitorview):
    states = {}
    # init
    for state, name in Ticket.STATES:
        states[state] = { 'count': 0 }
        # TODO: add expiration (tickets need to be expired first?)

    for visitor in visitorview:
        state = visitor['state']
        states[state]['count'] = states[state]['count'] + 1

    return states

def get_days_from_tickets(tickets):
    """ iterates over tickets and extract all days from tickets 
    :param tickets: iterable object containing Ticket instances
    :returns: set of ConferenceRegistrationDay objects
    """
    days = set()
    for ticket in tickets:
        days.add(ticket.day)

    return days

def get_visitors_from_tickets(tickets):
    """ iterates over tickets and extract all visitors
    :param tickets: iterable object containing Ticket instances
    :returns: a 2-tuple containing an array of visitors (with ticket contact first) and an array of anonymous visitors
    """

    days = get_days_from_tickets(tickets)

    visitors = []
    anon_tickets = {}
    for anon_state in Ticket.ANONYMOUS_STATES:
        anon_tickets[anon_state] = {}

    for state in anon_tickets:
        for day in days:
            anon_tickets[state][day] = []
    
    for ticket in tickets:
        if ticket.visitor:
            if ticket.visitor not in visitors:
                visitors.append(ticket.visitor)
        else:
            anon_tickets[ticket.state][ticket.day].append(ticket)

    # sort contact on visitor's first
    first_visitor = tickets[0].booking.contact
    if first_visitor:
        visitors_ordered = [first_visitor]
        for v in visitors:
            if v != first_visitor:
                visitors_ordered.append(v)
        visitors = visitors_ordered
        

    # group anonymous tickets to (anonymous) visitors
    anon_visitors = group_anon_visitors(anon_tickets)

    return visitors, anon_visitors


def get_active_conference_registration_from_conference_id(conference_id):
    conference = get_object_or_404(Conference, url_snippet = conference_id)
    if conference in ConferenceQuerysets.archived_conferences():
        raise Http404('registration is closed')
    if not hasattr(conference, 'visitor_registration'):
        raise Http404('registration is not active yet')
    reg = conference.visitor_registration
    if not reg.is_active:
        raise Http404('registration is not active yet')
    return conference, reg

class CreateView(View):
    def post(self, request, conference_id):
        if 'quantity' not in request.POST:
            raise Http404()

        conference, reg = get_active_conference_registration_from_conference_id(conference_id)

        quantity = 0
        try:
            quantity = int(request.POST['quantity'])
        except ValueError:
            quantity = 0
        if quantity <= 0:
            return render(request,'visitorregistration/error.html')

        booking = Booking.objects.create_booking_for_all_days(reg, quantity)

        if 'visitorregistration' not in request.session:
            request.session['visitorregistration'] = {
                'booking_ids': [booking.id],
            }
        else:
            request.session['visitorregistration']['booking_ids'].append(booking.id)

        # this is necessary as we did not always assign a new object to the session
        request.session.modified = True
        return redirect('visitorregistration:edit', conference_id, booking.id)

class StartView(View):
    def get(self, request, conference_id):
        """
        currently only registering for all days is implemented
        """
        conference, reg = get_active_conference_registration_from_conference_id(conference_id)

        return render(
            request,
            'visitorregistration/start.html',
            {
                'conference': conference,
                'registration': reg,
                'remaining': min(day.remaining_capacity() for day in reg.days.all()),
                'choices': range(1, reg.max_visitors_per_booking+1),
            }
    )

class RemainingCapacityView(View):
    def get(self, request, conference_id):
        conference, reg = get_active_conference_registration_from_conference_id(conference_id)
        
        result = { 
            'max_tickets':reg.max_visitors_per_booking,
            'days': {}
        }

        for day in reg.days.all():
            result['days'][day.id] = {
                'name': str(day.day),
                'remaining_capacity': day.remaining_capacity()
            }


        return HttpResponse(json.dumps(result), content_type='application/json')


def is_booking_id_in_session(request, conference_id, booking_id):
    if 'visitorregistration' not in request.session:
        return False
    if 'booking_ids' not in request.session['visitorregistration']:
        return False
    return (int(booking_id) in request.session['visitorregistration']['booking_ids'])


class EditView(View):
    def prepare(self, request, conference_id, booking_id):
        conference, reg = get_active_conference_registration_from_conference_id(conference_id)
        if not is_booking_id_in_session(request, conference_id, booking_id):
            raise Http404()
        booking = get_object_or_404(Booking, id=booking_id)
        if booking.conference_registration.conference != conference:
            raise Http404()
        return conference, reg, booking


    def post(self, request, conference_id, booking_id):
        conference, reg, booking = self.prepare(request, conference_id, booking_id)
        visitor_formset = VisitorFormSet(request.POST)
        if 'action' not in request.POST:
            raise Http404()
        action = request.POST['action']

        stay_on_edit_page = True

        # encapsulate the following into try..catch block?
        # otherwise a DoesNotExist exception is raised
        # but this exception will trigger an email to admins in production
        if visitor_formset.is_valid():
            stay_on_edit_page = False
            
            # ALWAYS check if tickets / visitors belong to booking! (done by just querying the correct sets)
            first_visitor = True
            for visitor_form in visitor_formset:
                id = visitor_form.cleaned_data['id']
                name = visitor_form.cleaned_data['name']
                email = visitor_form.cleaned_data['email']
                subscribe_to_newsletter = visitor_form.cleaned_data['subscribe_to_newsletter']
                ticket_ids = visitor_form.cleaned_data['ticket_ids'].split(',')
                toggle_cancel = visitor_form.cleaned_data['toggle_cancel']

                tickets = []
                for ticket_id in ticket_ids:
                    ticket = booking.ticket_set.get(id=ticket_id)
                    tickets.append(ticket)

                state = tickets[0].state
                for ticket in tickets:
                    state = Ticket.stronger_ticket_state(state, ticket.state)

                visitor = None
                if visitor_form.cleaned_data['id'] == None:
                    if not email:
                        # if email is set then also name is set (enforced by form validation)
                        # Idea: do nothing
                        pass
                    else:                        
                        visitor = Visitor(name=name, email=email, subscribe_to_newsletter=subscribe_to_newsletter)
                        visitor.clean()
                        visitor.save()
                        visitor.assign_tickets(tickets)
                else:
                    visitor = booking.visitors.get(id=id)
                    if not email:
                        # if email is set then also name is set (enforced by form validation)
                        # TODO: catch error
                        # Idea: set tickets to pending again
                        # currently, this case cannot occur (enforced by formset validation)
                        pass
                    else:
                        visitor.name = name
                        visitor.email = email
                        visitor.subscribe_to_newsletter = subscribe_to_newsletter
                        visitor.clean()
                        visitor.save()
                        visitor.assign_tickets(tickets)

                if toggle_cancel:
                    if state != Ticket.CANCELED:
                        if visitor:
                            messages.warning(request, _('You have canceled the registration for %(name)s (%(email)s).') % { 'name': visitor.name, 'email': visitor.email})
                        for ticket in tickets:
                            ticket.cancel()
                    else:
                        try:
                            for ticket in tickets:
                                ticket.uncancel()
                        except MaxNumberOfVisitorsReachedError:
                            messages.error(request, _('You can only have up to %(max_visitors)i registered visitors in this booking. To un-cancel a visitor registration you can either (1) cancel other tickets and then try again, or (2) create a new separate booking, or (3) <a href="%(contact_url)s">contact us</a> for a group reservation.') % { 'max_visitors': booking.get_max_visitors(), 'contact_url': settings.FROSCH_CONTACT_URL[request.LANGUAGE_CODE] })
                            stay_on_edit_page = True

                # if first set contact in booking
                if first_visitor:
                    # first item must always be a visitor and not anonymous (enforced by formset validation)
                    booking.contact = visitor
                    booking.clean()
                    booking.save()

                first_visitor = False

            # claim tickets
            booking.claim_tickets()
            
            if action == 'add':
                try:
                    booking.add_visitors_for_all_days()
                except MaxNumberOfVisitorsReachedError:
                    messages.error(request, _('You can only have up to %(max_visitors)i registered visitors in this booking. If you want to attend with a larger group, please <a href="%(contact_url)s">contact us</a>.') % { 'max_visitors': booking.get_max_visitors(), 'contact_url': settings.FROSCH_CONTACT_URL[request.LANGUAGE_CODE] })
                return redirect('visitorregistration:edit', conference_id, booking.id)

            if booking.state == Booking.NEW:
                if 'visitorregistration' not in request.session:
                    request.session['visitorregistration'] = {}
                if 'booking' not in request.session['visitorregistration']:
                    request.session['visitorregistration']['booking'] = {}
                if str(booking.id) not in request.session['visitorregistration']['booking']:
                    request.session['visitorregistration']['booking'][str(booking.id)] = {}
                if 'confirmation_email_sent' not in request.session['visitorregistration']['booking'][str(booking.id)]:
                    request.session['visitorregistration']['booking'][str(booking.id)]['confirmation_email_sent'] = False
                if not request.session['visitorregistration']['booking'][str(booking.id)]['confirmation_email_sent']:
                    booking.send_confirmation_email(request)
                    request.session['visitorregistration']['booking'][str(booking.id)]['confirmation_email_sent'] = True
                    request.session.modified = True
                    
                    return render(request, 'visitorregistration/wait_for_email.html', {
                        'conference': conference,
                        'registration': reg,
                    })

        if not stay_on_edit_page:
            return redirect('visitorregistration:show', conference_id, booking.id)

        return render(request, 'visitorregistration/edit.html', {
            'registration': reg,
            'conference': conference,
            'visitor_formset':visitor_formset,
            'booking': booking,
            'booking_state': {
                'ok': Booking.OK,
                'new': Booking.NEW,
            },
            'ticket_state': {
                'ok': Ticket.OK,
                'canceled': Ticket.CANCELED,
                'pending_ok': Ticket.PENDING_OK,
                'unconfirmed': Ticket.UNCONFIRMED,
                'waiting': Ticket.WAITING,
                'pending_waiting': Ticket.PENDING_WAITING,
                'stale': Ticket.STALE,
                'new': Ticket.NEW,
            },
        })


    def get(self, request, conference_id, booking_id):
        conference, reg, booking = self.prepare(request, conference_id, booking_id)
        booking.remove_expired_tickets_without_visitor()
        
        tickets = booking.ticket_set.all()

        if not tickets:
            booking.delete()
            messages.error(request, _('Your reservations have expired. Please start over.'))
            
            return redirect('visitorregistration:start', conference_id)
            
        visitors, anon_visitors = get_visitors_from_tickets(tickets)

        visitorview = create_visitorview(visitors, anon_visitors, booking)

        visitor_formset = VisitorFormSet(initial=visitorview)

        return render(request, 'visitorregistration/edit.html', {
            'registration': reg,
            'conference': conference,
            'visitor_formset':visitor_formset,
            'booking': booking,
            'booking_state': {
                'ok': Booking.OK,
                'new': Booking.NEW,
            },
            'ticket_state': {
                'ok': Ticket.OK,
                'canceled': Ticket.CANCELED,
                'pending_ok': Ticket.PENDING_OK,
                'unconfirmed': Ticket.UNCONFIRMED,
                'waiting': Ticket.WAITING,
                'pending_waiting': Ticket.PENDING_WAITING,
                'stale': Ticket.STALE,
                'new': Ticket.NEW,
            },
        })

class ShowView(View):
    def get(self, request, conference_id, booking_id):
        conference, reg = get_active_conference_registration_from_conference_id(conference_id)
        if not is_booking_id_in_session(request, conference_id, booking_id):
            raise Http404()
        booking = get_object_or_404(Booking, id=booking_id)
        if booking.conference_registration.conference != conference:
            raise Http404()

        booking.remove_expired_tickets_without_visitor()

        tickets = booking.ticket_set.all()
        visitors, anon_visitors = get_visitors_from_tickets(tickets)
        visitorview = create_visitorview(visitors, anon_visitors, booking)

        #visitorview_visitors = create_visitorview_visitors(visitors, booking)
        #visitorview_anon_visitors = create_visitorview_anon_visitors(anon_visitors, booking)
        #remaining_tickets = aggregate_states_from_visitorview(visitorview_anon_visitors)

        can_confirm_tickets = False
        for t in visitorview:
            if t['state'] == Ticket.UNCONFIRMED:
                can_confirm_tickets = True
                break

        return render(request, 'visitorregistration/show.html', {
            'registration': reg,
            'conference': conference,
            'booking': booking,
            'visitorview':visitorview,
            'booking_state': {
                'ok': Booking.OK,
                'new': Booking.NEW,
            },
            'ticket_state': {
                'ok': Ticket.OK,
                'canceled': Ticket.CANCELED,
                'pending_ok': Ticket.PENDING_OK,
                'unconfirmed': Ticket.UNCONFIRMED,
                'waiting': Ticket.WAITING,
                'pending_waiting': Ticket.PENDING_WAITING,
                'stale': Ticket.STALE,
                'new': Ticket.NEW,
            },
            'can_confirm_tickets': can_confirm_tickets,
        })


class ShowWithTokenView(View):
    def get(self, request, conference_id, booking_id, token):
        conference, reg = get_active_conference_registration_from_conference_id(conference_id)
        booking = get_object_or_404(Booking, id=booking_id)

        if booking.conference_registration.conference != conference:
            raise Http404()

        if booking.token != token:
            raise Http404()

        if 'visitorregistration' not in request.session:
            request.session['visitorregistration'] = {
                'booking_ids': [booking.id],
            }
        elif 'booking_id' not in request.session['visitorregistration']['booking_ids']:
            request.session['visitorregistration']['booking_ids'].append(booking.id)

        # this is necessary as we did not always assign a new object to the session
        request.session.modified = True

        if booking.confirm_booking():
            for ticket in booking.ticket_set.all():
                if ticket.state not in [Ticket.STALE, Ticket.CANCELED]:
                    messages.success(request, _('Your booking has been confirmed!'))
                    break

        return redirect('visitorregistration:show', conference_id, booking_id)


class RequestConfirmationEmailView(View):
    def post(self, request, conference_id, booking_id):
        conference, reg = get_active_conference_registration_from_conference_id(conference_id)
        if not is_booking_id_in_session(request, conference_id, booking_id):
            raise Http404()
        booking = get_object_or_404(Booking, id=booking_id)
        if booking.conference_registration.conference != conference:
            raise Http404()

        booking.send_confirmation_email(request)

        return HttpResponse("Sent.", content_type="text/plain")


class ConfirmTicketsView(View):
    def post(self, request, conference_id, booking_id):
        ''' confirm tickets which have been released from waiting list to become 'booked' '''
        conference, reg = get_active_conference_registration_from_conference_id(conference_id)
        if not is_booking_id_in_session(request, conference_id, booking_id):
            raise Http404()
        booking = get_object_or_404(Booking, id=booking_id)
        if booking.conference_registration.conference != conference:
            raise Http404()

        booking.confirm_tickets()

        return redirect('visitorregistration:show', conference_id, booking_id)


class RequestBookingTokens(View):
    def post(self, request, conference_id):
        if 'email' not in request.POST:
            return redirect('visitorregistration:start', conference_id)
        conference, reg = get_active_conference_registration_from_conference_id(conference_id)

        email = request.POST['email']

        bookings = Booking.objects.filter(conference_registration=reg).filter(contact__email__exact=email)

        if bookings:
            template, lang_context = find_template('visitorregistration.request-booking-tokens')
            with lang_context:
                mail.send(
                    template=template,
                    recipients=[email],
                    context={
                        'conference': conference,
                        'bookings': bookings,
                    },
                    priority=mail.PRIORITY.now,
                )

            return render(request, 'visitorregistration/request_booking_tokens.html', {
                'registration': reg,
            })

        messages.warning(request, _("We couldn't find the email address %s in our system.") % escape(email))

        return redirect('visitorregistration:start', conference_id)
