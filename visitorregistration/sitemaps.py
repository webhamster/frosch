from django.contrib.sitemaps import Sitemap
from django.urls import reverse
from .models import ConferenceRegistration


class VisitorregistrationStartViewSitemap(Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return ConferenceRegistration.objects.filter(conference__is_active=True, is_active=True)

