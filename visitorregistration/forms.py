from django import forms
from django.forms.formsets import formset_factory, BaseFormSet
from django.forms.widgets import HiddenInput
from django.forms.utils import ValidationError
from django.utils.translation import ugettext as _

class VisitorForm(forms.Form):
    id = forms.IntegerField(widget = HiddenInput, required = False) 
    name = forms.CharField(required = False)
    email = forms.EmailField(required = False)
    subscribe_to_newsletter = forms.BooleanField(required=False)
    ticket_ids = forms.CharField(widget = HiddenInput)
    toggle_cancel = forms.BooleanField(required=False)
    
    
    def clean(self):
        cleaned_data = super(VisitorForm, self).clean()
        id = cleaned_data.get("id")
        name = cleaned_data.get("name")
        email = cleaned_data.get("email")

        if id and not name and not email:
            self.add_error('name', _('Please enter the name of this visitor.'))
            self.add_error('email', _('Please enter the email address of this visitor.'))
        else:
            if name and not email:
                self.add_error('email', _('Please enter the email address of this visitor.'))
            elif email and not name:
                self.add_error('name', _('Please enter the name of this visitor.'))



class BaseVisitorFormSet(BaseFormSet):
    def clean(self):
        if any(self.errors):
            # Don't bother validating the formset unless each form is valid on its own
            return
        if not self.forms[0].cleaned_data['email']:
            # first visitor has not been set (if email is set all other required fields are guaranteed to be set due to the VisitorForm validation
            self.forms[0].add_error('name', _('Please enter the name of this visitor.'))
            self.forms[0].add_error('email', _('Please enter the email address of this visitor.'))


VisitorFormSet = formset_factory(VisitorForm, formset=BaseVisitorFormSet, extra=0, can_delete=False)
