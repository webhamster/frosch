from core.tests import CoreTestSetup
from django.urls import reverse
from django.core import mail
from visitorregistration.models import Booking, ConferenceRegistration, Ticket


class VisitorregistrationTestSetup(CoreTestSetup):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.visitorregistration = ConferenceRegistration.objects.create(
            conference=cls.conference,
            max_visitors_per_booking=5,
            show_remaining_capacity_limit = 2,
            #booking_tickets_pending_timeout = ..,
            #booking_new_timeout = ..,
            #tickets_unconfirmed_waiting_timeout = ..,
            is_active=True,
            welcome_text='welcome_text',
            wait_for_email_text='wait for email text',
            show_registration_text='show registration_text',
        )
        for day in cls.cds:
            if not day.public_start:
                continue
            cls.visitorregistration.days.create(
                day=day,
                capacity=10,
                overbooking_quantity=2,
            )


class VisitorregistrationTest(VisitorregistrationTestSetup):

    def setUp(self):
        self.mailcount = 0

    def get_start_page(self):
        response = self.app.get(
            reverse(
                'visitorregistration:start',
                kwargs={'conference_id': self.conference.url_snippet}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'visitorregistration/start.html')
        return response

    def _request_tickets(self, start_page, visitors):
        form = start_page.forms['ticket-form']
        edit_page = form.submit(name='quantity', value=str(len(visitors))).follow()

        self.assertEqual(edit_page.status_code, 200)
        self.assertTemplateUsed(edit_page, 'visitorregistration/edit.html')

        return edit_page

    def _check_if_new_booking(self, edit_page, visitors):
        booking = edit_page.context['booking']
        booking_db = Booking.objects.get(id=booking.id)
        self.assertNotEqual(booking_db, None)
        self.assertEqual(booking.state, Booking.NEW)
        tickets = booking.ticket_set.all()
        self.assertEqual(len(tickets), len(visitors))
        for ticket in tickets:
            self.assertIn(ticket.state, [Ticket.PENDING_OK, Ticket.PENDING_WAITING])
        return booking.id

    def _assign_visitors_to_tickets(self, edit_page, visitors):
        visitor_formset = edit_page.context['visitor_formset']
        self.assertEqual(len(visitors), len(visitor_formset))
        form = edit_page.forms['ticket-form']
        for i in range(len(visitors)):
            form[visitor_formset[i]['name'].html_name] = visitors[i][0]
            form[visitor_formset[i]['email'].html_name] = visitors[i][1]
        response = form.submit(name='action', value='save')

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'visitorregistration/wait_for_email.html')

        return response

    def _check_if_new_email_sent(self):
        self.assertEqual(len(mail.outbox), self.mailcount + 1)
        self.mailcount += 1
        return mail.outbox[-1]

    def _check_if_new_email_with_confirmation_link(self, booking_id):
        m = self._check_if_new_email_sent()
        booking = Booking.objects.get(id=booking_id)
        confirm_url = reverse(
            'visitorregistration:show_with_token',
            kwargs={
                'conference_id': self.conference.url_snippet,
                'booking_id': booking_id,
                'token': booking.token})
        self.assertIn(confirm_url, m.body)
        return confirm_url

    def _confirm_booking(self, confirm_url):
        response = self.app.get(confirm_url).follow()
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'visitorregistration/show.html')
        m = self._check_if_new_email_sent()
        self.assertIn(confirm_url, m.body)
        return response

    def _check_booking_ok(self, booking_id, visitors):
        booking = Booking.objects.get(id=booking_id)
        self.assertEqual(booking.state, Booking.OK)
        booking = Booking.objects.get(id=booking_id)
        tickets = booking.ticket_set.all()

        visitor_map = {visitor[0]: visitor for visitor in visitors}
        self.assertEqual(len(tickets), len(visitors))
        for ticket in tickets:
            visitor = visitor_map[ticket.visitor.name]
            self.assertEqual(ticket.visitor.name, visitor[0])
            self.assertEqual(ticket.visitor.email, visitor[1])
            self.assertEqual(ticket.state, visitor[2])

    def book_tickets(self, visitors):
        start_page = self.get_start_page()
        edit_page = self._request_tickets(start_page, visitors)
        booking_id = self._check_if_new_booking(edit_page, visitors)
        self._assign_visitors_to_tickets(edit_page, visitors)
        confirm_url = self._check_if_new_email_with_confirmation_link(
            booking_id)
        self._confirm_booking(confirm_url)
        self._check_booking_ok(booking_id, visitors)
        return booking_id

    def test_book_one_more_than_allowed(self):
        response = self.get_start_page()
        quantity = self.visitorregistration.max_visitors_per_booking + 1
        form = response.forms['ticket-form']
        response = form.submit(name='quantity', value=quantity, status=404)

    def test_book_one(self):
        visitors = [
            ('Alice', 'alice@example.com', Ticket.OK),
        ]
        self.book_tickets(visitors)

    def test_book_two(self):
        visitors = [
            ('Alice', 'alice@example.com', Ticket.OK),
            ('Bob', 'bob@example.com', Ticket.OK),
        ]
        self.book_tickets(visitors)

    def test_book_into_waitinglist(self):
        visitors1 = [
            ('Alice', 'alice@example.com', Ticket.OK),
            ('Bob', 'bob@example.com', Ticket.OK),
            ('Charlie', 'charlie@example.com', Ticket.OK),
            ('Dagobert', 'dagobert@example.com', Ticket.OK),
            ('Eve', 'eve@example.com', Ticket.OK),
        ]
        visitors2 = [
            ('Fred', 'fred@example.com', Ticket.OK),
            ('Grumpy', 'grumpy@example.com', Ticket.OK),
            ('Humphrey', 'humphrey@example.com', Ticket.OK),
            ('Ida', 'ida@example.com', Ticket.OK),
            ('Jane', 'jane@example.com', Ticket.OK),
        ]
        visitors3 = [
            ('Kane', 'kane@example.com', Ticket.WAITING),
        ]
        self.book_tickets(visitors1)
        self.book_tickets(visitors2)
        self.book_tickets(visitors3)

