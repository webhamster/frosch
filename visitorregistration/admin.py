from django.contrib import admin
from .models import ConferenceRegistrationDay, ConferenceRegistration, Ticket, Booking, Visitor
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponse
from django.conf.urls import url
from django.shortcuts import get_object_or_404
from django.utils.html import mark_safe
from django.urls import reverse
from core.models import ConferenceDay
from modeltranslation.admin import TranslationStackedInline


class DeleteVisitorRegistrationsAdminExtension:
    def actions(self):
        def deleteVisitorRegistrations(self, request, queryset):
            for reg in queryset:
                tickets = Ticket.objects.filter(day__in = reg.days.values('id'))
                visitors = Visitor.objects.filter(id__in=tickets.values('visitor_id'))
                bookings = Booking.objects.filter(id__in=tickets.values('booking_id'))
                visitors.delete()
                bookings.delete()
                tickets.delete()
        deleteVisitorRegistrations.short_description = ' Delete visitor registrations'
        return (deleteVisitorRegistrations, )


class ConferenceRegistrationDayInline(admin.TabularInline):
    model = ConferenceRegistrationDay
    extra = 0

    
@admin.register(ConferenceRegistration)
class ConferenceRegistrationAdmin(admin.ModelAdmin):
    inlines = [
        ConferenceRegistrationDayInline,
    ]
    list_display = (str, 'booked_tickets', 'booked_tickets_last_24h', 'custom_actions')
    actions = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._extensions = [DeleteVisitorRegistrationsAdminExtension()]

        for e in self._extensions:
            if hasattr(e, 'actions'):
                self.actions.extend(e.actions())

    def custom_actions(self, registration):
        return mark_safe(
            """<a href="%s">show newsletter subscribers</a>, <a href="%s">show emails of OK tickets</a>""" % (
                reverse('admin:conference_registration_newsletter_subscribers', args=[registration.conference.id]),
                reverse('admin:conference_registration_ok_visitor_emails', args=[registration.conference.id]))
        )

    def get_urls(self):
        urls = super(ConferenceRegistrationAdmin, self).get_urls()
        my_urls = [
            url(r'(?P<conference_id>\d+)/newsletter_subscribers/$',
                self.admin_site.admin_view(self.newsletter_subscribers_view),
                name="conference_registration_newsletter_subscribers"),
            url(r'(?P<conference_id>\d+)/ok_visitor_emails/$',
                self.admin_site.admin_view(self.ok_visitor_emails_view),
                name="conference_registration_ok_visitor_emails")
        ]

        return my_urls + urls

    def newsletter_subscribers_view(self, request, conference_id):
        registration = get_object_or_404(ConferenceRegistration, conference=conference_id)

        subscribers = set()

        days = registration.days.all()
        tickets = set()
        for day in days:
            tickets = tickets.union(day.ticket_set.all())
        for ticket in tickets:
            if ticket.visitor and ticket.visitor.subscribe_to_newsletter:
                subscribers.add(ticket.visitor.email)

        content = ''
        for s in subscribers:
            content += '%s\n' % s
        return HttpResponse(content, content_type='text/plain')
            
    def ok_visitor_emails_view(self, request, conference_id):
        registration = get_object_or_404(ConferenceRegistration, conference=conference_id)

        subscribers = set()

        days = registration.days.all()
        tickets = set()
        for day in days:
            tickets = tickets.union(day.ticket_set.filter(state=Ticket.OK).all())
        for ticket in tickets:
            if ticket.visitor:
                subscribers.add(ticket.visitor.email)

        content = ''
        for s in subscribers:
            content += '%s\n' % s
        return HttpResponse(content, content_type='text/plain')
            
    
class TicketInline(admin.TabularInline):
    model = Ticket
    extra = 0

    
@admin.register(Booking)
class BookingAdmin(admin.ModelAdmin):
    inlines = [
        TicketInline,
    ]

    
class TicketStateListFilter(admin.SimpleListFilter):
    title = 'State'
    parameter_name = 'state'

    def lookups(self, request, model_admin):
        return Ticket.STATES

    def queryset(self, request, queryset):
        if self.value() is not None:
            return queryset.filter(state=self.value())
        return queryset

    
class TicketConferenceListFilter(admin.SimpleListFilter):
    title = 'Conference'
    parameter_name = 'conference'

    def lookups(self, request, model_admin):
        conferences = ConferenceRegistration.objects.all()
        return [('*', _('All')), (None, _('Latest'))] + [(conf.id, conf.conference.short_name) for conf in conferences]

    def choices(self, changelist):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == lookup,
                'query_string': changelist.get_query_string({
                    self.parameter_name: lookup,
                }, []),
                'display': title,
            }

    def queryset(self, request, queryset):
        if self.value() == '*':
            return queryset
        if self.value():
            return queryset.filter(day__in=ConferenceRegistration.objects.get(id=self.value()).days.all())
        reg = ConferenceDay.objects.filter(
            visitor_capacity__isnull=False).latest('date').conference.visitor_registration
        return queryset.filter(day__in=reg.days.all())

    
class TicketConferenceDayFilter(admin.SimpleListFilter):
    title = 'Day'
    parameter_name = 'day'

    def lookups(self, request, model_admin):
        days = set([t.day for t in model_admin.model.objects.all()])
        return [(d.id, str(d)) for d in days]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(day__exact=self.value())
        return queryset

    
@admin.register(Ticket)
class TicketAdmin(admin.ModelAdmin):
    list_filter = (TicketConferenceListFilter, TicketStateListFilter, TicketConferenceDayFilter)
    list_display = ('get_day', 'visitor', 'booking', 'created', 'state')

    def get_day(self, obj):
        return str(obj.day.day)

    
admin.site.register(Visitor)


class ConferenceRegistrationInline(TranslationStackedInline):
    model = ConferenceRegistration
    extra = 1
