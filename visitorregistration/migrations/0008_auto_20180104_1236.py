# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-01-04 11:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('visitorregistration', '0007_merge_20180104_1236'),
    ]

    operations = [
        migrations.AlterField(
            model_name='conferenceregistration',
            name='show_registration_text',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AlterField(
            model_name='conferenceregistration',
            name='show_registration_text_de',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AlterField(
            model_name='conferenceregistration',
            name='show_registration_text_en',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AlterField(
            model_name='conferenceregistration',
            name='wait_for_email_text',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AlterField(
            model_name='conferenceregistration',
            name='wait_for_email_text_de',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AlterField(
            model_name='conferenceregistration',
            name='wait_for_email_text_en',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AlterField(
            model_name='conferenceregistration',
            name='welcome_text',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AlterField(
            model_name='conferenceregistration',
            name='welcome_text_de',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AlterField(
            model_name='conferenceregistration',
            name='welcome_text_en',
            field=models.TextField(blank=True, default='', null=True),
        ),
    ]
