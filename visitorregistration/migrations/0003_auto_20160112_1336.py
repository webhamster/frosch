# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('visitorregistration', '0002_visitor_subscribe_to_newsletter'),
    ]

    operations = [
        migrations.AlterField(
            model_name='visitor',
            name='subscribe_to_newsletter',
            field=models.BooleanField(default=False, verbose_name=b'Subscribe to newsletter'),
        ),
    ]
