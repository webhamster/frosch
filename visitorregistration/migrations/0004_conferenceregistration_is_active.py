# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('visitorregistration', '0003_auto_20160112_1336'),
    ]

    operations = [
        migrations.AddField(
            model_name='conferenceregistration',
            name='is_active',
            field=models.BooleanField(default=False, verbose_name=b'Make visitor registration publicly accessible'),
        ),
    ]
