# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('visitorregistration', '0005_conferenceregistration_show_remaining_capacity_limit'),
    ]

    operations = [
        migrations.AddField(
            model_name='conferenceregistration',
            name='show_registration_text',
            field=models.TextField(default=b'', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='conferenceregistration',
            name='show_registration_text_de',
            field=models.TextField(default=b'', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='conferenceregistration',
            name='show_registration_text_en',
            field=models.TextField(default=b'', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='conferenceregistration',
            name='wait_for_email_text',
            field=models.TextField(default=b'', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='conferenceregistration',
            name='wait_for_email_text_de',
            field=models.TextField(default=b'', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='conferenceregistration',
            name='wait_for_email_text_en',
            field=models.TextField(default=b'', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='conferenceregistration',
            name='welcome_text',
            field=models.TextField(default=b'', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='conferenceregistration',
            name='welcome_text_de',
            field=models.TextField(default=b'', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='conferenceregistration',
            name='welcome_text_en',
            field=models.TextField(default=b'', null=True, blank=True),
        ),
    ]
