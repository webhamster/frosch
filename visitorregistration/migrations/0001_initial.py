# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import core.models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_auto_20151016_1914'),
    ]

    operations = [
        migrations.CreateModel(
            name='Booking',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('token', models.CharField(max_length=64)),
                ('preferred_language', models.CharField(default=core.models.get_initial_lang, help_text='Used for emails.', max_length=10, verbose_name='Preferred language', choices=[(b'de', 'German'), (b'en', 'English')])),
                ('max_visitors_override', models.PositiveIntegerField(null=True, blank=True)),
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modification_time', models.DateTimeField(auto_now=True)),
                ('state', models.IntegerField(default=1, choices=[(0, b'OK'), (1, b'NEW')])),
                ('state_expiration', models.DateTimeField(default=None, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='ConferenceRegistration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('max_visitors_per_booking', models.PositiveIntegerField(default=5, verbose_name=b'Limit for visitors per booking (can be overridden per booking)')),
                ('booking_tickets_pending_timeout', models.PositiveIntegerField(default=600, verbose_name=b'Timeout for tickets without a visitor assigned')),
                ('booking_new_timeout', models.PositiveIntegerField(default=86400, verbose_name=b'Timeout for tickets until booking is confirmed by email token')),
                ('tickets_unconfirmed_waiting_timeout', models.PositiveIntegerField(default=86400, verbose_name=b'Timeout for tickets which have been released from the waiting list')),
                ('conference', models.OneToOneField(related_name='visitor_registration', to='core.Conference')),
            ],
        ),
        migrations.CreateModel(
            name='ConferenceRegistrationDay',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('capacity', models.PositiveIntegerField()),
                ('overbooking_quantity', models.PositiveIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Ticket',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('state', models.IntegerField(default=7, choices=[(0, b'OK'), (1, b'CANCELED'), (2, b'PENDING_OK'), (3, b'UNCONFIRMED'), (4, b'WAITING'), (5, b'PENDING_WAITING'), (6, b'STALE'), (7, b'NEW')])),
                ('state_expiration', models.DateTimeField(default=None, null=True, blank=True)),
                ('entered_waiting_queue', models.DateTimeField(default=None, null=True, blank=True)),
                ('created', models.DateTimeField()),
                ('changed', models.DateTimeField()),
                ('booking', models.ForeignKey(to='visitorregistration.Booking')),
                ('day', models.ForeignKey(to='visitorregistration.ConferenceRegistrationDay')),
            ],
        ),
        migrations.CreateModel(
            name='Visitor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('email', models.EmailField(max_length=254)),
            ],
        ),
        migrations.AddField(
            model_name='ticket',
            name='visitor',
            field=models.ForeignKey(blank=True, to='visitorregistration.Visitor', null=True),
        ),
        migrations.AddField(
            model_name='conferenceregistrationday',
            name='bookings',
            field=models.ManyToManyField(to='visitorregistration.Booking', through='visitorregistration.Ticket'),
        ),
        migrations.AddField(
            model_name='conferenceregistrationday',
            name='conference_registration',
            field=models.ForeignKey(related_name='days', to='visitorregistration.ConferenceRegistration'),
        ),
        migrations.AddField(
            model_name='conferenceregistrationday',
            name='day',
            field=models.OneToOneField(related_name='visitor_capacity', to='core.ConferenceDay'),
        ),
        migrations.AddField(
            model_name='conferenceregistrationday',
            name='visitors',
            field=models.ManyToManyField(to='visitorregistration.Visitor', through='visitorregistration.Ticket'),
        ),
        migrations.AddField(
            model_name='booking',
            name='conference_registration',
            field=models.ForeignKey(to='visitorregistration.ConferenceRegistration'),
        ),
        migrations.AddField(
            model_name='booking',
            name='contact',
            field=models.ForeignKey(related_name='+', blank=True, to='visitorregistration.Visitor', null=True),
        ),
        migrations.AddField(
            model_name='booking',
            name='visitors',
            field=models.ManyToManyField(to='visitorregistration.Visitor', through='visitorregistration.Ticket'),
        ),
    ]
