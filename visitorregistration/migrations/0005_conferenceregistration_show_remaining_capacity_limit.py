# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('visitorregistration', '0004_conferenceregistration_is_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='conferenceregistration',
            name='show_remaining_capacity_limit',
            field=models.PositiveIntegerField(default=50, verbose_name=b'Show remaining capacity when remaining tickets are less or equal than this limit.'),
        ),
    ]
