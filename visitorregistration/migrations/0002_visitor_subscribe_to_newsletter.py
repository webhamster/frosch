# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('visitorregistration', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='visitor',
            name='subscribe_to_newsletter',
            field=models.BooleanField(default=False, verbose_name=b'Subscribe to the newsletter'),
        ),
    ]
