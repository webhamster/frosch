from django.db import models, transaction
from core.models import Conference, ConferenceDay
from datetime import datetime, timedelta
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from core.models import get_initial_lang
from django.utils import translation
from urllib.parse import urljoin
from core.utils import find_template
from post_office import mail
from core.view_utils import ConferenceQuerysets
import random


class ConferenceRegistration(models.Model):
    conference = models.OneToOneField(Conference, related_name='visitor_registration', on_delete=models.CASCADE)
    max_visitors_per_booking = models.PositiveIntegerField("Limit for visitors per booking (can be overridden per booking)", default=5)
    show_remaining_capacity_limit = models.PositiveIntegerField("Show remaining capacity when remaining tickets are less or equal than this limit.", default=50)
    booking_tickets_pending_timeout = models.PositiveIntegerField("Timeout for tickets without a visitor assigned", default=600)
    booking_new_timeout = models.PositiveIntegerField("Timeout for tickets until booking is confirmed by email token", default=86400)
    tickets_unconfirmed_waiting_timeout = models.PositiveIntegerField("Timeout for tickets which have been released from the waiting list", default=86400)
    is_active = models.BooleanField('Make visitor registration publicly accessible', default=False)
    welcome_text = models.TextField(blank=True, null=True, default='')
    wait_for_email_text = models.TextField(blank=True, null=True, default='')
    show_registration_text = models.TextField(blank=True, null=True, default='')

    def __str__(self):
        return _('%s Registration') % self.conference.short_name

    @transaction.atomic
    def expire_tickets(self):
        tickets = []
        for day in self.days.all():
            et = day.expire_tickets()
            tickets = tickets + list(et)
        return tickets

    def booked_tickets(self):
        return sum(d.booked_tickets().count() for d in self.days.all())

    def booked_tickets_last_24h(self):
        return sum(d.booked_tickets().filter(created__gte = datetime.now() - timedelta(hours=24)).count() for d in self.days.all())
    
    # TODO: add cleanup: delete unconfirmed bookings which do not have any visitors

    def get_absolute_url(self):
        return reverse('visitorregistration:start', kwargs={'conference_id': self.conference.url_snippet})

class Ticket(models.Model):
    """Ticketing Logic:

    When creating a new ticket, it will take either the state
    PENDING_OK or PENDING_WAITING depending on the availibility of tickets
    for the specific day.

    OK tickets are booked tickets with a visitor assigned.

    PENDING_OK tickets have no visitor assigned yet and are counted
    as booked tickets (OK), but expire after a timeout and become
    STALE. PENDING_OK tickets become OK when a visitor is set. If a
    visitor is set they become OK.

    UNCONFIRMED tickets are released from WAITING and may become
    booked tickets (OK). They must have a visitor and an expiration.

    WAITING tickets are tickets waiting for capacity to become available.
    
    PENDING_WAITING tickets have no visitor assigned yet and become
    WAITING tickets after a visitor has been assigned.

    When a (temporary, time-limited) state of a ticket has expired,
    they become STALE.

    NEW tickets are assigned to the states PENDING_OK or
    PENDING_WAITING by Booking.claim_tickets depending on the
    remaining ticket capacity of that day

    Tickets can always be canceled. They are not deleted
    automatically, but get the state CANCELED.
 

             +------------+
             | PENDING_OK |
    +----+<--+------------+
    | OK |
    +----+<--+-------------+   +---------+
             | UNCONFIRMED |<--| WAITING |
             +-------------+   +---------+

    """
    visitor = models.ForeignKey('Visitor', null=True, blank=True, on_delete=models.CASCADE)
    day = models.ForeignKey('ConferenceRegistrationDay', on_delete=models.CASCADE)
    booking = models.ForeignKey('Booking', on_delete=models.CASCADE)

    OK, CANCELED, PENDING_OK, UNCONFIRMED, WAITING, PENDING_WAITING, STALE, NEW = range(8)
    STATES = (
        (OK, 'OK'),
        (CANCELED, 'CANCELED'),
        (PENDING_OK, 'PENDING_OK'),
        (UNCONFIRMED, 'UNCONFIRMED'),
        (WAITING, 'WAITING'),
        (PENDING_WAITING, 'PENDING_WAITING'),
        (STALE, 'STALE'),
        (NEW, 'NEW'),
    )
    ANONYMOUS_STATES = [PENDING_OK,PENDING_WAITING,STALE,NEW] # states for anoymous tickets (or unvalidated bookings, which are kind of anonymous
    OCCUPYING_STATES = [OK,PENDING_OK,UNCONFIRMED] # states which occupy event capacity
    TEMPORARY_STATES = [PENDING_OK,PENDING_WAITING,UNCONFIRMED] # states which require state_expiration
    state = models.IntegerField(choices = STATES, default=NEW)
    state_expiration = models.DateTimeField(null=True, blank=True, default=None)
    entered_waiting_queue = models.DateTimeField(null=True, blank=True, default=None)
    created = models.DateTimeField()
    changed = models.DateTimeField()

    def __str__(self):
        name = 'anonymous'
        email = 'anonymous'
        if self.visitor:
            name = self.visitor.name
            email = self.visitor.email
        return u'%s - %s <%s> %s' % (self.day.day,name,email,self.STATES[self.state][1])

    def save(self, **kwargs):
        if not self.id:
            self.created = timezone.now()
        self.changed = timezone.now()
        super(Ticket, self).save(**kwargs)

    def clean(self):
        if self.day not in self.booking.conference_registration.days.all():
            raise ValidationError('Ticket %s: day %s is not contained in conference %s'%(self,self.day,self.booking.conference_registration))
        
        if not self.visitor:
            # this is an anonymous ticket
            if self.state not in self.ANONYMOUS_STATES:
                raise ValidationError('Ticket %s: state %s is not valid for anonymous tickets' % (self,self.state))

        if self.state in self.TEMPORARY_STATES:
            if not self.state_expiration:
                raise ValidationError('Ticket %s: state %s requires state_expiration to be set' % (self,self.state))
        
        if self.state not in self.TEMPORARY_STATES:
            if self.state_expiration:
                raise ValidationError('Ticket %s: state %s may not have state_expiration to be set' % (self,self.state))

        if self.state == self.WAITING:
            if not self.entered_waiting_queue:
                raise ValidationError('Ticket %s: state %s requires entered_waiting_queue to be set' % (self,self.state))
        else:
            if self.entered_waiting_queue:
                raise ValidationError('Ticket %s: state %s may not have entered_waiting_queue to be set' % (self,self.state))

    @transaction.atomic
    def cancel(self):
        if self.visitor:
            self.state = Ticket.CANCELED
            self.state_expiration = None
            self.entered_waiting_queue = None
            self.clean()
            self.save()
        else:
            self.delete()

    @transaction.atomic
    def uncancel(self):
        if self.booking._can_book_more_tickets_for_all_days(1):
            self.state = Ticket.NEW
            self.clean()
            self.save()
        else:
            raise MaxNumberOfVisitorsReachedError(self.booking)

    @transaction.atomic
    def confirm(self):
        if self.state == Ticket.UNCONFIRMED:
            self.state = Ticket.OK
            self.state_expiration = None
            self.clean()
            self.save()
        else:
            raise ValidationError('Ticket %s cannot be confirmed.' % self)
            
    @staticmethod
    def stronger_ticket_state(s1,s2):
        states = [s1,s2]
        if Ticket.NEW in states:
            return Ticket.NEW
        if Ticket.CANCELED in states:
            return Ticket.CANCELED
        if Ticket.STALE in states:
            return Ticket.STALE
        if Ticket.PENDING_WAITING in states:
            return Ticket.PENDING_WAITING
        if Ticket.WAITING in states:
            return Ticket.WAITING
        if Ticket.UNCONFIRMED in states:
            return Ticket.UNCONFIRMED
        if Ticket.PENDING_OK in states:
            return Ticket.PENDING_OK
        if Ticket.OK in states:
            return Ticket.OK

    @staticmethod
    def earlier_ticket_state_expiration(e1, e2):
        if e1 == None:
            return e2
        if e2 == None:
            return e1
        return min(e1,e2)

class ConferenceRegistrationDay(models.Model):
    conference_registration = models.ForeignKey(ConferenceRegistration, related_name='days', on_delete=models.CASCADE)
    day = models.OneToOneField(ConferenceDay, related_name='visitor_capacity', on_delete=models.CASCADE)
    capacity = models.PositiveIntegerField()
    overbooking_quantity = models.PositiveIntegerField()
    visitors = models.ManyToManyField('Visitor', through='Ticket')
    bookings = models.ManyToManyField('Booking', through='Ticket')

    def __str__(self):
        return '%s - max %i' % (self.day,self.capacity)

    def booked_tickets(self):
        return self.ticket_set.filter(state__exact=Ticket.OK)

    def occupying_tickets(self):
        return self.ticket_set.filter(state__in=Ticket.OCCUPYING_STATES)

    def remaining_capacity(self, overbooking=False):
        capacity = self.capacity
        if overbooking:
            capacity = capacity + self.overbooking_quantity
        rc = capacity - self.occupying_tickets().count()
        if rc > 0:
            return rc
        return 0

    def capacity_soft_limit_reached(self):
        return self.capacity <= self.occupying_tickets().count()

    def capacity_hard_limit_reached(self):
        return self.capacity + self.overbooking_quantity <= self.occupying_tickets().count()

    @transaction.atomic
    def expire_tickets(self):
        if self.conference_registration.conference in ConferenceQuerysets.archived_conferences():
            # if conference is over, we don't do anything anymore
            return []
        
        et = self.ticket_set.filter(state_expiration__lt=timezone.now())
        for ticket in et:
            if ticket.visitor:
                ticket.state = Ticket.CANCELED
            else:
                ticket.state = Ticket.STALE
            ticket.state_expiration = None
            ticket.clean()
            ticket.save()

        # waiting list is processed intentionally unconditional
        # handle waiting list
        limit = self.remaining_capacity(True)
        wt = self.ticket_set.filter(state__exact=Ticket.WAITING).order_by('entered_waiting_queue').all()[:limit]

        # prioritize all bookings with existing overbooking_grant
        for ticket in wt:
            if not ticket.booking.has_overbooking_grant():
                continue
            ticket.booking.release_waiting_tickets()

        for ticket in wt:
            ticket.booking.release_waiting_tickets()

        return et
        
    
class BookingManager(models.Manager):
    
    def create_booking_for_all_days(self, registration, quantity, max_visitors_override=None):
        """ creates a new booking
        :param registration: a VisitorRegistration object for which the booking shall be created
        :param quantity: the number of tickets which shall be created for each day of the conference
        :returns: booking
        """

        booking = self.create(
            conference_registration = registration,
            token = str(random.getrandbits(64)),
            max_visitors_override = max_visitors_override,
            state = Booking.NEW,
            state_expiration = timezone.now() + timedelta(0, registration.booking_new_timeout),
        )

        tickets = booking.add_visitors_for_all_days(quantity)
        
        booking.clean()
        booking.save()
        booking.claim_tickets()

        return booking

class Booking(models.Model):
    conference_registration = models.ForeignKey(ConferenceRegistration, on_delete=models.CASCADE)
    token = models.CharField(max_length=64)
    preferred_language = models.CharField(_('Preferred language'), max_length = 10, choices = settings.LANGUAGES, default=get_initial_lang, help_text = _("Used for emails."))
    contact = models.ForeignKey('Visitor',related_name='+', null=True, blank=True, on_delete=models.CASCADE)
    max_visitors_override = models.PositiveIntegerField(null=True, blank=True)
    creation_time = models.DateTimeField(auto_now_add=True)
    last_modification_time = models.DateTimeField(auto_now=True)
    visitors = models.ManyToManyField('Visitor', through='Ticket')

    OK, NEW = range(2)
    STATES = (
        (OK, 'OK'),
        (NEW, 'NEW'),
    )
    TEMPORARY_STATES = [NEW]

    state = models.IntegerField(choices = STATES, default=NEW)
    state_expiration = models.DateTimeField(null=True, blank=True, default=None)


    objects = BookingManager()

    def __str__(self):
        name = 'anonymous'
        email = 'anonymous'
        if self.contact:
            name = self.contact.name
            email = self.contact.email
        return '#%i %s <%s> %s' % (self.id,name,email,self.creation_time.strftime('%d.%m.%Y %H:%M'))


    def send_templated_mail(self, template, context={}):
        context.update({
            'contact': self.contact,
            'conference': self.conference_registration.conference,
            'booking': self,
            'booking_url': self.get_booking_url(),
            'FROSCH_NAME': settings.FROSCH_NAME,
        })
        template, lang_context = find_template(template, self.preferred_language)
        with lang_context:
            mail.send(
                template=template,
                recipients=[self.contact.email],
                context=context,
                priority=mail.PRIORITY.now
            )

    def delete_if_no_tickets(self):
        if not self.ticket_set.exists():
            self.delete()
            
    
    def has_overbooking_grant(self):
        if self.ticket_set.filter(state__in=Ticket.OCCUPYING_STATES).exclude(state_expiration__lt=timezone.now()).count() > 0:
            return True

        for ticket in self.ticket_set.all():
            if not ticket.day.capacity_soft_limit_reached():
                return True

        return False

    def get_booking_url(self):
        booking_url = urljoin(settings.FROSCH_BASE_URL, reverse('visitorregistration:show_with_token', args = [self.conference_registration.conference.url_snippet, self.id, self.token]))
        return booking_url
    
    def get_max_visitors(self):
        if self.max_visitors_override:
            return self.max_visitors_override
        return self.conference_registration.max_visitors_per_booking

    def get_tickets_by_days(self):
        tickets_by_days = {}
        for day in self.conference_registration.days.all():
            tickets_by_days[day] = []
        
        for ticket in self.ticket_set.all():
            ticket.clean()
            tickets_by_days[ticket.day].append(ticket)
        return tickets_by_days

    def _can_book_more_tickets_for_all_days(self, quantity):
        tickets_by_days = self.get_tickets_by_days()

        for day, tickets in tickets_by_days.items():
            num = 0
            for ticket in tickets:
                if ticket.state != Ticket.CANCELED:
                   num = num + 1 
            if num + quantity > self.get_max_visitors():
                raise MaxNumberOfVisitorsReachedError(self)

        return tickets_by_days

    @transaction.atomic
    def add_visitors_for_all_days(self, quantity=1):
        
        if self._can_book_more_tickets_for_all_days(quantity):

            tickets = [ [] for i in range(quantity) ]
            for day in self.conference_registration.days.all():
                # create tickets
                for i in range(quantity):
                    t = Ticket.objects.create(
                        day = day,
                        booking = self,
                        state = Ticket.NEW,
                    )
                    t.clean()
                    t.save()
                    tickets[i].append(t)

            self.clean()
            self.save()
            self.claim_tickets()

            return tickets

        return None

    @transaction.atomic
    def claim_tickets(self):
        for ticket in self.ticket_set.exclude(state__exact=Ticket.OK):

            if ticket.state == Ticket.NEW:
                ticket.state_expiration = timezone.now() + timedelta(0,self.conference_registration.booking_tickets_pending_timeout)
                if ticket.day.remaining_capacity(self.has_overbooking_grant()) > 0:
                    ticket.state = Ticket.PENDING_OK
                else:
                    ticket.state = Ticket.PENDING_WAITING
            
            if self.state == self.OK:
            
                if ticket.state == Ticket.PENDING_OK and ticket.visitor:
                    ticket.state = Ticket.OK
                    ticket.state_expiration = None

                if ticket.state == Ticket.PENDING_WAITING and ticket.visitor:
                    # TODO: what if tickets have become available in the meantime? -> waiting queue must be processed (solution: always process waiting list)
                    ticket.state = Ticket.WAITING
                    ticket.state_expiration = None
                    ticket.entered_waiting_queue = timezone.now()

                if ticket.state in [Ticket.STALE,Ticket.NEW] and ticket.visitor:
                    if ticket.day.remaining_capacity(self.has_overbooking_grant()) > 0:
                        ticket.state = Ticket.OK
                    else:
                        ticket.state = Ticket.WAITING
                        ticket.entered_waiting_queue = timezone.now()

            else:
                # booking state is NEW
                if ticket.state in Ticket.TEMPORARY_STATES and ticket.visitor:
                    ticket.state_expiration = self.state_expiration            

            ticket.clean()
            ticket.save()

    @transaction.atomic
    def release_waiting_tickets(self):
        if self.conference_registration.conference in ConferenceQuerysets.archived_conferences():
            # if conference is over, we don't do anything anymore
            return
        
        released_tickets = []
        expiration = timezone.now() + timedelta(0,self.conference_registration.tickets_unconfirmed_waiting_timeout)
        for ticket in self.ticket_set.filter(state__exact=Ticket.WAITING):
            rc = ticket.day.remaining_capacity(self.has_overbooking_grant())
            if rc > 0:
                ticket.state = Ticket.UNCONFIRMED
                ticket.state_expiration = expiration
                ticket.entered_waiting_queue = None
                ticket.clean()
                ticket.save()
                released_tickets.append(ticket)

        with translation.override(self.preferred_language):
            
            if released_tickets:
                self.send_templated_mail(
                    'visitorregistration.tickets-from-waitinglist-released',
                    {
                        'booking_url': self.get_booking_url(),
                        'expiration': expiration,
                        'tickets': released_tickets,
                    }
                )
                
    @transaction.atomic
    def remove_expired_tickets_without_visitor(self):
        for ticket in self.ticket_set.filter(state__in=Ticket.ANONYMOUS_STATES).filter(state_expiration__lt=timezone.now()):
            if not ticket.visitor:
                ticket.delete()
                pass

    def confirm_booking(self):
        """ confirm booking with token sent by email (intentionally not an atomic transaction) """
        
        if self.conference_registration.conference in ConferenceQuerysets.archived_conferences():
            # if conference is over, we don't do anything anymore
            return

        if self.state == Booking.NEW:
            self.state = Booking.OK
            self.state_expiration = None
            self.clean()
            self.save()
            self.claim_tickets()

            for ticket in self.ticket_set.all():
                if not ticket.state in [Ticket.STALE, Ticket.CANCELED]:
                    self.send_templated_mail('visitorregistration.booking-confirmed')
                    break

            return True
        else:
            return False

    def confirm_tickets(self):
        for ticket in self.ticket_set.filter(state__exact=Ticket.UNCONFIRMED):
            ticket.confirm()

    def send_confirmation_email(self, request):
        if self.conference_registration.conference in ConferenceQuerysets.archived_conferences():
            # if conference is over, we don't do anything anymore
            return
        self.send_templated_mail('visitorregistration.confirm-booking')
        
    def clean(self):
        if self.state in self.TEMPORARY_STATES and not self.state_expiration:
            raise ValidationError('Booking %s: state $s requires state_expiration to be set' % (self,self.state))
        
        if self.state not in self.TEMPORARY_STATES and self.state_expiration:
                raise ValidationError('Ticket %s: state %s may not have state_expiration to be set' % (self,self.state))
        
        tickets_by_days = self.get_tickets_by_days()
        
        for day, tickets in tickets_by_days.items():
            num = 0
            for ticket in tickets:
                if ticket.state != Ticket.CANCELED:
                   num = num + 1 
            if num > self.get_max_visitors():
                raise MaxNumberOfVisitorsReachedError(booking)



class Visitor(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()
    subscribe_to_newsletter = models.BooleanField(verbose_name='Subscribe to newsletter', default=False)

    def __str__(self):
        return '%s <%s>' % (self.name,self.email)

    def assign_tickets(self, tickets):
        for ticket in tickets:
            ticket.visitor = self
            ticket.clean()
            ticket.save()


class MaxNumberOfVisitorsReachedError(Exception):
    def __init__(self, booking):
        self.booking = booking

    def __str__(self):
        ValidationError('Booking may only have up to %i visitors per day' % self.booking.get_max_visitors())
