from modeltranslation.translator import translator, TranslationOptions
from visitorregistration.models import ConferenceRegistration

class ConferenceRegistrationTranslationOptions(TranslationOptions):
    fields = ('welcome_text','wait_for_email_text','show_registration_text')

translator.register(ConferenceRegistration, ConferenceRegistrationTranslationOptions)


