from django.core.management.base import BaseCommand
from visitorregistration.models import *
from django.utils.translation.trans_real import get_supported_language_variant


class Command(BaseCommand):
    help = 'Expire tickets and remove stale bookings'

    def add_arguments(self, parser):
        parser.add_argument('--booking-max-days', dest='booking-max-days', type=int, default=7, help='Set the number of days after which incomplete bookings are purged (default=7).')
    
    def handle(self, *args, **options):

        # expire tickets, i.e., calculate new states
        for reg in ConferenceRegistration.objects.all():
            tickets = reg.expire_tickets()
            tickets_by_booking = {}
            for ticket in tickets:
                if ticket.booking not in tickets_by_booking.keys():
                    tickets_by_booking[ticket.booking] = [ticket]
                else:
                    tickets_by_booking[ticket.booking].append(ticket)

            for booking, tickets in tickets_by_booking.items():
                if booking.contact:
                    with translation.override(get_supported_language_variant(booking.preferred_language)):
                        booking_url = booking.get_booking_url()

                        booking.send_templated_mail(
                            'visitorregistration.tickets-automatically-cancelled',
                            {'tickets': tickets}
                        )

        # delete old stale tickets without visitor
        for ticket in Ticket.objects.filter(state__exact = Ticket.STALE).filter(visitor__isnull=True).filter(changed__lt=timezone.now() - timedelta(options['booking-max-days'])):
            ticket.delete()

        # delete old bookings
        for booking in Booking.objects.filter(state__exact=Booking.NEW).filter(state_expiration__lt=timezone.now() - timedelta(options['booking-max-days'])):
            booking.delete_if_no_tickets()
                
