# FROSCH Conference Management System
*The FRee and Open Source Conference Helper*

FROSCH was written specifically for [Pi and More][1], but aims to be reusable with moderate efforts.

FROSCH covers the following areas of a conference:

 - Call for Contributions/Papers (finished)
 - Visitor Registration (under development)
 - Helper Registration and Management (planned)

FROSCH is written in Python using the Django framework.

Main Features:

- Fully translatable and l10nable (currently featuring English and German translations)
- Call for Contributions:
  - Multiple Conferences with multiple days
  - Multiple configurable types or formats of contributions (e.g., talk, exhibition and workshop)
  - Support for rooms, tracks, contribution languages
  - Automatic opening/closing of the Call for Contributions
  - Visual timetable for easy scheduling
  - Submitters can 
     - request assets (e.g., special hardware) for their contribution
     - select configurable tags (e.g., "do not record")
  - Individual lengths can be configured for contribution formats
  - Submission formatting with Markdown
- Visitor Registration (TBD)

Planned features:

 - Data export to standard conference schedule formats

Features not intended for inclusion at the moment:

 - Paid tickets

  [1]: https://piandmore.de

## Setup

(TBD)

### Load Sample Data #

    $ ./manage.py loaddata core/fixtures/example.json
    $ ./manage.py loaddata callforcontributions/fixtures/example.json

## License

FROSCH is licensed under the GPL 3.0 (see LICENSE)
